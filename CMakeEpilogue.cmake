# cpack for linux rpm, binary distribution cc7
# default into /opt/OpcUaLxiServer/
# this is how it works:
# 1. this CMakeEpilogue.cmake is run at the end of the quasar build chain.
#    it will create a cpack configuration file in build/CPackConfig.cmake as intermediate step
# 2. then run cpack -G RPM inside the build directory
# 3. the resulting RPM is in build/, copy to ../RPM
# inspired from Ben's caen server
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]" )

include(InstallRequiredSystemLibraries)

set(CPACK_BINARY_RPM ON)
set(CPACK_PROJECT_NAME OpcUaLxiServer.${OPCUA_LXI_SERVER_SUFFIX} )
set(CPACK_PACKAGE_VENDOR "CERN-BE-ICS-FT")
set(CPACK_PACKAGE_MAINTAINER "Michael Ludwig" )
set(CPACK_RPM_PACKAGE_LICENSE "(c) CERN 2024")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "OPCUA Lxi server (CERN-BE-ICS-FT)")
set(CPACK_PACKAGE_DESCRIPTION "OPCUA LXI server for lab power supplies (i.e. aimTTi), polling only")
set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
set(CPACK_PACKAGE_RELEASE ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH} )

set(CPACK_PACKAGE_NAME ${CPACK_PROJECT_NAME}) 
set(CPACK_PACKAGE_CONTACT ${CPACK_PACKAGE_MAINTAINER})


set(CPACK_RPM_PACKAGE_AUTOREQ 1)       # enable automatic shared lib dependency detection
set(CPACK_RPM_PACKAGE_RELOCATABLE ON)  # can install where you want, relocatable
set(CPACK_SET_DESTDIR "OFF")           # relocatable

set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION "/opt")
set(CPACK_PACKAGING_INSTALL_PREFIX "/opt")
 
SET( INSTALL_DIR_ROOT /opt/${CPACK_PROJECT_NAME} )
SET( INSTALL_DIR_BIN ${INSTALL_DIR_ROOT}/bin )
SET( INSTALL_DIR_CONFIGURATION ${INSTALL_DIR_ROOT}/Configuration )
# not yet needed
#SET( CPACK_RPM_POST_INSTALL_SCRIPT_FILE ${PROJECT_SOURCE_DIR}/Installer/Linux/post_install.sh )
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] EXECUTABLE= ${EXECUTABLE}" )

install( TARGETS 
	${EXECUTABLE}  #	OpcUaLxiServer.${OPCUA_LXI_SERVER_SUFFIX} 
	RUNTIME DESTINATION ${INSTALL_DIR_BIN} 
	LIBRARY DESTINATION ${INSTALL_DIR_BIN}	)
install( FILES
	${PROJECT_SOURCE_DIR}/run/config_rpm.xml
	DESTINATION ${INSTALL_DIR_BIN}
	RENAME config.xml )
install( FILES 
	${PROJECT_SOURCE_DIR}/run/ServerConfig_rpm.xml
	DESTINATION ${INSTALL_DIR_BIN}
	RENAME ServerConfig.xml)
install( FILES 
	${PROJECT_SOURCE_DIR}/run/Configuration/Configuration.xsd 
	DESTINATION ${INSTALL_DIR_CONFIGURATION} )
install( FILES 
	${PROJECT_SOURCE_DIR}/run/Configuration/ServerConfig.xsd 
	DESTINATION ${INSTALL_DIR_CONFIGURATION} )
#install( FILES 
#	/usr/lib64/libxerces-c-3.2.so 
#	DESTINATION ${INSTALL_DIR_BIN} )
#install(RUNTIME_DEPENDENCY_SET /usr/local/lib/libxerces-c.so RUNTIME DESTINATION bin)

  
SET( CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}.${OPCUA_TOOLKIT}.${CMAKE_SYSTEM_PROCESSOR}" ) 
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] assemble package ${CPACK_PACKAGE_FILE_NAME}" )

include(CPack)

 
