# @author mludwig (c) cern
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cc7.ua" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
# SET( IGNORE_DEFAULT_BOOST_SETUP "ON" )
SET( BOOST_HOME /opt/3rdPartySoftware/boost/boost_1_75_0 )
SET( BOOST_ROOT ${BOOST_HOME} )
SET( BOOST_PATH_LIBS ${BOOST_HOME}/stage/lib )
SET( BOOST_PATH_HEADERS  ${BOOST_HOME} )


#-----------
# UA toolkit
#-----------
SET( OPCUA_TOOLKIT_PATH "/3rdPartySoftware/sdk" )
message(STATUS "UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" ) 
add_definitions( -DBACKEND_UATOOLKIT )
SET( OPCUA_TOOLKIT_LIBS_DEBUG   "-luamodule -lcoremodule -luapki -luabase -luastack -lxmlparser -lxml2 -lssl -lcrypto -lpthread" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS_DEBUG} ) 
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uabase )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uastack )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uaserver )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uapki )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/xmlparser )

# tookit: add empty target as flag for ua
add_custom_target ( quasar_opcua_backend_is_ready )

# strange problem: does not find a base class include
#include_directories( ./build/Device/generated )


# liblxi done in toolchain

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

