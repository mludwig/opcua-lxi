#!/bin/bash
# run cpack to get the RPMs
BUILD_DIR=./build
cd ${BUILD_DIR} 
#make package

cpack -D CPACK_RPM_PACKAGE_DEBUG=1 -G RPM --verbose 
rm -rf ../RPM
mkdir -p ../RPM
cp -v ./OpcUaLxiServer*.rpm ../RPM/
# check it:
echo "===checking the contents of the rpm==="
rpm -qpl ../RPM/OpcUaLxiServer*.rpm
