#!/bin/bash
# quasar 1.5.18 on alma9
# prepare an open6 sdk, from ./build/open62541-compat/extern/open62541/prepare_open62541.sh
# result differs for various linux OS (compiler versions)
# lets copy the correct version explicitly
cp -v ./open62541/src/open62541_pmoschov.c ./open62541/src/open62541.c
cp -v ./open62541/include/open62541_pmoschov.h ./open62541/include/open62541.h

python3 ./quasar.py set_build_config ./gitlab_cal9_open6.cmake
python3 ./quasar.py enable_module open62541-compat
# somewhat undocumented quasar command, call that explicitly to generate the file as well. Otherwise it is missing arbitrarily 3/5 cases
python3 ./quasar.py generate base_h LxiCommand
python3 ./quasar.py build 
#
cp -v ./build/bin/OpcUaLxiServer.cal9.open6 ./run/OpcUaLxiServer.cal9.open6
cp -v ./build/Configuration/Configuration.xsd ./run/Configuration
#
# adapt the ServerConfig.xml for the toolkit
cp -v ./run/ServerConfig.open6.xml ./run/ServerConfig.xml 
cp -v ./run/ServerConfig.open6.xml ./run/ServerConfig_rpm.xml 
cp -v ./build/open62541-compat/xsd/ServerConfig.xsd ./run/Configuration
# adapt ServerConfig.xml to toolkit
cp ./run/ServerConfig.open6.xml ./run/ServerConfig.xml

