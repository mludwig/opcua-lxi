/*
 * LxiComm.cpp
 *
 * standalone class providing configurable hardware communication for Lxi devices.
 * all the lxi comms take place in this class.
 *
 * There is a very basic simulation mode available which does not lxi communicate but fakes
 * replies and values insted. To switch this on for a configured device, use port=0
 *
 *  Created on: Jun 2, 2020
 *      Author: mludwig
 */


#include "LxiComm.h"
#include <lxi.h>

#include <DLxiCommand.h>
#include <thread>
#include <mutex>

/**
 * a hadcoded list of supported power supplies. Like this we can just discover the ps from it's ip,
 * read the identification, get it's type string and look up the rest of the characteristics using this map.
 */
/* static */ std::map<std::string, LxiComm::METATYPES_t> LxiComm::metatypes_map = {
		{"PL155-P",     {1, 15,  5}}, // type, nbChannels, hwVmax, hwImax
		{"PL303-P",     {1, 30,  3}},
		{"PL601-P",     {1, 60,  1.5}},
		{"PL303QMD-P",  {2, 30,  3}},
		{"PLH120-P",    {1, 120, 0.75}},
		{"PLH250-P",    {1, 250, 0.375}},
		{"PLO68-P",     {1, 6,   8}},
		{"PLO68-P",     {1, 6,   8}},
		{"CPX400SP",    {1, 60,  20}}, // 1 channel only supported
		{"CPX400DP",    {2, 60,  20}}
};
/* static */ std::map<std::string, uint32_t> LxiComm::delays_map = {
		{"PL155-P",     M_DELAY_MS_SLOW}, // type, comm delay in ms
		{"PL303-P",     M_DELAY_MS_SLOW},
		{"PL601-P",     M_DELAY_MS_SLOW},
		{"PL303QMD-P",  M_DELAY_MS_SLOW},
		{"PLH120-P",    M_DELAY_MS_SLOW},
		{"PLH250-P",    M_DELAY_MS_SLOW},
		{"CPX400SP",    M_DELAY_MS_SLOW},
		{"CPX400DP",    M_DELAY_MS_SLOW}
};

/**
 * constructor, one (inherited) instance per ps. we also set a timeout for all lxi communications, which
 * is very lenient.
 */
LxiComm::LxiComm():
							m_device(0),
							m_connected( false ),
							// m_timeout_ms(5000),
							m_timeout_ms(1000),
							m_vendor(""),
							m_identifier(""),
							m_psType(""),
							m_family(""),
							m_serialNumber(""),
							m_firmwareVersion(""),
							m_nbChannels(0),
							_hostname("my_localhost"),
							m_hwVmax(0),
							m_hwImax(0),
							m_port(0),
							m_ip("no ip"),
							m_reset_ack( false ),
							m_sessionOpened( false )
{
	/*
	 * populate with known power supply types for each family
	 * . This has to be hardcoded because the abstraction methods
	 * have to be handcoded anyway and are part of the underlying server design.
	 *
	 *                                                       ps type,    family
	 */
	m_familyMap.insert( std::pair<std::string, std::string>("PL155-P",   "aimTTi_PL"));
	m_familyMap.insert( std::pair<std::string, std::string>("PL303-P", "aimTTi_PL"));
	m_familyMap.insert( std::pair<std::string, std::string>("PL601-P", "aimTTi_PL"));
	m_familyMap.insert( std::pair<std::string, std::string>("PL303QMD-P", "aimTTi_PL"));
	m_familyMap.insert( std::pair<std::string, std::string>("PLH120-P", "aimTTi_PL"));
	m_familyMap.insert( std::pair<std::string, std::string>("PLH250-P", "aimTTi_PL"));
	m_familyMap.insert( std::pair<std::string, std::string>("PLO68-P", "aimTTi_PL"));

	// ENS-30720 add CPX400SP. might need it's own abstraction or family
	m_familyMap.insert( std::pair<std::string, std::string>("CPX400SP", "aimTTi_CPX"));
	m_familyMap.insert( std::pair<std::string, std::string>("CPX400DP", "aimTTi_CPX"));

	// not so sure about these yet: needs testing
	m_familyMap.insert( std::pair<std::string, std::string>("TSX1820P",  "aimTTi_TSX"));

	// not so sure about these yet: needs testing
	m_familyMap.insert( std::pair<std::string, std::string>("LD400P",    "aimTTi_LD"));

	char hostname[ 4096 ] = "\0";
	size_t len = 4096;
	gethostname( hostname, len);
	_hostname = std::string( hostname );
	LOG(Log::DBG) << "OK constructor " << __FUNCTION__;
}


LxiComm::~LxiComm() {}

/**
 * connect over ethernet, basically a glorified TCP/IP connect in this case.
 */
int LxiComm::connect( std::string ip, int port ){
	LOG(Log::INF) << __FUNCTION__ << ": try to connect to ip= " << ip
			<< ":" << port
			<< " timeout [ms] = " << m_timeout_ms;

	m_device = lxi_connect( ip.c_str(), port, "", m_timeout_ms, RAW );
	LOG(Log::DBG) << __FUNCTION__ << ": lxi_connect returned ip= " << ip
			<< ":" << port
			<< " timeout [ms] = " << m_timeout_ms;
	m_sessionOpened = true;
	m_ip = ip;
	m_port = port;
	if ( m_device >= 0 ) {
		m_connected = true;
		LOG(Log::INF) << __FUNCTION__ << ": bingo, connect to ip= " << ip
				<< ":" << port
				<< " timeout [ms] = " << m_timeout_ms
				<< " seems OK";
		return( m_device );
	} else {
		return( -1 );
	}
}


/**
 * from the ID string, find the vendor, serial nb, firmware version. assume it is comma separated and
 * has 4 fields, i.e.:
 * THURLBY THANDAR, PLH250-P, 517522, 1.04-4.06
 */
void LxiComm::_decodeIdentifier(){
	int field = 0;
	for ( unsigned int i = 0; i < m_identifier.length(); i++ ){
		if ( m_identifier[ i ] == ','){
			field++;
		} else {
			switch( field){
			case 0:{ // vendor
				m_vendor += m_identifier[ i ];
				break;
			}
			case 1:{ // type
				// m_type += m_identifier[ i ];
				break;
			}
			case 2:{ // serial nb
				m_serialNumber += m_identifier[ i ];
				break;
			}
			case 3:{ // fw number
				m_firmwareVersion += m_identifier[ i ];
				break;
			}
			}
		}
	}
	if ( field != 3 ){
		LOG(Log::WRN) << "identifier decoding did not work, vendor, serial nb and firmware nb might be wrong";
	} else {
		m_serialNumber.erase(m_serialNumber.find_first_of(" "), 1);
		m_firmwareVersion.erase(m_firmwareVersion.find_first_of(" "), 1);
	}
}

/**
 * from the identifier string, find the type of the ps. We just check for all supported PS to be sure
 * THURLBY THANDAR, PLH250-P, 517522, 1.04-4.06
 */
void LxiComm::_type(){
	m_psType = "undefined";
	for (auto& x: m_familyMap ){
		std::size_t found = m_identifier.find( x.first );
		if ( found != std::string::npos ) {
			m_psType = x.first;
		}
	}
	if ( m_psType == "undefined"){
		LOG(Log::ERR) << "could not find power supply type from identifier= " << m_identifier << ", exiting..";
		exit( 0 );
	}


	// ENS-30720: unfortunately there are several versions with 1 or two channels which have the same *IDN?
	// therefore, for the CPX family, this does not work.
	// The nb of channels can not be determined by reading the ID. We only support 2 channels
	// how many channels does this unit have? read the hardcoded type map
	m_nbChannels = LxiComm::metatypes_map.find( m_psType )->second.nbChannels;
	if ( m_nbChannels < 1 ){
		LOG(Log::ERR) << "could not find nbChannels for p.s. type= " << m_psType << ", exiting..";
		exit( 0 );
	}
	LOG(Log::INF) << m_psType << " has m_nbChannels= " << m_nbChannels;


	// max V and I hardware limits
	m_hwVmax = LxiComm::metatypes_map.find( m_psType )->second.hwVmax;
	if ( m_hwVmax <= 0 ){
		LOG(Log::ERR) << "could not find hardware Vmax for p.s. type= " << m_psType << ", exiting..";
		exit( 0 );
	}
	LOG(Log::INF) << m_psType << " has m_hwVmax= " << m_hwVmax;

	m_hwImax = LxiComm::metatypes_map.find( m_psType )->second.hwImax;
	if ( m_hwImax <= 0 ){
		LOG(Log::ERR) << "could not find hardware Imax for p.s. type= " << m_psType << ", exiting..";
		exit( 0 );
	}
	LOG(Log::INF) << m_psType << " has m_hwImax= " << m_hwImax;
}

/**
 * from the type, get the family. We just check for all supported PS to be sure
 */
void LxiComm::_family(){
	std::map<std::string, std::string>::iterator found = m_familyMap.find( m_psType );
	if ( found != m_familyMap.end() ) {
		LOG(Log::DBG) << "found family= " << found->second << " for power supply type= " << m_psType;
		m_family = found->second;
	} else {
		LOG(Log::ERR) << "could not find family for power supply type= " << m_psType << ", exiting..";
		exit( 0 );
	}
}

/**
 * discover, and return the identification string, from the ipAdress. From this string we get
 * - vendor
 * - p.s. type
 * - serial number
 * - firmware version
 * - family
 *
 * Unfortunately, after a hard boot, this does not always return something since the PS seems
 * still to go through init. Therefore, if we get an empty string as a reply, we will try again
 * a couple of times.
 */
void LxiComm::getIdentifier( std::string ipAddress ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	LOG(Log::TRC) << "got command= " << cmd << " from map for method= " << __FUNCTION__;
	char m_response[SZRESPONSE];
	memset (m_response, 0, SZRESPONSE);

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );

	// read reply
	int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);

	// there was a reply
	if ( ret >= 0 ){
		LOG(Log::TRC) << __FUNCTION__ << " OK cmd= " << cmd << " reply= " << m_response;

		// the reply was empty: try again
		if ( std::string( m_response ).empty() ){
			int counter = 10;
			LOG(Log::WRN) << __FUNCTION__ << " could not read the ID string from cmd= "
					<< cmd << " reply= " << m_response << " try again " << counter;
			while ( counter > 0 ){
				lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );

				std::this_thread::sleep_for(std::chrono::milliseconds( 5000 ));

				// read reply
				int ret0 = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
				if ( ret0 >= 0 && ! std::string( m_response ).empty() ){
					LOG(Log::INF) << __FUNCTION__ << " could finally read the ID string from cmd= "
							<< cmd << " reply= " << m_response << ", continuing normally";
					counter = -99; // stop the tries
					continue;
				} else {
					counter--; // try again until 0
				}
			}

			// the reply was still empty also after 10 tries, therefore reject as "no reply"
			if ( counter == 0 ){
				LOG(Log::ERR) << __FUNCTION__ << " could not read the ID string from cmd= "
						<< cmd << " reply= " << m_response << " abandoning " << counter;
				m_identifier = "no reply after 10 tries";
				m_connected = false;

				// we try to reset the PS nevertheless, and we slightly abuse the command map, and make another round
				std::string cmd = Device::DLxiCommand::getCommandForMethod( "reset" );
				LOG(Log::TRC) << "got command= " << cmd << " from map for method= reset";
				lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
				counter = 10;
			}
		} else {
			// the reply was not empty and seems OK
			m_identifier = std::string( m_response );
			m_identifier.erase(m_identifier.length()-2);
		}
	} else {
		// there was no reply
		m_identifier = "no reply";
		m_connected = false;
	}
	_type();
	_family();
	_decodeIdentifier();
	LOG(Log::DBG) << __FUNCTION__ << " m_identifier= " << m_identifier << " m_nbChannels= " << m_nbChannels;
}


/**
 * we return voltages for all channels, since we cover also multi-channel devices
 * request-reply for all channels in a loop, return vector
 * request: V1?  reply: V1 12.000
 */
std::vector<float> LxiComm::getTargetVoltage(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	return(_getChannelsFloatsNoUnits( cmd ));
}


/**
 * applies a command and reads the reply without units, per channel, and returns everything in a vector
 * of floats.
 */
std::vector<float> LxiComm::_getChannelsFloatsNoUnits( std::string cmd ){

	std::vector<float> vtarget;
	vtarget.clear();

	for ( unsigned int ch = 0; ch < m_nbChannels; ch++ ){
		std::string co = cmd;
		size_t pos = co.find("n");
		co.replace(pos, 1, std::to_string( ch + 1));

		char m_response[SZRESPONSE];
		memset (m_response, 0, SZRESPONSE);
		lxi_send( m_device, co.c_str(), strlen( co.c_str()), m_timeout_ms );

		// read reply
		int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
		if ( ret >= 0 ){
			std::string r = std::string(m_response);
			if ( ! r.empty() ){
				vtarget.push_back( _decode2ndFloatNumberWithoutUnit( r ));
				m_connected = true;
			} else {
				LOG(Log::WRN) << "got empty reply for cmd= " << co
						<< " id= " << la_identifier()
						<< " ip= " << la_ip()
						<< " reply= " << r
						<< " set to 0.0";
				vtarget.push_back(0.0);
				m_connected = false;
			}
		} else {
			LOG(Log::ERR) << "got no reply for cmd= " << co
					<< " id= " << la_identifier()
					<< " ip= " << la_ip()
					<< " set to 0.0";;
			vtarget.push_back(0.0);
			m_connected = false;
		}
	}
	return( vtarget );
}

/**
 * applies a command and reads the reply without units, per channel, and returns everything in a vector
 * of floats.
 */
std::vector<unsigned int> LxiComm::_getChannelsIntsNoUnits( std::string cmd ){

	std::vector<unsigned int> vtarget;
	vtarget.clear();

	for ( unsigned int ch = 0; ch < m_nbChannels; ch++ ){
		std::string co = cmd;
		size_t pos = co.find("n");
		co.replace(pos, 1, std::to_string( ch + 1));

		char m_response[SZRESPONSE];
		memset (m_response, 0, SZRESPONSE);
		lxi_send( m_device, co.c_str(), strlen( co.c_str()), m_timeout_ms );

		// read reply
		int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
		if ( ret >= 0 ){
			std::string r = std::string(m_response);
			if ( ! r.empty() ){
				vtarget.push_back( _decodeIntNumber( r ));
				m_connected = true;
			} else {
				LOG(Log::WRN) << "got no reply for cmd= " << co
						<< " id= " << la_identifier()
						<< " ip= " << la_ip()
						<< " reply= " << r
						<< " set to 0";
				vtarget.push_back(0);
				m_connected = false;
			}
		} else {
			LOG(Log::ERR) << "got no reply for cmd= " << co
					<< " id= " << la_identifier()
					<< " ip= " << la_ip()
					<< " set to 0";;
			vtarget.push_back(0);
			m_connected = false;
		}
	}
	return( vtarget );
}

/**
 * applies a command and reads the reply without units, per channel, and returns everything in a vector
 * of bool.
 */
std::vector<bool> LxiComm::_getChannelsBoolsNoUnits( std::string cmd ){

	std::vector<bool> vtarget;
	vtarget.clear();

	for ( unsigned int ch = 0; ch < m_nbChannels; ch++ ){
		std::string co = cmd;
		size_t pos = co.find("n");
		co.replace(pos, 1, std::to_string( ch + 1));

		char m_response[SZRESPONSE];
		memset (m_response, 0, SZRESPONSE);
		lxi_send( m_device, co.c_str(), strlen( co.c_str()), m_timeout_ms );

		// read reply
		int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
		if ( ret >= 0 ){
			std::string r = std::string(m_response);
			m_connected = true;
			if ( ! r.empty() ){
				if ( r.find("1") == std::string::npos) {
					vtarget.push_back( false );
				} else {
					vtarget.push_back( true );
				}
				m_connected = true;
			} else {
				LOG(Log::WRN) << __FUNCTION__ << " got no reply for cmd= " << co
						<< " id= " << la_identifier()
						<< " ip= " << la_ip()
						<< " set to false";
				vtarget.push_back( false );
				m_connected = false;
			}
		} else {
			LOG(Log::ERR) << __FUNCTION__<< " got no reply for cmd= " << co
					<< " id= " << la_identifier()
					<< " ip= " << la_ip()
					<< " set to false";;
			vtarget.push_back( false );
			m_connected = false;
		}
	}
	return( vtarget );
}

/**
 * acquire the output enable from the hardware, for all channels
 */
std::vector<bool> LxiComm::getEnableOutput(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	return(_getChannelsBoolsNoUnits( cmd));
}
/**
 * acquire the current range from hardware: can be "high" or "low" whatever this means,
 * depending on the model and the shoesize of the manager. I would prefer honest "amp" units
 * but these guys made an abstraction: not my fault if this is opaque.
 *
 * Worse: not all models implement that. So.. make sure it behaves and at least returns "true"
 */
std::vector<bool> LxiComm::getCurrentRange(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	if ( !cmd.compare("none") ){
		std::vector<bool> v;
		for ( unsigned int ch = 0; ch < m_nbChannels; ch++ ){
			v.push_back( true );
		}
		return( v );
	} else {
		return(_getChannelsBoolsNoUnits( cmd));
	}
}

/**
 * acquire the setting for the tripping current. If the output reaches this tripping current the channel
 * is disabled, the output goes to 0 and a trip register bit is set
 */
std::vector<float> LxiComm::getTripCurrent(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	return(_getChannelsFloatsNoUnits( cmd));
}

/**
 * acquire the setting for the tripping voltage. If the output reaches this tripping voltage the channel
 * is disabled, the output goes to 0 and a trip register bit is set
 */
std::vector<float> LxiComm::getTripVoltage(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	std::vector<float> vtarget;
	vtarget.clear();

	for ( unsigned int ch = 1; ch <= m_nbChannels; ch++ ){
		std::string co = cmd;
		size_t pos = co.find("n");
		co.replace(pos, 1, std::to_string( ch ));

		char m_response[SZRESPONSE];
		memset (m_response, 0, SZRESPONSE);
		lxi_send( m_device, co.c_str(), strlen( co.c_str()), m_timeout_ms );

		// read reply
		int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
		if ( ret >= 0 ){
			std::string r = std::string(m_response);
			if ( ! r.empty() ){
				vtarget.push_back( _decode2ndFloatNumberWithoutUnit( r ));
				m_connected = true;
			} else {
				LOG(Log::WRN) << "got no reply for cmd= " << co
						<< " id= " << la_identifier()
						<< " ip= " << la_ip()
						<< " reply= " << r
						<< " set to 0.0";
				vtarget.push_back(0.0);
				m_connected = false;
			}
		} else {
			LOG(Log::ERR) << "got no reply for cmd= " << co
					<< " id= " << la_identifier()
					<< " ip= " << la_ip()
					<< " set to 0.0";;
			vtarget.push_back(0.0);
			m_connected = false;
		}
	}
	return( vtarget );
}


/**
 * we set target voltage for one specified channel
 * request: V1V 9.8, no reply
 */
void LxiComm::setTargetVoltageForChannel( unsigned int ch, float vtarget ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	// replace "n" with channel number
	size_t pos = cmd.find("n");
	cmd.replace(pos, 1, std::to_string( ch + 1 ));
	pos = cmd.find("p");
	cmd.replace(pos, 1, std::string(" ") + std::to_string( vtarget));

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
}

/**
 * set the trip voltage for a channel
 */
void LxiComm::setTripVoltageForChannel( unsigned int ch, float vtarget ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	// replace "n" with channel number
	size_t pos = cmd.find("n");
	cmd.replace(pos, 1, std::to_string( ch + 1 ));
	pos = cmd.find("p");
	cmd.replace(pos, 1, std::string(" ") + std::to_string( vtarget));

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
}

/**
 * set the trip current for a channel
 */
void LxiComm::setTripCurrentForChannel( unsigned int ch, float vtarget ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );

	// replace "n" with channel number
	size_t pos = cmd.find("n");
	cmd.replace(pos, 1, std::to_string( ch + 1 ));
	pos = cmd.find("p");
	cmd.replace(pos, 1, std::string(" ") + std::to_string( vtarget));

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
}

/**
 * set the current range to high or low
 */
void LxiComm::setCurrentRangeForChannel( unsigned int ch, bool range ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );

	// replace "n" with channel number
	size_t pos = cmd.find("n");
	cmd.replace(pos, 1, std::to_string( ch + 1 ));
	pos = cmd.find("p");
	cmd.replace(pos, 1, std::string(" ") + std::to_string( range));

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
}

/**
 * we set limit current for one specified channel
 * request: I1V 0.8, no reply
 */
void LxiComm::setLimitCurrentForChannel( unsigned int ch, float vtarget ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	// for all present channels: replace "n" with channel number
	size_t pos = cmd.find("n");
	cmd.replace(pos, 1, std::to_string( ch + 1 ));
	pos = cmd.find("p");
	cmd.replace(pos, 1, std::string(" ") + std::to_string( vtarget));
	char m_response[SZRESPONSE];
	memset (m_response, 0 , SZRESPONSE);

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
}



/**
 * we acquire the output voltages for all channels, since we cover also multichannel devices
 * request-reply for all channels in a loop, return vector
 * request: V1O?  reply: -0.611V
 */
std::vector<float> LxiComm::getOutputVoltage(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	m_reset_ack = false; // if you can get an output voltage the reset was successful and can be unset
	return(_getChannelsFloatsNoUnits(cmd));
}

/**
 * we acquire the output currents for all channels, since we cover also multichannel devices
 * request-reply for all channels in a loop, return vector
 * request: I1O?  reply: 0.103A
 */
std::vector<float> LxiComm::getOutputCurrent(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	return(_getChannelsFloatsNoUnits(cmd));
}

/**
 * we acquire the limit currents for all channels, since we cover also multichannel devices
 * request-reply for all channels in a loop, return vector
 * request: I1?  reply: 0.103A
 */
std::vector<float> LxiComm::getLimitCurrent(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	return(_getChannelsFloatsNoUnits(cmd));
}

/**
 * internal string decoding
 */
float LxiComm::_decode2ndFloatNumberWithoutUnit( std::string result ){
	size_t p = result.find_first_of(" ");
	return( std::stof( result.erase(0, p+1) ) );
}

/**
 * internal string decoding
 */
int LxiComm::_decodeIntNumber( std::string result ){
	return( std::stoi( result ) );
}

/**
 * internal string decoding
 */
float LxiComm::_decodeFloatNumberWithUnit( std::string result ){
	// must drop any trailing units as well like i.e. -0.611V
	std::size_t found = result.find_first_not_of("-+0123456789.");
	return( std::stof( result.erase(found) ) );
}

/**
 * acquire the interface status, just for fun. A real hardware reservation by locking
 * the interface is not possible, as it seems, even if such a thing would be nice.
 */
std::string LxiComm::getInterfaceStatus(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );

	char m_response[SZRESPONSE];
	memset (m_response, 0 , SZRESPONSE);

	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );

	// read reply
	int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
	if ( ret >= 0 ){
		std::string r = std::string(m_response);
		switch (_decodeIntNumber(r))
		{
		case 0: return("no active lock");
		case 1:	return("owned by " + _hostname);
		default: return("unavailable");
		}
	} else {
		return("timeout for reply");
	}
}




/**
 * enable (true) or disable (false) output of channel n
 * OPnp
 */
void LxiComm::setEnableOutputForChannel( unsigned int ch, bool flag ){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );

	std::string co = cmd;
	size_t pos = co.find("n");
	co.replace(pos, 1, std::to_string( ch + 1 ));

	pos = co.find("p");
	std::string sw;
	if ( flag ) sw = "1";
	else sw = "0";
	co.replace(pos, 1, std::string(" ") + sw);

	lxi_send( m_device, co.c_str(), strlen( co.c_str()), m_timeout_ms );
}

/**
 * acquire the enable for a given channel, from hardware
 */
bool LxiComm::getEnableOutputForChannel( unsigned int ch){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );

	std::string co = cmd;
	size_t pos = co.find("n");
	co.replace(pos, 1, std::to_string( ch + 1 ));

	lxi_send( m_device, co.c_str(), strlen( co.c_str()), m_timeout_ms );

	// read reply
	char m_response[SZRESPONSE];
	memset (m_response, 0, SZRESPONSE);
	int ret = lxi_receive( m_device, m_response, sizeof(m_response), m_timeout_ms);
	if ( ret >= 0 ){
		std::string r = std::string(m_response);
		switch (_decodeIntNumber(r))
		{
		case 0: {
			m_connected = true;
			return( 0 );
		}
		case 1:	{
			m_connected = true;
			return( 1 );
		}
		default: {
			LOG(Log::ERR) << "got a broken reply= " << m_response
					<< " id= " << la_identifier()
					<< " ip= " << la_ip()
					<< " from " << m_identifier << " assuming channel is ENABLED, continue";
			m_connected = false;
			return( 1 ); // rather safe than sorry, should NEVER happen
		}
		}
	} else {
		m_connected = false;
		return("timeout for reply");
	}
}

/**
 * get limit status event register. we just display it, but do not use it anyhow.
 */
std::vector<unsigned int> LxiComm::getLimitStatusRegister(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	return(_getChannelsIntsNoUnits(cmd));
}

/**
 * reset the PS. This can be done, it does not take a lot of time (~1 second) and potentially
 * puts a messy state back to OK. Use whenever in doubt.
 */
void LxiComm::reset(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
	m_reset_ack = true;
}

/**
 * reset the trip register for whole PS: get rid of trip indication.
 */
void LxiComm::resetTrip(){
	std::string cmd = Device::DLxiCommand::getCommandForMethod( __FUNCTION__ );
	lxi_send( m_device, cmd.c_str(), strlen( cmd.c_str()), m_timeout_ms );
}


/**
 * empty any pending replies, if there are. We just timeout after a while, so this works and potentially
 * puts communication things back into order during initialisation.
 */
void LxiComm::emptyReply(){
	char m_response[SZRESPONSE];
	memset (m_response, 0, SZRESPONSE);
	LOG(Log::INF) << "START --- empty reply queue, ignore timeouts (no timeout means there was a reply stuck)";
	lxi_receive( m_device, m_response, sizeof(m_response), 100);
	LOG(Log::INF) << "STOP --- empty reply queue";
}

