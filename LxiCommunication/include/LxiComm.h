/*
 * LxiComm.h
 *
 * communication with hardware through the lxi library and ethernet. This is basically
 * a eth/socket RAW connection with SCPI "string" commands, as they are specified by the
 * hardware vendor, nothing fancy. The upshot is that
 * the lib should be compatible to non-ethernet comms as well (according to LXi standard)
 * but the lib is not implementing that and neither do we care. The real upshot is that it
 * makes us independent from proprietary windows drivers (LXi IVI) and it is LGPL license ;-)d
 *
 *  Created on: Jun 2, 2020
 *      Author: mludwig
 */
#ifndef OPCUA_LXI_LXICOMMUNICATION_LXICOMM_H_
#define OPCUA_LXI_LXICOMMUNICATION_LXICOMM_H_

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <unistd.h>

#include <LogIt.h>


#define SZRESPONSE 65536
/**
 * slow down lxi comm, for everyone. This is a delay "per family"
 * so that certain families can go faster than snail speed.
 * delay is in ms
 */
#define M_DELAY_MS_SUPER 0
#define M_DELAY_MS_FAST 10
#define M_DELAY_MS_MEDIUM 150  // too fast for aimTTi_PL
#define M_DELAY_MS_SLOW 1000   // aimTTi_PL
#define M_DELAY_MS_SNAIL 2000 // default if family is unknown

/**
 * This class is the communication abstraction for Lxi.
 * in fact. The DPowerSupply class inherits from this class as a second inheritance
 * (constructor invocation sequence is: DPowerSupply_Base, LxiComm). All hw interaction
 * is done on the level of the power supplies only, not on the channels.
 *
 * We need some meta-knowledge about the power supplies as well, and this depends on the
 * type of each module and that is hardcoded in the server's behavior. It would also
 * make the configuration too clumsy and complex.
 *
 * Lxi communication functional abstraction: Each of these methods is a transactional
 * interaction with hw: either read only, or write only, or write and read the reply.
 *
 * The method names for harware interaction have to be identical to the names
 * which are configured in config.xml , and yes, there is a check and a fatal error if
 * you do not configure LXi properly.
 */
class LxiComm {
public:
	LxiComm();
	virtual ~LxiComm();
	int connect( std::string ip, int port );
	void emptyReply();

	/**
	 * get with reply
	 */
	void getIdentifier( std::string ipAddress );
	std::vector<float> getTargetVoltage();
	std::vector<float> getOutputVoltage();
	std::vector<float> getOutputCurrent();
	std::vector<float> getLimitCurrent();
	std::vector<float> getTripVoltage();
	std::vector<float> getTripCurrent();
	std::vector<bool> getEnableOutput();
	std::vector<bool> getCurrentRange();

	std::vector<unsigned int> getLimitStatusRegister();
	std::string getInterfaceStatus();
	bool getEnableOutputForChannel( unsigned int ch );

	/**
	 * set without reply
	 */
	void setTargetVoltageForChannel( unsigned int ch, float vtarget );
	void setLimitCurrentForChannel( unsigned int ch, float vtarget );
	void setTripVoltageForChannel( unsigned int ch, float vtarget );
	void setTripCurrentForChannel( unsigned int ch, float vtarget );
	void setCurrentRangeForChannel( unsigned int ch, bool range );

	void setEnableOutputForChannel( unsigned int ch, bool flag  );
	void reset();
	void resetTrip();


	/**
	 * public API for private fields, no hw interaction. prefixed with "la_" for "lxi abstraction"
	 */
	std::string la_ip(){ return ( m_ip ); }
	std::string la_family(){ return ( m_family ); }
	std::string la_identifier(){ return ( m_identifier ); }
	std::string la_type(){ return ( m_psType ); }
	std::string la_vendor(){ return( m_vendor); }
	std::string la_serialNumber(){ return( m_serialNumber); }
	std::string la_firmwareVersion(){ return( m_firmwareVersion); }
	unsigned int la_nbChannels(){ return ( m_nbChannels ); }
	float la_hwVmax(){ return ( m_hwVmax ); }
	float la_hwImax(){ return ( m_hwImax ); }
	void la_connected( bool f ){ m_connected = f; }
	bool la_connected(){ return ( m_connected );}
    float la_hwMaxVoltage(){ return ( m_hwVmax ); }
    float la_hwMaxCurrent(){ return ( m_hwImax ); }
	bool la_sessionOpened(){ return ( m_sessionOpened );}


    /**
     * internal methods related to reset triggers
     */
    bool _reset_ack(){ return m_reset_ack;}

	/**
	 * meta-information about  power supply types, hardcoded into a static map
	 */
	typedef struct {
		int nbChannels;
		float hwVmax;
		float hwImax;
	} METATYPES_t;
	static std::map<std::string, METATYPES_t> metatypes_map;

	static std::map<std::string, uint32_t> delays_map;

private:
	int m_device;
    bool m_connected;
	int m_timeout_ms; //ms
	std::string m_vendor;
	std::string m_identifier;
	std::string m_psType; // from search in table
	std::string m_family;
	std::string m_serialNumber;
	std::string m_firmwareVersion;
	std::map<std::string, std::string> m_familyMap;
	unsigned int m_nbChannels;
	std::string _hostname;
	float m_hwVmax;
	float m_hwImax;
	int m_port;
	std::string m_ip;
	bool m_reset_ack;
    bool m_sessionOpened;

	/**
	 * internal helper methods, no hw communication
	 */
	void _type();
	void _family();
	float _decode2ndFloatNumberWithoutUnit( std::string result );
	int _decodeIntNumber( std::string result );
	float _decodeFloatNumberWithUnit( std::string result );
	void _decodeIdentifier();

	void _increment_decrement( int deviceHandle, std::string scmd, unsigned int ch );
	std::vector<float> _getChannelsFloatsNoUnits( std::string cmd );
	std::vector<bool> _getChannelsBoolsNoUnits( std::string cmd );
	std::vector<unsigned int> _getChannelsIntsNoUnits( std::string cmd );
};

#endif /* OPCUA_LXI_LXICOMMUNICATION_LXICOMM_H_ */
