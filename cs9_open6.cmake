# @author mludwig
# Tue Jun  6 17:01:58 CEST 2017

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cs9.open6" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
# alma9 runs with 1.75.0
SET( IGNORE_DEFAULT_BOOST_SETUP "ON" )
SET( BOOST_HOME "/opt/3rdPartySoftware/boost/boost_1_81_0" )
SET( BOOST_PATH_HEADERS "/opt/3rdPartySoftware/boost/boost_1_81_0" )
SET( BOOST_PATH_LIBS "/opt/3rdPartySoftware/boost/boost_1_81_0/stage/lib" )
#----------------
# UA toolkit demo
#----------------
IF ( FALSE )
SET ( OPCUA_TOOLKIT_PATH "/opt/3rdPartySoftware/UnifiedAutomation/1.7.0/sdk" )
message( INFO " UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" ) 
add_definitions( -DBACKEND_UATOOLKIT ) 
SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamodule -lcoremodule -luapki -luabase -luastack -lxmlparser" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS_DEBUG} ) 
include_directories( ${OPCUA_TOOLKIT_PATH}/include )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uabase )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/platform )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/platform/linux )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uastack )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uaserver )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uapki )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/xmlparser )

# tookit: add empty target as flag for ua
add_custom_target ( quasar_opcua_backend_is_ready )
ENDIF()


#--------------
# open6 toolkit
#-------------- 
add_definitions( -DBACKEND_OPEN62541 )

SET( OPCUA_TOOLKIT_PATH "" )
SET( OPCUA_TOOLKIT_LIBS_RELEASE -lrt -lpthread )
SET( OPCUA_TOOLKIT_LIBS_DEBUG -lrt -lpthread )
SET (LOGIT_HAS_UATRACE FALSE)
SET (SERVERCONFIG_LOADER ON CACHE BOOL "Since quasar 1.5.1 the open62541-compat will also load ServerConfig.xml same way UA-SDK does")
include_directories( ${PROJECT_SOURCE_DIR}/open62541/include )


#-------
# liblxi
#-------
SET ( LIBLXI_PATH "/opt/3rdPartySoftware/liblxi" )
include_directories( ${LIBLXI_PATH}/include )
link_directories( ${LIBLXI_PATH}/lib )



MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

