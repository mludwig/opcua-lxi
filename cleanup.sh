#!/bin/bash
rm -rf ./CMakeFiles
rm -rf ./build
find ./ -name "*.generated.orig" -exec rm -rvf {} \;
#find ./ -name "*.orig" -exec rm -rf {} \;
#find ./ -name "cmake_*.cmake" -exec rm -rf {} \;
find ./ -name "Makefile" -exec rm -rvf {} \;
find ./ -name "CMakeCache.txt" -exec rm -rvf {} \;
find ./ -name "CMakeFiles" -exec rm -rvf {} \;
find ./ -name "*.o" -exec rm -rvf {} \;
#
# remove the copied targets
rm -rf ./RPM
rm -fv ./run/OpcUaLxiServer.*


