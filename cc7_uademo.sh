#!/bin/bash
# quasar 1.5.1 10 may 2021 ua demo
python3 ./quasar.py set_build_config ./cc7_uademo.cmake
python3 ./quasar.py disable_module open62541-compat
python3 ./quasar.py build 
python3 ./quasar.py build 

#
# copy to run
echo "===copy bins to run==="
cp -v ./build/bin/OpcUaLxiServer.uademo ./run/OpcUaLxiServer.uademo
cp -v ./build/Configuration/Configuration.xsd ./run/Configuration
# adapt the ServerConfig.xml for the toolkit
cp -v ./run/ServerConfig.ua.xml ./run/ServerConfig.xml 
cp -v ./run/ServerConfig.ua.xml ./run/ServerConfig_rpm.xml 
cp -v ./build/bin/ServerConfig.xsd ./run/Configuration
# adapt ServerConfig.xml to toolkit
cp ./run/ServerConfig.ua.xml ./run/ServerConfig.xml


