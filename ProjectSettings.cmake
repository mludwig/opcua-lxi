## -------------------------------------------------------------
## Please see http://quasar.docs.cern.ch/quasarBuildSystem.html
## for information how to use this file.
## -------------------------------------------------------------
set(CUSTOM_SERVER_MODULES LxiCommunication )
set(EXECUTABLE OpcUaLxiServer.${OPCUA_LXI_SERVER_SUFFIX})
set(SERVER_INCLUDE_DIRECTORIES  include )
set(SERVER_LINK_DIRECTORIES   )
set(SERVER_LINK_LIBRARIES )

##
## If ON, in addition to an executable, a shared object will be created.
##
set(BUILD_SERVER_SHARED_LIB OFF)

##
## Add here any additional boost libraries needed with their canonical name
## examples: date_time atomic etc.
## Note: boost paths are resolved either from $BOOST_ROOT if defined or system paths as fallback
##
set(ADDITIONAL_BOOST_LIBS )


MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] OPCUA_LXI_SERVER_SUFFIX= ${OPCUA_LXI_SERVER_SUFFIX}" )
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] EXECUTABLE= ${EXECUTABLE}" )
