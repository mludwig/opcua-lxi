# @author mludwig (c) cern

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cc7.uademo" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
#SET( IGNORE_DEFAULT_BOOST_SETUP "ON" )
SET( BOOST_HOME /opt/3rdPartySoftware/boost/boost_1_75_0 )
SET( BOOST_ROOT ${BOOST_HOME} )
SET ( BOOST_PATH_LIBS ${BOOST_HOME}/stage/lib )
SET ( BOOST_PATH_HEADERS  ${BOOST_HOME} )

#----------------
# UA toolkit demo
#----------------
SET ( TOOLKIT_EXTRA_LIBS -lpthread )
SET ( OPCUA_TOOLKIT_PATH "/opt/3rdPartySoftware/UnifiedAutomation-1.5.5/UnifiedAutomation/1.5.5/sdk" )
message( INFO " UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" ) 
add_definitions( -DBACKEND_UATOOLKIT )
SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamodule -lcoremodule -luapki -luabase -luastack -lxmlparser ${TOOLKIT_EXTRA_LIBS}" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS_DEBUG} ) 
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uabase )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uastack )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uaserver )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uapki )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/xmlparser )

# tookit: add empty target as flag for ua
add_custom_target ( quasar_opcua_backend_is_ready )

# liblxi done in toolchain

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

