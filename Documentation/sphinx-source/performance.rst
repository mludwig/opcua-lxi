===========
Performance
===========

Each PS is acquired and controlled in it's own server thread, in a polling mechanism.
A fixed sequence of reading and writing to each PS is therefore observed, which should
simplfy problem-solving. The hardware does not offer any "event subscription" mechanism.

The ethernet communication is quite fast, 1..3 ms per command usually. A polling thread 
usually executes some 20 commands, therefore one "round of polling" will be finished in under 100ms
for each channel. For power multi-channel supplies this time is per channel of course. This
hardware delay is added to the base polling frequency (600ms). The usual time for the electronics
to settle is around 500ms (the reply over ethernet is much faster).

.. doxygenclass:: QuasarServer
   :project: opcua-lxi
   :members: mainLoop, initialize
   

The server is limited (by Design.xml) to 100 PS, each having 32 channels at most. This is rather
a theoretical limit, most of the times not more than 20 PS with 1..4 channels each are expected.    

It turns out that some PS from aimTTi have a connectivity problem when more that 10 commands per 
second are sent/received: the PS looses connectivity and part of the firmware seems to crash. In
order to avoid that (waiting for a possible fw update, nov2020) the command frequency is therefore
throttled down by the server, depnding on the PS type. The client asynchronicity is kept.
This throttling is done per PS type as configured. The default is 1Hz, for unknown PS types, which is
reaaaaly slow. For the aimTTi *PL series 10Hz is hardcoded (20Hz also failed afer a few days).

.. doxygenclass:: LxiComm
   :project: opcua-lxi
   :members: 
   :undoc-members:
   