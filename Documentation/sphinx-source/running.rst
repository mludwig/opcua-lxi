=======
Running
=======

Here I descibe the **OPCUA address space (AS)** in detail and document how this works.


LxiConfiguration
================

The mapping of the hardware functionality to the LXi commands is shown under the node "aimTTi_PL" (or similar), 
for the configured hardware family. This is the configuration, named as it is configured. Each abstracted method
has an LxiCommand as a string. Values are RO as configured and can not be changed during runtime.

Each power supply
=================

.. doxygenclass:: AddressSpace::ASPowerSupply
   :project: opcua-lxi
   :members:
   :undoc-members:


Each Power Supply has it's own named node, with Channels beneath it. The fields directly beneath the power supply are:
 
Family
------
[acquisition] the hardware family, as configured.
 
HwImax
------
[acquisition] maximum current by hardware.
The exact type of the power supply is discovered through the IP number, and compared with an internal list of hardware. This internal list specifies some basic 
characteristics like maximum current the hardware of this type can provide.
 
HwVmax
------
[acquisition] maximum voltage by hardware.
 
Identifier
----------
[acquisition] the full identification string which includes type, serial number and firmware version.
 
IpAddress
---------
[acquisition] the IP address or hostname (better), as configured
 
PSMessage
---------
[acquisition] a hopefully useful message concerning the whole power supply, to help the user

PSConnected
-----------
[acquisition] bool: after a few timeouts, PSConnected is marked as "false" reconnection attempt is made. If successful, we get back to "true". 
 
Reset
-----
[setting] a software system reset to factory defaults which preserves targetvoltages, limit currents and trip settings. Use it if things seem stuck, it might save
you a trip to the installation. It is quite reliable, but of course this is a "SW nuke option" for the power suppply. The server should continue normally.
 
ResetTrip
---------
[setting] if a channel has tripped, the limit status register of the channel shows "something". This trigger will reset that register for all channels. The reset has
to be triggered by the user (just write a "true") because the detection of a trip should not be reset automatically. 
 
Type
----
[acquisition] the exact type of the PS, within the family. Types within the same family basically differ with output voltages, currents and number of channels, 
but behave the same otherwise.
 
Counter
-------
[acquisition] an activity counter for the polling cycle, which should increase during running, as a "heartbeat".
 
 
Each Channel
============


.. doxygenclass:: AddressSpace::ASChannel
   :project: opcua-lxi
   :members:
   :undoc-members:


.. doxygenclass:: Device::DChannel
   :project: opcua-lxi
   :members:
   :undoc-members:

has the fields:
 
 
CHMessage
---------
[acquisition] a hopefully useful message for the user about what is going on with the channel.
 
EnableOutput
------------
[setting] only enabled channels will give a voltage and current on the load. After a trip a channel gets disabled, and has to be enabled again.
If the trip condition is still present, the channel will disable immediately again.
 
LimitCurrent
------------
[setting] the maximum operational current allowed. If the load is consuming too much for the given target voltage, the voltage will be reduced until the current 
is within this limit: current limit mode.
 
LimitStatusRegister
-------------------
[acquisition] the limit register showing all sorts of values which do not correspond to the documentation at all, but they do seem to make some sense.
 
OutputCurrent
-------------
[acquisition] the measured output curent
 
OutputVoltage
-------------
[acquisition] the measured output voltage
 
SWMaxCurrent
------------
[acquisition] the maximum allowed setting for the limit current, which is configured for this channel. This is used to protect specific installations 
against control errors.  
 
SWMaxVoltage
------------
[acquisition] the maximum allowed setting for the target voltage, which is configured for this channel. This is used to protect specific installations 
against control errors.  
 
TripCurrent
-----------
[setting] if the outputCurrent is higher than the tripCurrent the channel will trip: outputs go to 0 and the channel is disabled. To recover,
correct the targetVoltage and/or limitCurrent or the trip settings, and switch it on again. You might also reset the resetTrip of the whole ps.

TripVoltage
-----------
[setting] if the outputVoltage is higher than the tripVoltage the channel will trip: outputs go to 0 and the channel is disabled. To recover,
correct the targetVoltage and/or limitCurrent or the trip settings, and switch it on again. You might also reset the resetTrip of the whole ps.
 
 
reconnection behavior
=====================
Once the AS is built according to the configured power supplies, the hardware is connected inside each p.s. thread. 
- If the initial connection fails then there is probably an error in the IP adress of the configuration. Nevertheless the server will try to connect until it succeeds to
the power suppliy (-ies) in question, and only then enter the infinite loop of the thread. 
- During the loop connectivity is checked and re-established if needed. 
- If connectivity is lost for a p.s. the PSMessage will show some text, accordingly, but the other AS fields
concerning this p.s. are not updated and will show "OPcUa_BadNoCommunication". 
- If a connection is lost the server can be shut down normally i.e. using ^C interactively, nevertheless the shutdown of the "hanging" power supply 
  might take some time (~60sec) due to network timeouts. Please be patient and hope for a clean shutdown which also notifies all clients properly.

 
 
