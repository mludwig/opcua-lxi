.. _contents:

OPCUA-LXi quasar server user documentation
==========================================

The opcua-lxi is an OPCUA server written in C++ using quasar, and it serves as a OPCUA<->LXi power supply ethernet bridge. 
Since the LXi hardware communication layer can be configured for each server instance this server is potentially compatible
to a wide range of LXi standard power supplies.
The server is available for two OPCUA toolkits runnin under CERN Linux: 
  - .ua (Unified Automation, license needed) and 
  - .open6 (open62541 from Fraunhofer Institute, free)
    
.. toctree::
   :maxdepth: 3

   quickstart
   overview
   configuration
   performance
   building
   addressspace
   status
   connection
   running
   support


alphabetical index
------------------

.. doxygenindex::
   :project: opcua-lxi
   :path: ...
   :outline:
   :no-link:



* :ref:`genindex`

.. * :ref:`search`
.. * :ref:`modindex`


This documentation is made using `sphinx`_ , `breathe_`_ and `doxygen`_ .

.. _sphinx: https://www.sphinx-doc.org/en/master/
.. _breathe: https://breathe.readthedocs.io/en/latest/index.html
.. _doxygen: https://www.doxygen.nl/index.html
