============
AddressSpace
============

Here I descibe the **OPCUA address space (AS)** in detail and document how this works.


LxiConfiguration
================

The mapping of the hardware functionality to the LXi commands is shown under the node "aimTTi_PL" (or similar), 
for the configured hardware family. These are the command LXI strings for the hardware abbstraction as they are configured.
It is strictly speaking not neccessary to show them in the AS at all (they are configured), but for a technical
reason it is easier to show them as they are. Values are RO and can not be changed during runtime.

Power Supply Interface
======================

Here is the full AS interface for the power supplies:

.. doxygenclass:: AddressSpace::ASPowerSupply
   :project: opcua-lxi
   :members:
   :undoc-members:


Power Supply AS
===============

Each Power Supply has it's own named node, with Channels beneath it. The fields directly beneath the power supply are:


Counter
-------
**[acquisition]** a looping counter which shows the polling activity. It should increase during running, as a "heartbeat".
 
Family
------
**[acquisition]** the hardware family, as configured, for the Lxi Commands

FirmwareVersion
---------------
**[acquisition]**
the firmware version string
 
HwImax
------
**[acquisition]** maximum current by hardware.
The exact type of the power supply is discovered through the IP number, and compared with an internal list of hardware. This internal list specifies some basic 
characteristics like maximum current the hardware of this type can provide.
 
HwVmax
------
**[acquisition]** maximum voltage by hardware.
 
Identifier
----------
**[acquisition]** the full identification string which includes type, serial number and firmware version.
 
IpAddress
---------
**[acquisition]** the IP address, as configured, or houstname (preferred)
 
PSMessage
---------
**[acquisition]** a hopefully useful message concerning the whole power supply, to help the user
 
Reset
-----
**[setting]** a software system reset to factory defaults which preserves targetvoltages, limit currents and trip settings. Use it if things seem stuck, it might save
you a trip to the installation. It is quite reliable, but of course this is a "SW nuke option" for the power suppply. The server should continue normally.
 
ResetTrip
---------
**[setting]** if a channel has tripped, the limit status register of the channel shows "something". This trigger will reset that register for all channels. The reset has
to be triggered by the user (just write a "true") because the detection of a trip should not be reset automatically. 
 
SerialNumber
------------
**[acquisition]** the serial number of the module as a string (yes, it might have characters as well so its not a numerical number, generally)

Type
----
**[acquisition]** the exact type of the PS, within the family. Types within the same family basically differ with output voltages, currents and number of channels, 
but behave the same otherwise.
 
Vendor
------
**[acquisition]** the producer of the module
 
 
Channel Interface
=================

Here is the complete footprint for each channel node:

.. doxygenclass:: AddressSpace::ASChannel
   :project: opcua-lxi
   :members:
   :undoc-members:

Channel AS
==========

The AS has the fields:
 
CHMessage
---------
**[acquisition]** a hopefully useful message for the user about what is going on with the channel.

CurrentRange
------------
**[setting]** the range can be "high"(true) or "low" (false), whateever this means exactly has to be looked up in the technical manual. 
High usuually means more current but less precision and vice versa.
 
EnableOutput
------------
**[setting]** only enabled channels will give a voltage and current on the load. After a trip a channel gets disabled, and has to be enabled again.
If the trip condition is still present, the channel will disable immediately again.
 
LimitCurrent
------------
**[setting]** the maximum operational current allowed. If the load is consuming too much for the given target voltage, the voltage will be reduced until the current 
is within this limit: current limit mode.
 
LimitStatusRegister
-------------------
**[acquisition]** the limit register showing all sorts of values which do not correspond to the documentation at all, but they do seem to make some sense.
 
OutputCurrent
-------------
**[acquisition]** the measured output curent of this channel
 
OutputVoltage
-------------
**[acquisition]** the measured output voltage of this channel
 
SWMaxCurrent
------------
**[acquisition]** the maximum allowed setting for the limit current, which is configured for this channel. This is used to protect specific installations 
against operator errors.  
 
SWMaxVoltage
------------
**[acquisition]** the maximum allowed setting for the target voltage, which is configured for this channel. This is used to protect specific installations 
against operator errors.  
 
TripCurrent
-----------
**[setting]** if the outputCurrent is higher than the tripCurrent the channel will trip: outputs go to 0 and the channel is disabled. To recover,
correct the targetVoltage and/or limitCurrent or the trip settings, and switch it on again. You might also reset the resetTrip of the whole ps.

TripVoltage
-----------
**[setting]** if the outputVoltage is higher than the tripVoltage the channel will trip: outputs go to 0 and the channel is disabled. To recover,
correct the targetVoltage and/or limitCurrent or the trip settings, and switch it on again. You might also reset the resetTrip of the whole ps.
 
 
reconnection behavior
=====================
Once the AS is built according to the configured power supplies, the hardware is connected inside each p.s. thread. 
- If the initial connection fails then there is probably an error in the IP adress of the configuration. Nevertheless the server will try to connect until it succeeds to
the power suppliy (-ies) in question, and only then enter the infinite loop of the thread. 
- During the loop connectivity is checked and re-established if needed. 
- If connectivity is lost for a p.s. the PSMessage will show some text, accordingly, but the other AS fields
concerning this p.s. are not updated and will show "OPcUa_BadNoCommunication". 

 
 
