==========
Connection
==========

see https://its.cern.ch/jira/browse/ENS-32211 for full details of the discussion.
The PLH series from aimTTi behaves in 2 ways when the connection is lost, firmware 1.04-4.06

a-the network is lost
---------------------
but power stays ON, network comes back. The network socket (tcp/ip) with it's lxi session is NOT reconnected on the PLH side, even when a lxi_disconnect, lxi_connect is made. The new session blocks on a mutex on the opcua-lxi server side tcp/ip kernel layer), which means basically that the PLH does not reply. After a long (unknown) time the PLH might drop the session and wait for a new request. Is there anything in the manual about such a session-expiry delay? If the opcua-lxi makes a new session request when the PLH is not ready then the server thread for that PLH concerned is stuck forever on the (tcp/ip-) session mutex.

If the opcua-lxi server ignores transmissison errors and just KEEPS the session it has, the communication with the PLH comes back after about 30secs when network is restored

b-power is lost
---------------
power comes back after some time. After power-up the PLH expects a new session request. Any previous session is invalid on the stack and can not be used on the opcua-lxi server. This is what the server does ONCE at init in all cases.

solution
--------
The opcua-lxi server can not distinguish between a and b. The difference in the code is not a big deal, but all I can do is to add a configuration (a or b to choose) for the reconnection behavior. If the PLH looses connection with (b) and opcua-lxi is configured (a) this will not work, and vice-versa. The behavior has to match on both sides.

A final fix would be to put the PLH (and all others) behind PDUs so that they can be remotely power cycled, and that we always have (b). I can even offer a opcua-pdu server, plus the WinCC-OA integration, for that. but the PDUs all have a web interface as well. I recommend the GUDE 8031, it is cheap, reliable and simple.


The current version of the opcua-lxi has (a) implemented. If the host computer for the opcua-lxi server is on the same 220V than the PLHs, then you need only (a): if there is a power loss the server will init anyway again. If there is a network loss the server will keep the socket and NOT close the lxi session: since the PS does not manage closing sessions either this will work in many cases

The two fields PSconnected and HWconnected reflect this:

PSconnected
-----------
true = if a fresh acquisition has been made within a fixed time interval (currently 50sec) AND the lxi has a valid socket.
false = if no fresh acquisition is made or lxi does not have a valid socket (or both).


.. doxygenclass:: PSconnected
   :project: opcua-lxi

HWconnected
-----------
true = lxi has a valid socket
false = lxi fails on a socket

- The HWconnected is unrelieable (the aimTTi do not manage closing sockets correctly) and is only present for information.
- The PSconnected must be used for production.

.. doxygenclass:: HWconnected
   :project: opcua-lxi


