========
Overview
========

The OPCUA LXi quasar server uses the `Lxi`_ standard to communicate with ethernet controlled power supplies. LXi is an industry standard
which is controlled by a consortium, so that LXi compatible devices can acquire LXi certification. LXi (created in 2004) 
is a successor to GPIB, VXI and PXI meaning "ethernet communication with instrument extension". LXi defines obligatory
behavior and some technical components as required: internal web server and windows drivers of a certain type (ivi). The LXi 
specification itself is a 100+ page document with prescriptions, but it does not contain technical implementation in the first place.
LXi compatible devices can be power supplies, spectrum analyzers, function generators, anything really.

In order to be compatible with as many types of LXi power supplies as possible the server's hardware communication layer is configurable.
The mapping of LXi commands to basic hardware functionality can be configured according to the hardware family needs, for a server instance.

Presently the hardware from aimTTi PL series is tested ( `aimttipl`_ , `aimttiplh`_ and `aimttl_cpx`_). The `aimTTi`_ TSX and LD series
are quite similar, therefore the server should be configurable for compatibility to those as well, and potentially many
other power supplies also from other vendors may be configurable. CERN's electronic pool owns some 200 aimTTi modules.

Even though the C++ code is built using the cmake toolchain, and therefore the build chain is compatible with windows, the `liblxi`_ 
communication core is only available under linux. For windows specific `ivi`_ drivers are available from the vendors, as it is obligatory 
for any LXi compatible hardware. This server is available only under linux presently, and since it acts as an "ethernet OPCUA-LXi bridge" 
over the network there are no strong OS-related constraints and a linux-only server is sufficient. If the LXi `ivi`_ drivers for windows 
are integrated instead of the liblxi then a windows version will also be possible, but is presently out of scope. 

A server instance is set up with the proper ip adresses of the power supplies, discovers the type and family of the electronics
and builds the OPCUA adress space (AS) on it's own.  

.. _Lxi: https://www.lxistandard.org
.. _aimttipl: https://www.aimtti.com/product-category/dc-power-supplies/aim-plseries
.. _aimttiplh: https://www.aimtti.com/product-category/dc-power-supplies/aim-plhseries
.. _aimtticpx: https://www.aimtti.com/product-category/dc-power-supplies/aim-cpxseries

.. _aimtti: https://www.aimtti.com

.. _liblxi: https://github.com/lxi-tools/liblxi
.. _ivi: https://www.lxistandard.org/About/IVI-Drivers-IVI-Foundation-for-LXI-Instruments.aspx
