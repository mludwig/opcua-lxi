==========
Quickstart
==========

Let's assume you know what you want and have a configuration in mind for running the OPCUA LXi server. 

- your LXi power supply is networking correctly and it's webserver can be accessed, looking like i.e. that:

.. image:: images/aimtti-plh250-p.png

- you have a host machine for the server, running cc7, where you can be root. An openstack VM is perfectly fine as well.
- download the RPM from `pipeline`_  stage "build" job "build_ua"
- install like: sudo rpm -iv OpcUaLxiServer-<version>.ua.x86_64.rpm
- alternatively also the .open6 toolkit can be used
- overwrite the /opt/OpcUaLxiServer/bin/config.xml to reflect your installation
- start the server: cd  /opt/OpcUaLxiServer/bin && ./OpcUaLxiServer 
- test the OPCUA adress space (AS) using uaexpert, it should look like i.e.:

The resulting adress space (AS) for this power supply, seen with uaexpert, will i.e. look like:

.. image:: images/as-plh250-p.png


You just need to call the binary without any options: you will get an adress space which you can explore with 
uaexpert or your clients (WinCC-OA).

.. _pipeline: https://gitlab.cern.ch/mludwig/opcua-lxi/-/pipelines


Installation using RPM
----------------------

- install xerces-3.2 on your system
	- download RPM from: https://rhel.pkgs.org/9/epel-x86_64/xerces-c-3.2.5-1.el9.x86_64.rpm.html (follow link)
	- sudo dnf install ./xerces-c-3.2.5-1.el9.x86_64.rpm
- download the RPM for your OS and UA toolkit from: https://jcop.web.cern.ch/pages/opc-hardware-simulation.html
	i.e. https://edms.cern.ch/file/2907023/1.0.18/OpcUaLxiServer.cal9.open6-1.0.18.cal9.open6.x86_64.rpm
- sudo dnf install ./OpcUaLxiServer.cal9.open6-1.0.18.cal9.open6.x86_64.rpm

then, cd /opt/.../run and call the server. This will read ./config.xml and ServerConfig.xml and check against the xsd in ./Configuration/Cpnfiguration.xsd and ./Configuration/ServerConfig.xsd
edit the ./config.xml to your needs

 

