========
Building
========

.. get supported languages for code-blocks with: pygmentize -L lexers
.. this shows a huuuge amount of languages available, I choose basemake 
.. for cmake stuff

We use cmake 3.0 or higher for cc7 building.
The dependencies are:

* xerces-c
* boost (1.73.0 preferred, but any version >= 1.53.x should work)
* lxilib (CC7)

**These dependencies should conveniently be injected into cmake using a toolchain file:**

.. code-block:: basemake

	CMakeLists.txt:
	---------------
	(...other stuff)
	#
	# Load build toolchain file and internal checks before we start building
	#
	if( DEFINED CMAKE_TOOLCHAIN_FILE )
		message( STATUS "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: CMAKE_TOOLCHAIN_FILE is defined -- including [${CMAKE_TOOLCHAIN_FILE}]")
		include( ${CMAKE_TOOLCHAIN_FILE} )    
	endif()
	(...other stuff)

**The toolchain file would look like this (cc7):**

.. code-block:: basemake

	toolchain.cmake:
	----------------
	# toolchain for CANX-tester for CI jenkins, w10e
	# mludwig at cern dot ch
	# cmake -DCMAKE_TOOLCHAIN_FILE=jenkins_CanModule_w10e.cmake .
	

**The toolchain gets then injected by running cmake:**

.. code-block:: basemake

	cmake -DCMAKE_TOOLCHAIN_FILE=toolchain.cmake
	
	
