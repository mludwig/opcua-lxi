======
Status
======

StatusPowerSupply
=================

The status of a power supply is reported as a unsigned int bitpattern. Additionally, the single bits are represented as bool fields in a subclass
for convenience. The bits mean: 

.. doxygendefine:: STATUS_PS_CH_ON
   :project: opcua-lxi
   
.. doxygendefine:: STATUS_PS_OVVS
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_OVVH
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_UNV
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_OVCS
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_OVCH
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_TRIP
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_TRIPV
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_TRIPC
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_NOC
   :project: opcua-lxi

.. doxygendefine:: STATUS_PS_CC
   :project: opcua-lxi
   
.. doxygendefine:: STATUS_PS_RESET_ACK
   :project: opcua-lxi
   
.. doxygendefine:: STATUS_PS_RESET_SUCCESS
   :project: opcua-lxi
   
   
StatusChannel
=============

Each channel has a status as well, also with single bits decoded for convenience. The meaninga are slightly different and the bits are not at the same positions compared to the power supply status.
The bits mean:

.. doxygendefine:: STATUS_CH_ON
   :project: opcua-lxi
   
.. doxygendefine:: STATUS_CH_TRIPV
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_TRIPC
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_CC
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_OVVS
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_OVVH
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_UNV
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_OVCS
   :project: opcua-lxi

.. doxygendefine:: STATUS_CH_OVCH
   :project: opcua-lxi

  
   
