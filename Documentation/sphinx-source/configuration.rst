=============
Configuration
=============

The server configuration, ServerConfig.xml, can stay at it's quasar defaults. To be noted there that the endpoint port
is configured by default to 23600 (for both .ua and .open6 toolkits).

The instance configuration, config.xml, defines for what hardware the server will produce an OPCUA adress space (AS).
Essentially this comes in two parts: LXi abstraction configuration and Power-Supply configuration. Each server instance runs 
one LXI abstraction.

LXi abstraction
===============
The server defines a set of core functionality methods, which correspond to the things a normal remote power supply
will be able to do. Each of these methods has a specific LXi command (a string) and maybe also a reply (also a string).
The LXi command for each of these methods has to be configured for the server: one configures the LXi-command mapping to 
functionality for a certain family of power supplies which all share the same command set. One can not mix differrent
families in the same server instance (or AS). One server runs for one family, of several families are needed then
several server instances have to be configured, on for each family.

Here is an example of such a configuration for the aim_TTi family of power supplies. The abstraction is also called
like this, but any other name can be given.

.. code-block:: xml

   <LxiAbstraction name="aimTTi_PL">
   <!-- command strings for LXI operation. The string contains also lowercase n and/or p 
   n : a channel
   p : a parameter 
   -->
   <!-- command has a reply, but no channel, no parameter -->
   <LxiCommand name="getIdentifier" Command="*IDN?"/>
   <LxiCommand name="getInterfaceStatus" Command="IFLOCK?"/>

   <!-- command has a reply and a channel, but no parameter -->
   <LxiCommand name="getLimitCurrent" Command="In?"/>
   <LxiCommand name="getOutputCurrent" Command="InO?"/>
   <LxiCommand name="getCurrentRange" Command="IRANGEn?"/>
   <LxiCommand name="getTripVoltage" Command="OVPn?"/>
   <LxiCommand name="getTripCurrent" Command="OCPn?"/>
   <LxiCommand name="getEnableOutput" Command="OPn?"/>
   <LxiCommand name="getTargetVoltage" Command="Vn?"/>
   <LxiCommand name="getOutputVoltage" Command="VnO?"/>
   <LxiCommand name="getLimitStatusRegister" Command="LSRn?"/>
   <LxiCommand name="getLimitEventRegister" Command="LSEn?"/>
   
   <!-- command has no reply, but has a channel and a parameter -->
   <LxiCommand name="setTargetVoltageForChannel" Command="Vnp"/>
   <LxiCommand name="setLimitCurrentForChannel" Command="Inp"/>
   <LxiCommand name="setTripVoltageForChannel" Command="OVPnp"/>
   <LxiCommand name="setTripCurrentForChannel" Command="OCPnp"/>
   <LxiCommand name="setEnableOutputForChannel" Command="OPnp"/>
   <LxiCommand name="setCurrentRangeForChannel" Command="IRANGEnp"/>
   
   <!-- command has no reply and no channel and no parameter -->
   <LxiCommand name="clearStatus" Command="*CLS"/>
   <LxiCommand name="reset" Command="*RST"/>
   <LxiCommand name="resetTrip" Command="TRIPRST"/>
   </LxiAbstraction>  

aimTTi PL series
----------------
the configuration as above will work

aimTTi CPX series
-----------------
the line from above
   <LxiCommand name="getCurrentRange" Command="IRANGEn?"/>
has to be replaced by
   <LxiCommand name="getCurrentRange" Command="none"/>


Lxi Commands
------------

The actual commands come in several flavours, depending on whether the command

- has a reply
- has a parameter (usually a float)
- a channel (an int [1...32]).

The flavours are the possible permutations of these three charateristics. In order to ease
the configuration nevertheless and keep things simple an internal convention for this server
is defined:

- a lowercase <n> denominates a channel
- a lowercase <p> denominates a parameter, which is separated by a leading whitespace .

Therefore a command "set trip voltage to 5.5 for channel 1", which is configured as "OVPnp"
gets internally expanded to "OVP1 5.5" when sent to the hardware. This command does not expect
a reply 


core methods
------------

The LXi-configurable functional methods are members of class LxiComm, which is a configurable hardware communication layer. These methods
represent an abstracted behaviour of a power supply.

.. doxygenclass: LxiComm
   :project: opcua-lxi
   :members:
   
   
Power-Supply
============

The specific power supplies are just configured using their IP adress. Additionally, the channels have to be 
named and indicated, and also software limits for voltage and current for each channel (this is a protection 
against operator error). Units are of course SI (Volts, Amps).

** Note the naming convention for the channel names: ChanXXX **

.. code-block:: xml

   <PowerSupply name="na62-straw5"  ipAddress="beicsfdplh250test1" port="9221">
      <Channel name="Chan001" channel="1" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01"/>
   </PowerSupply>  
   <PowerSupply name="na62-detector1"  ipAddress="beicsfdplh250test2" port="9221">
      <Channel name="Chan001" channel="1" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01"/>
   </PowerSupply>
   <PowerSupply name="na62-detector2"  ipAddress="beicsfdpl303qmdtest3" port="9221">
      <Channel name="Chan001" channel="1" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01"/>
      <Channel name="Chan002" channel="2" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01"/>
   </PowerSupply>


example config.xml
==================

Here is a complete example file, which has to be adapted for each instance according to the needs. Also remember to
specify the software limits for each channel, and remember the "ChanXXX" naming convention.

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8"?>
   <configuration xmlns="http://cern.ch/quasar/Configuration" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://cern.ch/quasar/Configuration ./Configuration/Configuration.xsd ">
   <StandardMetaData>
      <Log>
         <GeneralLogLevel logLevel="DBG"/>
      </Log>
      <SourceVariableThreadPool maxThreads="10" minThreads="1" maxJobs="15"/>
      <Server></Server>
   </StandardMetaData>

   <LxiAbstraction name="aimTTi_PL">

   <!-- command strings for LXI operation. The string contains also lowercase n and/or p 
   n : a channel
   p : a parameter 
   -->

   <!-- command has a reply, but no channel, no parameter -->
   <LxiCommand name="getIdentifier" Command="*IDN?"/>
   <LxiCommand name="getInterfaceStatus" Command="IFLOCK?"/>

   <!-- command has a reply and a channel, but no parameter -->
   <LxiCommand name="getLimitCurrent" Command="In?"/>
   <LxiCommand name="getOutputCurrent" Command="InO?"/>
   <LxiCommand name="getCurrentRange" Command="IRANGEn?"/>
   <LxiCommand name="getTripVoltage" Command="OVPn?"/>
   <LxiCommand name="getTripCurrent" Command="OCPn?"/>
   <LxiCommand name="getEnableOutput" Command="OPn?"/>
   <LxiCommand name="getTargetVoltage" Command="Vn?"/>
   <LxiCommand name="getOutputVoltage" Command="VnO?"/>
   <LxiCommand name="getLimitStatusRegister" Command="LSRn?"/>
   <LxiCommand name="getLimitEventRegister" Command="LSEn?"/>
      
   <!-- command has no reply, but has a channel and a parameter -->
   <LxiCommand name="setTargetVoltageForChannel" Command="Vnp"/>
   <LxiCommand name="setLimitCurrentForChannel" Command="Inp"/>
   <LxiCommand name="setTripVoltageForChannel" Command="OVPnp"/>
   <LxiCommand name="setTripCurrentForChannel" Command="OCPnp"/>
   <LxiCommand name="setEnableOutputForChannel" Command="OPnp"/>
   <LxiCommand name="setCurrentRangeForChannel" Command="IRANGEnp"/>

   <!-- command has no reply and no channel and no parameter -->
   <LxiCommand name="clearStatus" Command="*CLS"/>
   <LxiCommand name="reset" Command="*RST"/>
   <LxiCommand name="resetTrip" Command="TRIPRST"/>
   </LxiAbstraction>
 
   <PowerSupply name="plh250_test1"  ipAddress="beicsfdplh250test1" port="9221">
	<Channel name="Chan001" channel="1" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01">
		<StatusCh name="StatusChannel"/>
	</Channel>
	<StatusPs name="StatusPowerSupply"/>
   </PowerSupply>  
   <PowerSupply name="plh250_test2"  ipAddress="beicsfdplh250test2" port="9221">
	<Channel name="Chan001" channel="1" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01">
		<StatusCh name="StatusChannel"/>
	</Channel>
	<StatusPs name="StatusPowerSupply"/>
   </PowerSupply>
   <PowerSupply name="pl303qmd_test3"  ipAddress="beicsfdpl303qmdtest3" port="9221">
	<Channel name="Chan001" channel="1" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01">
		<StatusCh name="StatusChannel"/>
	</Channel>
	<Channel name="Chan002" channel="2" SoftwareMaxVoltage="20" SoftwareMaxCurrent="0.01">
		<StatusCh name="StatusChannel"/>
	</Channel>
	<StatusPs name="StatusPowerSupply"/>
   </PowerSupply>

   </configuration>




