var structUA__Client =
[
    [ "asyncConnectCall", "structUA__Client.html#aa097c1ce985bb5453f4d2782b72cc647", null ],
    [ "authenticationToken", "structUA__Client.html#a1c37bcff0440b93abceb45cf1b94e78f", null ],
    [ "channel", "structUA__Client.html#a1336cc6f1cbd92b86cbae08c46026aa4", null ],
    [ "config", "structUA__Client.html#a795b43f4f166aba13d181682d5e6869b", null ],
    [ "connection", "structUA__Client.html#aaa8b69a4bb9b67c96dfa2835a61058a7", null ],
    [ "connectStatus", "structUA__Client.html#a7f79d239a0243991a20cbf8e5987901f", null ],
    [ "endpointsHandshake", "structUA__Client.html#a239e0baa154608e59a75e98f95b95a64", null ],
    [ "endpointUrl", "structUA__Client.html#a0e043658b44e6f3fb405a316d42208b2", null ],
    [ "nextChannelRenewal", "structUA__Client.html#a485985852226f7d9becf62cb680505dd", null ],
    [ "requestHandle", "structUA__Client.html#acbaf11a1e7750f81a80ef1291d01941a", null ],
    [ "requestId", "structUA__Client.html#a05f0dc16990effd6b7fca6bc3f6bb636", null ],
    [ "state", "structUA__Client.html#a7daf1c4b6bce32e14991ed008795d58c", null ],
    [ "timer", "structUA__Client.html#a019e1df2378cdd0fd6f7ea62f674ed39", null ]
];