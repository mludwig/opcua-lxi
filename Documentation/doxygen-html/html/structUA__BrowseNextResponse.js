var structUA__BrowseNextResponse =
[
    [ "diagnosticInfos", "structUA__BrowseNextResponse.html#af051fc4c7bd79a2c7b756fdacc0691ac", null ],
    [ "diagnosticInfosSize", "structUA__BrowseNextResponse.html#af56e7c8ae7a700602962b8e4863f4209", null ],
    [ "responseHeader", "structUA__BrowseNextResponse.html#a52c67ed5c296a8b685986f0e0ac5faf5", null ],
    [ "results", "structUA__BrowseNextResponse.html#a4549ab177c9074288b9558aa1378e8a2", null ],
    [ "resultsSize", "structUA__BrowseNextResponse.html#a6ec9f7e342b62b31afe1fdcde93bdb70", null ]
];