var structUA__NodeAttributes =
[
    [ "description", "structUA__NodeAttributes.html#afa65c95c945d36c127705447d106edfb", null ],
    [ "displayName", "structUA__NodeAttributes.html#ae8fcefcb2d9dacd4b6a2cb1772846489", null ],
    [ "specifiedAttributes", "structUA__NodeAttributes.html#afb9c2209c714421a37a483a7b7d65619", null ],
    [ "userWriteMask", "structUA__NodeAttributes.html#a5670bfd57c557d76591f084aa5225b79", null ],
    [ "writeMask", "structUA__NodeAttributes.html#a775074a93c8d995fc24a44dd1d8b1f30", null ]
];