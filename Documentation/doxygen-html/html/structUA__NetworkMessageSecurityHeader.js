var structUA__NetworkMessageSecurityHeader =
[
    [ "forceKeyReset", "structUA__NetworkMessageSecurityHeader.html#ae52ff1af980e6ee014c9514ffac985e6", null ],
    [ "messageNonce", "structUA__NetworkMessageSecurityHeader.html#a7557b4c72c1634ca122282549a0eaa57", null ],
    [ "networkMessageEncrypted", "structUA__NetworkMessageSecurityHeader.html#ab3df2d4a9357cad147e0b933eca306ca", null ],
    [ "networkMessageSigned", "structUA__NetworkMessageSecurityHeader.html#ab9e6eb917dd16840eb4e3776b3550210", null ],
    [ "nonceLength", "structUA__NetworkMessageSecurityHeader.html#a7dd57c2d3770c0d1312b73b02b1d2745", null ],
    [ "securityFooterEnabled", "structUA__NetworkMessageSecurityHeader.html#ad26a491c70a84a95177640ff4c7732c7", null ],
    [ "securityFooterSize", "structUA__NetworkMessageSecurityHeader.html#a83c2cb3acd92adc34a4ef03baa7d6577", null ],
    [ "securityTokenId", "structUA__NetworkMessageSecurityHeader.html#a39478a62634e69b75dc428d2f6ae2f02", null ]
];