var structUA__GetEndpointsRequest =
[
    [ "endpointUrl", "structUA__GetEndpointsRequest.html#ad3e17d8d39060d584ae8dc255533b613", null ],
    [ "localeIds", "structUA__GetEndpointsRequest.html#add036eb92edf8da9b66dfe1c6f2527dc", null ],
    [ "localeIdsSize", "structUA__GetEndpointsRequest.html#acd00dcaf05642a6e644a69b1aacfb57a", null ],
    [ "profileUris", "structUA__GetEndpointsRequest.html#adbdadde2e6b238e90297876427761de1", null ],
    [ "profileUrisSize", "structUA__GetEndpointsRequest.html#aba48cff644b2846e621f5f28d256c931", null ],
    [ "requestHeader", "structUA__GetEndpointsRequest.html#abe219fc80a7155cd32579d6ec1c31e3b", null ]
];