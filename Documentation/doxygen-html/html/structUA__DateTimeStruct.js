var structUA__DateTimeStruct =
[
    [ "day", "structUA__DateTimeStruct.html#a82fb098716be9bc33435231438762041", null ],
    [ "hour", "structUA__DateTimeStruct.html#a7c5113ba97db7d2635d83c5c54bc74a9", null ],
    [ "microSec", "structUA__DateTimeStruct.html#afb39870aa4d539fb90509c9eae8135dd", null ],
    [ "milliSec", "structUA__DateTimeStruct.html#a2b9bc315b550e8c18e302ab509d1fbb6", null ],
    [ "min", "structUA__DateTimeStruct.html#a37178e566b792d203f89e6900ba4ddaf", null ],
    [ "month", "structUA__DateTimeStruct.html#a0071e919885386bca4d271c955f19dec", null ],
    [ "nanoSec", "structUA__DateTimeStruct.html#adc3e26d11f113d08316c246159c2d227", null ],
    [ "sec", "structUA__DateTimeStruct.html#ae9d6a8dad499bbee624330fa68c3423c", null ],
    [ "year", "structUA__DateTimeStruct.html#a3d64e15b369ce30c03ff47b8df2403c3", null ]
];