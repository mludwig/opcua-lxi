var structUA__ActivateSessionResponse =
[
    [ "diagnosticInfos", "structUA__ActivateSessionResponse.html#a90190fd8efbf79011cf873ae99309710", null ],
    [ "diagnosticInfosSize", "structUA__ActivateSessionResponse.html#abc266dc55daab2e813a63ed1dd49daad", null ],
    [ "responseHeader", "structUA__ActivateSessionResponse.html#aff76ec629553dc3e14c79acd0881afc9", null ],
    [ "results", "structUA__ActivateSessionResponse.html#a5f476cb66bd5ee9345b36a2a08d7fc15", null ],
    [ "resultsSize", "structUA__ActivateSessionResponse.html#a996160233ff62600e49d8ea527bfc93f", null ],
    [ "serverNonce", "structUA__ActivateSessionResponse.html#a7e6a7c00b9594bf7e1b2b1d01d88d737", null ]
];