var structUA__TcpAcknowledgeMessage =
[
    [ "maxChunkCount", "structUA__TcpAcknowledgeMessage.html#a21309024f776a02f44817dedc89f97cf", null ],
    [ "maxMessageSize", "structUA__TcpAcknowledgeMessage.html#aeef37538c04dd30f5395fc858ae7b51a", null ],
    [ "protocolVersion", "structUA__TcpAcknowledgeMessage.html#a3a886d3ba01b389ec9c18342c58afc8f", null ],
    [ "receiveBufferSize", "structUA__TcpAcknowledgeMessage.html#ac8cd371b9716a990fc049d40d4c0f897", null ],
    [ "sendBufferSize", "structUA__TcpAcknowledgeMessage.html#ac5ee1492cc1b0fc8cd49660c6afc07e5", null ]
];