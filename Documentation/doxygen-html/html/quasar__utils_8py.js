var quasar__utils_8py =
[
    [ "amalgamate", "quasar__utils_8py.html#a59c92ebfb850a45aacfef7fe44a88926", null ],
    [ "args", "quasar__utils_8py.html#aacfc1669860eff35f14d1ab8eeb2df8d", null ],
    [ "func", "quasar__utils_8py.html#a728f0c951adde7b27eb5783b846a966b", null ],
    [ "help", "quasar__utils_8py.html#aec3d0e77530125fbb8461e502c805e30", null ],
    [ "nargs", "quasar__utils_8py.html#a990ed46d7db7e339f5e8b2bfb31ee578", null ],
    [ "parser", "quasar__utils_8py.html#ad71157e724c01f58d679851f91df039b", null ],
    [ "parser_amalgamate", "quasar__utils_8py.html#aca7bddc871899d496b25167349d4ce12", null ],
    [ "str", "quasar__utils_8py.html#af7f6c88fefcb5c6fb28a87b56b684fe1", null ],
    [ "subparsers", "quasar__utils_8py.html#a099b62a3d1708d16e3aa3f7f40f8270c", null ],
    [ "type", "quasar__utils_8py.html#a7952d7b24aee0310f43adcc578c5f661", null ]
];