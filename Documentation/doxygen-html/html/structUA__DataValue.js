var structUA__DataValue =
[
    [ "hasServerPicoseconds", "structUA__DataValue.html#ae05b644e0d350d7e38c8eb1583a68b55", null ],
    [ "hasServerTimestamp", "structUA__DataValue.html#a5e0ea1cf32c085085722370da0960970", null ],
    [ "hasSourcePicoseconds", "structUA__DataValue.html#a09a0c6d09e75f20fc02774751bbba988", null ],
    [ "hasSourceTimestamp", "structUA__DataValue.html#ac14d24ddfaff6540055bcab42a2c889d", null ],
    [ "hasStatus", "structUA__DataValue.html#ab9103bff620cafc90337c7d98b119b2e", null ],
    [ "hasValue", "structUA__DataValue.html#af7a6da643af31005029ad3759deae136", null ],
    [ "serverPicoseconds", "structUA__DataValue.html#a6012a45a2d1c61d611cd86556fdb97a3", null ],
    [ "serverTimestamp", "structUA__DataValue.html#a72156c581820c38b5eaf22bf13000050", null ],
    [ "sourcePicoseconds", "structUA__DataValue.html#a878dae470cc6c4af7442b666144c598e", null ],
    [ "sourceTimestamp", "structUA__DataValue.html#ab7348f73003634fac46f91fb26935d54", null ],
    [ "status", "structUA__DataValue.html#a96804144fa60cfc250d5b0085148e588", null ],
    [ "value", "structUA__DataValue.html#ab18e898e2ee5478aa0cd295f3357841b", null ]
];