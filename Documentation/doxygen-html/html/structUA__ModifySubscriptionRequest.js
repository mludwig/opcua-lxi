var structUA__ModifySubscriptionRequest =
[
    [ "maxNotificationsPerPublish", "structUA__ModifySubscriptionRequest.html#a419f92d9aa0b3a91503275569ab9f57a", null ],
    [ "priority", "structUA__ModifySubscriptionRequest.html#a6e62ebac4f20db48b5abe587f6d89663", null ],
    [ "requestedLifetimeCount", "structUA__ModifySubscriptionRequest.html#a46bae621ded6293a22e6ab818f0b8be6", null ],
    [ "requestedMaxKeepAliveCount", "structUA__ModifySubscriptionRequest.html#a4386f12d643322632bafcf88e54c9490", null ],
    [ "requestedPublishingInterval", "structUA__ModifySubscriptionRequest.html#a64931e44b47ba9a7c1d23cba675be9de", null ],
    [ "requestHeader", "structUA__ModifySubscriptionRequest.html#a78b7feca396bc0628b53958dd81251fa", null ],
    [ "subscriptionId", "structUA__ModifySubscriptionRequest.html#ae7e443f644b57f998228fc2a035e2e2e", null ]
];