var structUA__ServerStatusDataType =
[
    [ "buildInfo", "structUA__ServerStatusDataType.html#ad4afb932c15ff97eef2a4a3ed8f4609d", null ],
    [ "currentTime", "structUA__ServerStatusDataType.html#a8ece07e0677206e7e688f932aa068edb", null ],
    [ "secondsTillShutdown", "structUA__ServerStatusDataType.html#a6661f4ad55b8043de1bc5cbed9d11464", null ],
    [ "shutdownReason", "structUA__ServerStatusDataType.html#a47fb14120aad4c792d6726a0ab7a433e", null ],
    [ "startTime", "structUA__ServerStatusDataType.html#aad6875ca07963a9bfbf6e60268f5c7d0", null ],
    [ "state", "structUA__ServerStatusDataType.html#a9a0d0a6313e7b6818be93d99c35ac7e5", null ]
];