var structUA__ModifyMonitoredItemsResponse =
[
    [ "diagnosticInfos", "structUA__ModifyMonitoredItemsResponse.html#a162e8a8d1222ad7b0c7b6ca71e978425", null ],
    [ "diagnosticInfosSize", "structUA__ModifyMonitoredItemsResponse.html#aa0b0b5382b629eea2f5776b4e817f1cc", null ],
    [ "responseHeader", "structUA__ModifyMonitoredItemsResponse.html#af27cdf9d4f9de1e1190cfef203e13efe", null ],
    [ "results", "structUA__ModifyMonitoredItemsResponse.html#af9f0144be270bca4e15e59763f1e5a66", null ],
    [ "resultsSize", "structUA__ModifyMonitoredItemsResponse.html#a14cc5ebfd8f3e70ee68fd793649044ec", null ]
];