var classUaPropertyMethodArgument =
[
    [ "ArgumentType", "classUaPropertyMethodArgument.html#a770d1bc1703bf136834b66c853c2874c", [
      [ "INARGUMENTS", "classUaPropertyMethodArgument.html#a770d1bc1703bf136834b66c853c2874cacae405ec63efc6b1815d0bc8497c8a7d", null ],
      [ "OUTARGUMENTS", "classUaPropertyMethodArgument.html#a770d1bc1703bf136834b66c853c2874ca30b92bf27cf4357bd6d0f32ded9e889f", null ]
    ] ],
    [ "UaPropertyMethodArgument", "classUaPropertyMethodArgument.html#a2dcf2b735ac86a0c6ee225c4d4126586", null ],
    [ "argumentType", "classUaPropertyMethodArgument.html#afef412a87f269340b7135f1a55424400", null ],
    [ "browseName", "classUaPropertyMethodArgument.html#acfce9e575b356cb3106b310c57d8eb94", null ],
    [ "implArgument", "classUaPropertyMethodArgument.html#a8341c1c7117b68dd49c81df18b19a282", null ],
    [ "nodeClass", "classUaPropertyMethodArgument.html#a23262036b93cd6b1c603eb0ec52ea65e", null ],
    [ "nodeId", "classUaPropertyMethodArgument.html#a015f3996658bb03ffd6e840d8fdd49e8", null ],
    [ "numArguments", "classUaPropertyMethodArgument.html#a927f32fc1e75487adfde6805c24317a9", null ],
    [ "setArgument", "classUaPropertyMethodArgument.html#af861c88b6e4713c23b642a9d0d244719", null ],
    [ "typeDefinitionId", "classUaPropertyMethodArgument.html#a7ca0ab4f46e1beea30a96bcecf321383", null ],
    [ "m_argumentType", "classUaPropertyMethodArgument.html#af5e3e4c90e4edee5c7af58b91dd70027", null ],
    [ "m_browseName", "classUaPropertyMethodArgument.html#a08049d4d9d5156263ca711b79a342961", null ],
    [ "m_impl", "classUaPropertyMethodArgument.html#ae7e7a6f8b6f6ee1dcbb1a009ebd8d2d7", null ],
    [ "m_nodeId", "classUaPropertyMethodArgument.html#a850c7d4d7f3950af6d09e2a201395e69", null ],
    [ "m_numberArguments", "classUaPropertyMethodArgument.html#acb1d32d4f77a67b602c928a2220b7e56", null ]
];