var structUA__BrowseRequest =
[
    [ "nodesToBrowse", "structUA__BrowseRequest.html#a684b9387fc6824199920b435a6324bc8", null ],
    [ "nodesToBrowseSize", "structUA__BrowseRequest.html#af37d958a523b4974b44788d95289daf6", null ],
    [ "requestedMaxReferencesPerNode", "structUA__BrowseRequest.html#a720946e1e5b67910fd6c6eca504edc24", null ],
    [ "requestHeader", "structUA__BrowseRequest.html#a2383ec16bb8998a8db769d50a059a5c0", null ],
    [ "view", "structUA__BrowseRequest.html#a6ff90479bf463d22621fe7995bf6fcb9", null ]
];