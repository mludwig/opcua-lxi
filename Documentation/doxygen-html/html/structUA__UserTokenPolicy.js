var structUA__UserTokenPolicy =
[
    [ "issuedTokenType", "structUA__UserTokenPolicy.html#ae1b44244ca5c78b3dbfa32e1aa01ff91", null ],
    [ "issuerEndpointUrl", "structUA__UserTokenPolicy.html#aef196a548f7401a7dd5cbf15d3eef22c", null ],
    [ "policyId", "structUA__UserTokenPolicy.html#a8d46bbba904da3b1a7ba789580479c5f", null ],
    [ "securityPolicyUri", "structUA__UserTokenPolicy.html#a05a881a553f5d5e0560e9ee20ac33b1c", null ],
    [ "tokenType", "structUA__UserTokenPolicy.html#a8bd687d93df617a6f89ac9afe316fe97", null ]
];