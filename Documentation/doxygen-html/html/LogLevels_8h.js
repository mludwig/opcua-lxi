var LogLevels_8h =
[
    [ "LOG_LEVEL", "LogLevels_8h.html#ad671761fe5e2e030871610bd6dfecd79", [
      [ "TRC", "LogLevels_8h.html#ad671761fe5e2e030871610bd6dfecd79ae6bf8bb3e34f59880268ec46bdbf0726", null ],
      [ "DBG", "LogLevels_8h.html#ad671761fe5e2e030871610bd6dfecd79ae41b345daa6b2e3de526828d8b86027f", null ],
      [ "INF", "LogLevels_8h.html#ad671761fe5e2e030871610bd6dfecd79addf0599cb02162d3e0e5b46a87a03483", null ],
      [ "WRN", "LogLevels_8h.html#ad671761fe5e2e030871610bd6dfecd79ad7e1b978af67d93778a417d7bfdf15b1", null ],
      [ "ERR", "LogLevels_8h.html#ad671761fe5e2e030871610bd6dfecd79acfb64e8a7eab3f0bf8da26cffbd8d175", null ]
    ] ],
    [ "logLevelFromString", "LogLevels_8h.html#a0cb204d4f5833c30f07f896cdade827d", null ],
    [ "logLevelToString", "LogLevels_8h.html#a0514d35b067113169a3ed7e73b819716", null ]
];