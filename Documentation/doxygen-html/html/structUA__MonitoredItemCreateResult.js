var structUA__MonitoredItemCreateResult =
[
    [ "filterResult", "structUA__MonitoredItemCreateResult.html#a581031ef470805bb50e8b1481c516b64", null ],
    [ "monitoredItemId", "structUA__MonitoredItemCreateResult.html#a3ba448069bdddb34b23f356d644bb75b", null ],
    [ "revisedQueueSize", "structUA__MonitoredItemCreateResult.html#a3fb01b6fae3f70b54093c5af22dad4e2", null ],
    [ "revisedSamplingInterval", "structUA__MonitoredItemCreateResult.html#ace4f6884bd368b9a956d2c88fb916035", null ],
    [ "statusCode", "structUA__MonitoredItemCreateResult.html#a848e3ed13c65ccd0eeb7ea937168e3ab", null ]
];