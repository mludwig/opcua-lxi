var structUA__RegisterServer2Response =
[
    [ "configurationResults", "structUA__RegisterServer2Response.html#a7615e451cde179a5bc66a4c70558c69e", null ],
    [ "configurationResultsSize", "structUA__RegisterServer2Response.html#a0eebc6effca07924e2621b2e14f3cdc6", null ],
    [ "diagnosticInfos", "structUA__RegisterServer2Response.html#adb223bac8efcbc195dccbd2fe8e7811a", null ],
    [ "diagnosticInfosSize", "structUA__RegisterServer2Response.html#a91ad4b479f932b23f6dca3f749d8b2f7", null ],
    [ "responseHeader", "structUA__RegisterServer2Response.html#a5bf181791591f6209872874e325934e6", null ]
];