var uavariant_8h =
[
    [ "UaVariant", "classUaVariant.html", "classUaVariant" ],
    [ "OpcUaType", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945ae", [
      [ "OpcUaType_Null", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea44b24ec5a6b7f87ebb81cfeb421412c0", null ],
      [ "OpcUaType_Boolean", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aeafd7ebcabe5687098985e084e67141fff", null ],
      [ "OpcUaType_SByte", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aeaec15de0c49336d7859189426ae7afad8", null ],
      [ "OpcUaType_Byte", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea4238503a5387df44c8b0ef697a4ed8fc", null ],
      [ "OpcUaType_Int16", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea0ddfdfa171e4e9e2e69a1128b0229274", null ],
      [ "OpcUaType_UInt16", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aeaed1a2b64a832cbbaab5cabebbe457322", null ],
      [ "OpcUaType_Int32", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea8f4bbbea6b1490a28b2fb906dd3ffdcd", null ],
      [ "OpcUaType_UInt32", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aead5b991bce77f360ca5694e0f3d101d99", null ],
      [ "OpcUaType_Int64", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aeab79edf0678781d5509adb86113f2d3e8", null ],
      [ "OpcUaType_UInt64", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aeae32bb0fffe6eab0c8b8387403a8b22d6", null ],
      [ "OpcUaType_Float", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aeab0fcbb3e88780f2a1bf5428e348d1fbf", null ],
      [ "OpcUaType_Double", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea4eab2a4b58aef1ab8689a599ddc83f60", null ],
      [ "OpcUaType_String", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea8f57d64fa5263bf27c937df1d19b75c8", null ],
      [ "OpcUaType_ByteString", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea5d567e0e042210105b7070203e944be7", null ],
      [ "OpcUaType_Variant", "uavariant_8h.html#a30b1bd6efe1788c86a3b986dc4d945aea3639ec46845fa62a54a3a42609377da9", null ]
    ] ]
];