var structUA__Variant =
[
    [ "arrayDimensions", "structUA__Variant.html#a355a8b884a259484008bee4f9068344e", null ],
    [ "arrayDimensionsSize", "structUA__Variant.html#aeb51badc8e696d0f9686ce805811f5cb", null ],
    [ "arrayLength", "structUA__Variant.html#ad67091bb86efe71a8b798cbd5c4f61d1", null ],
    [ "data", "structUA__Variant.html#a1479a5e23d6ab4b80c926d3ccea57485", null ],
    [ "storageType", "structUA__Variant.html#a09ff1e93e291d5b9db1ae5faa2c14780", null ],
    [ "type", "structUA__Variant.html#a0a64c9ccbd2125b6fdda794b68404994", null ]
];