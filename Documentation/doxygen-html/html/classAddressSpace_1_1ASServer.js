var classAddressSpace_1_1ASServer =
[
    [ "ASServer", "classAddressSpace_1_1ASServer.html#a0117b4f0f4cf4163925121683dc6f580", null ],
    [ "~ASServer", "classAddressSpace_1_1ASServer.html#a9683cab6fc8a96ebfc0d7a5fb470aa89", null ],
    [ "getDeviceLink", "classAddressSpace_1_1ASServer.html#ab3fca9af58bfda6179bc8da295b0381c", null ],
    [ "getRemainingCertificateValidity", "classAddressSpace_1_1ASServer.html#a930090fee157141a4d476ccfcacf9ad6", null ],
    [ "getRemainingCertificateValidity", "classAddressSpace_1_1ASServer.html#ab34cea570689606420d82b2e77c3bcb1", null ],
    [ "linkDevice", "classAddressSpace_1_1ASServer.html#a4b03f231da2795fbaeb432fb7088f8fa", null ],
    [ "setRemainingCertificateValidity", "classAddressSpace_1_1ASServer.html#ae682b2907841d04fb9ff58fff6d92961", null ],
    [ "typeDefinitionId", "classAddressSpace_1_1ASServer.html#a57cb5498f5b5efdbf13f51a1ce4f5785", null ],
    [ "UA_DISABLE_COPY", "classAddressSpace_1_1ASServer.html#a3af500523f0ba27746485e526b1d124c", null ],
    [ "unlinkDevice", "classAddressSpace_1_1ASServer.html#ac2bd757190e4fa662c65399ceb63371e", null ],
    [ "m_deviceLink", "classAddressSpace_1_1ASServer.html#a8fa8ee79c818b298b535e2f03c8fddb3", null ],
    [ "m_remainingCertificateValidity", "classAddressSpace_1_1ASServer.html#a9a14ed53db9acc8e0a73f67428a25335", null ],
    [ "m_typeNodeId", "classAddressSpace_1_1ASServer.html#a026c85dc9409064178d491bea867501f", null ]
];