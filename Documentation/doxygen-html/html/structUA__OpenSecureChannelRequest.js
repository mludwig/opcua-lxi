var structUA__OpenSecureChannelRequest =
[
    [ "clientNonce", "structUA__OpenSecureChannelRequest.html#ae01ee533471ba236875ab3225d585540", null ],
    [ "clientProtocolVersion", "structUA__OpenSecureChannelRequest.html#a2139e5701d49b46b8a5d9c7b0aa0fd53", null ],
    [ "requestedLifetime", "structUA__OpenSecureChannelRequest.html#a4c32f76f04587063108a3ee16e31a591", null ],
    [ "requestHeader", "structUA__OpenSecureChannelRequest.html#ad433d25718e7c0bb4edd7e383a1937fd", null ],
    [ "requestType", "structUA__OpenSecureChannelRequest.html#a6b7fe43afdd5688889c35bc2a1c1a842", null ],
    [ "securityMode", "structUA__OpenSecureChannelRequest.html#a8dfb3e9af9c75747f8005dd50661bb77", null ]
];