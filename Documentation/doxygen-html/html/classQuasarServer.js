var classQuasarServer =
[
    [ "QuasarServer", "classQuasarServer.html#a94c5b49aa95437896dd48210aaf0413b", null ],
    [ "~QuasarServer", "classQuasarServer.html#aef8e69026c6afd9580a25be92a7416b2", null ],
    [ "QuasarServer", "classQuasarServer.html#a71384ea3100129b118ea4fb9023bd670", null ],
    [ "initialize", "classQuasarServer.html#a04364e9edf119ba533bc86ed392b14d6", null ],
    [ "initializeLogIt", "classQuasarServer.html#ae4ee72c569d4bcfe00228f70a56a9331", null ],
    [ "mainLoop", "classQuasarServer.html#a1ee46fea63aadae3c8c314335c856a5d", null ],
    [ "operator=", "classQuasarServer.html#aa2f782596a2da2346c4e458b61ba964c", null ],
    [ "shutdown", "classQuasarServer.html#a31eb47659010e61582a6270c6b314bc8", null ]
];