var structUA__ModifyMonitoredItemsRequest =
[
    [ "itemsToModify", "structUA__ModifyMonitoredItemsRequest.html#ad90c175cfbd886376132a6563fd8ca9f", null ],
    [ "itemsToModifySize", "structUA__ModifyMonitoredItemsRequest.html#a263015c565efd26c23f1195a6c9a3800", null ],
    [ "requestHeader", "structUA__ModifyMonitoredItemsRequest.html#afec3392a524a921cf9bbea47a226960d", null ],
    [ "subscriptionId", "structUA__ModifyMonitoredItemsRequest.html#a7c81bc75a243cdcc00af3ee98a815c3f", null ],
    [ "timestampsToReturn", "structUA__ModifyMonitoredItemsRequest.html#a825489e0ce02ee9846bfb840318e1b69", null ]
];