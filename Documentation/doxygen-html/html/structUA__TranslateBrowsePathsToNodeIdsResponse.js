var structUA__TranslateBrowsePathsToNodeIdsResponse =
[
    [ "diagnosticInfos", "structUA__TranslateBrowsePathsToNodeIdsResponse.html#aa18f7ae4141181c577f8d5cd2adfe9cc", null ],
    [ "diagnosticInfosSize", "structUA__TranslateBrowsePathsToNodeIdsResponse.html#a781429ab06c53eb6fabbfe03b24656b0", null ],
    [ "responseHeader", "structUA__TranslateBrowsePathsToNodeIdsResponse.html#a93f870b7ba3698ff50566bddd204a615", null ],
    [ "results", "structUA__TranslateBrowsePathsToNodeIdsResponse.html#a28a69e876a59068d4a1dbe5fc1371b48", null ],
    [ "resultsSize", "structUA__TranslateBrowsePathsToNodeIdsResponse.html#a598f2757f541c93127da8e0ad6af91d5", null ]
];