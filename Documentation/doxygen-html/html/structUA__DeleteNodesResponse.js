var structUA__DeleteNodesResponse =
[
    [ "diagnosticInfos", "structUA__DeleteNodesResponse.html#af2bc3d08efa6383ebeb0e10c62917fcd", null ],
    [ "diagnosticInfosSize", "structUA__DeleteNodesResponse.html#a396650b995488f844139fea6064d7dd1", null ],
    [ "responseHeader", "structUA__DeleteNodesResponse.html#a554b5826103875eff48edea39c104148", null ],
    [ "results", "structUA__DeleteNodesResponse.html#a64b73622568727d729d317425f86e4b4", null ],
    [ "resultsSize", "structUA__DeleteNodesResponse.html#a97bf34e63eba00bc476318a0a744121c", null ]
];