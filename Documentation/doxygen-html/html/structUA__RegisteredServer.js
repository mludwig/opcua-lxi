var structUA__RegisteredServer =
[
    [ "discoveryUrls", "structUA__RegisteredServer.html#aaafaf60419fbcd7e2c88de3b2ab3db85", null ],
    [ "discoveryUrlsSize", "structUA__RegisteredServer.html#a21fa97bb59fc63248502384cb58add6f", null ],
    [ "gatewayServerUri", "structUA__RegisteredServer.html#ae4703560c6168b165bf7188f981cea0d", null ],
    [ "isOnline", "structUA__RegisteredServer.html#a66266f40fde01d51a41b5c76bf956c46", null ],
    [ "productUri", "structUA__RegisteredServer.html#a77070555d24cae17c2716ac17f4ec24d", null ],
    [ "semaphoreFilePath", "structUA__RegisteredServer.html#a862ad3b55f1f182e613f95b5d6f19d62", null ],
    [ "serverNames", "structUA__RegisteredServer.html#a2404b9fd640ba4ac3c2776ddd4ac471b", null ],
    [ "serverNamesSize", "structUA__RegisteredServer.html#a8cbfa4f1b5e431c1417edada17c49932", null ],
    [ "serverType", "structUA__RegisteredServer.html#a6dc80c61a454d319271f7db706851ba4", null ],
    [ "serverUri", "structUA__RegisteredServer.html#a3abc4907afc303031a030aa30d495d5e", null ]
];