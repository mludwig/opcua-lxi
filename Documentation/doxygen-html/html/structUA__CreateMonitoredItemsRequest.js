var structUA__CreateMonitoredItemsRequest =
[
    [ "itemsToCreate", "structUA__CreateMonitoredItemsRequest.html#a3e647c86d4c530d5d8fba7ff4d84f2ee", null ],
    [ "itemsToCreateSize", "structUA__CreateMonitoredItemsRequest.html#a0eeb386ffcea9e2cd24942da954fa5c8", null ],
    [ "requestHeader", "structUA__CreateMonitoredItemsRequest.html#a2149579febce776aeb92d54b9f852193", null ],
    [ "subscriptionId", "structUA__CreateMonitoredItemsRequest.html#a08108d340bb09c1322b33c6b1d42530f", null ],
    [ "timestampsToReturn", "structUA__CreateMonitoredItemsRequest.html#ade537232ab50bc3dd4f17ee2ebc7a23a", null ]
];