var structUA__ServerDiagnosticsSummaryDataType =
[
    [ "cumulatedSessionCount", "structUA__ServerDiagnosticsSummaryDataType.html#a3136b5d49823978f3b254309225d48fe", null ],
    [ "cumulatedSubscriptionCount", "structUA__ServerDiagnosticsSummaryDataType.html#aee310ab36fe56bf16ff0ce57631dbda0", null ],
    [ "currentSessionCount", "structUA__ServerDiagnosticsSummaryDataType.html#a7de00f053d451005e80ddef120021b8f", null ],
    [ "currentSubscriptionCount", "structUA__ServerDiagnosticsSummaryDataType.html#af1351f2f612ce7b29623f47414eb9420", null ],
    [ "publishingIntervalCount", "structUA__ServerDiagnosticsSummaryDataType.html#a1fc6b0daa342712d10c4629ca02790e5", null ],
    [ "rejectedRequestsCount", "structUA__ServerDiagnosticsSummaryDataType.html#acaa49d039108af2cfeff1386c0ac8d1a", null ],
    [ "rejectedSessionCount", "structUA__ServerDiagnosticsSummaryDataType.html#ad7fdce8e7b8d2c4fca09634da60d43fe", null ],
    [ "securityRejectedRequestsCount", "structUA__ServerDiagnosticsSummaryDataType.html#a23015f50a2789d86cdb812e7ae0f59be", null ],
    [ "securityRejectedSessionCount", "structUA__ServerDiagnosticsSummaryDataType.html#a8b18585cea7eb9b6a8d052161fc79b72", null ],
    [ "serverViewCount", "structUA__ServerDiagnosticsSummaryDataType.html#a6c22c362109a0942de9ef90c03da8136", null ],
    [ "sessionAbortCount", "structUA__ServerDiagnosticsSummaryDataType.html#a055d8d2ab10d0770f71813d630f5ba23", null ],
    [ "sessionTimeoutCount", "structUA__ServerDiagnosticsSummaryDataType.html#a47ccb1b0141a41f4d901bb8ace7567c8", null ]
];