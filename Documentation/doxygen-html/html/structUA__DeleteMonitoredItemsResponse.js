var structUA__DeleteMonitoredItemsResponse =
[
    [ "diagnosticInfos", "structUA__DeleteMonitoredItemsResponse.html#a6df126fbf374a44ed8f3d609f31ad008", null ],
    [ "diagnosticInfosSize", "structUA__DeleteMonitoredItemsResponse.html#a0e1ec016d314632bdfed5088c4ca3429", null ],
    [ "responseHeader", "structUA__DeleteMonitoredItemsResponse.html#a4a34767786b8b9b1b1126c3f9ebee057", null ],
    [ "results", "structUA__DeleteMonitoredItemsResponse.html#a20ea9429dcd96f1c0f04bf25d81e7955", null ],
    [ "resultsSize", "structUA__DeleteMonitoredItemsResponse.html#a7f8987b149669d574359139269af8415", null ]
];