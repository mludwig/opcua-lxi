var structUA__ExtensionObject =
[
    [ "body", "structUA__ExtensionObject.html#ac1af8afdad7bb507bf0e25c93527c645", null ],
    [ "content", "structUA__ExtensionObject.html#a9a001a958e6aa2a046d950f25bb5fcd6", null ],
    [ "content", "structUA__ExtensionObject.html#a80b71e515a769cba5ea09fd94d68022f", null ],
    [ "data", "structUA__ExtensionObject.html#a5767c86ad0c1012878e76ddad0ab61eb", null ],
    [ "decoded", "structUA__ExtensionObject.html#a04b472f78145f781f36a64ceec804b59", null ],
    [ "decoded", "structUA__ExtensionObject.html#a9dc54899e1161016e5796cffb5ecfba2", null ],
    [ "encoded", "structUA__ExtensionObject.html#ac111ecee0f54d8c9a58dc3312e4c9173", null ],
    [ "encoded", "structUA__ExtensionObject.html#aa3d704aba3afa9c57a7e0fdc3a7f5bcf", null ],
    [ "encoding", "structUA__ExtensionObject.html#a12f4e0212831d9948cde30506ae08a7f", null ],
    [ "type", "structUA__ExtensionObject.html#a779308659bff83a4a572e6436c1b2c98", null ],
    [ "typeId", "structUA__ExtensionObject.html#aae7bb3e696306431a2cd7bc511bd5c3f", null ]
];