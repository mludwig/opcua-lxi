var structUA__EventFilterResult =
[
    [ "selectClauseDiagnosticInfos", "structUA__EventFilterResult.html#aca768cf0ff79d20d52629124e1a6fb08", null ],
    [ "selectClauseDiagnosticInfosSize", "structUA__EventFilterResult.html#a8d1765f328ed45c3f4d6a3b3d3328338", null ],
    [ "selectClauseResults", "structUA__EventFilterResult.html#aab6d90ad8c99daa3765021b567d0e6db", null ],
    [ "selectClauseResultsSize", "structUA__EventFilterResult.html#a4d31169cb57ff47c02d1fd98faa26aca", null ],
    [ "whereClauseResult", "structUA__EventFilterResult.html#af5172ab6898e21505ede5cb28fe3c481", null ]
];