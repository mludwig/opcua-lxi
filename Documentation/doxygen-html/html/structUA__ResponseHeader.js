var structUA__ResponseHeader =
[
    [ "additionalHeader", "structUA__ResponseHeader.html#a37fb1ce36459e6023eb7e8ba0c696a2f", null ],
    [ "requestHandle", "structUA__ResponseHeader.html#ab950bd723cc238302fbd1de25ff04818", null ],
    [ "serviceDiagnostics", "structUA__ResponseHeader.html#a659d7e48a090357fa0800c71be4de84f", null ],
    [ "serviceResult", "structUA__ResponseHeader.html#a37d6cd2c9430cbbce1f777c5b1a6b4df", null ],
    [ "stringTable", "structUA__ResponseHeader.html#aa08bd3883de0bd9e6936faa6f51a7033", null ],
    [ "stringTableSize", "structUA__ResponseHeader.html#a301e38422b0e80f4b3a3f014576e4b34", null ],
    [ "timestamp", "structUA__ResponseHeader.html#ab59f0891b57b8a4cc2d3aecc38a9b885", null ]
];