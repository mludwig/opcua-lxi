var structUA__DiagnosticInfo =
[
    [ "additionalInfo", "structUA__DiagnosticInfo.html#a27ca8a6c90713742b582b3cfaf804bb7", null ],
    [ "hasAdditionalInfo", "structUA__DiagnosticInfo.html#affc1fe6b9121a6c4d9bab755947dcc54", null ],
    [ "hasInnerDiagnosticInfo", "structUA__DiagnosticInfo.html#a061ea36f7f4d488c0ba6ea3be69754ed", null ],
    [ "hasInnerStatusCode", "structUA__DiagnosticInfo.html#acb1b8dd7767baee39f4a8726f236046d", null ],
    [ "hasLocale", "structUA__DiagnosticInfo.html#a3b22d43bb7b08938abff2454f7066b30", null ],
    [ "hasLocalizedText", "structUA__DiagnosticInfo.html#a6207a939d6e7c77679f21accf8f86e6d", null ],
    [ "hasNamespaceUri", "structUA__DiagnosticInfo.html#a029311f425dfb1b179e91f950d61fcd4", null ],
    [ "hasSymbolicId", "structUA__DiagnosticInfo.html#a23b01f98b07c4b56f0f6bce2cb412fca", null ],
    [ "innerDiagnosticInfo", "structUA__DiagnosticInfo.html#a9426fc6c5496107ab4a9fb0e7e93d2e7", null ],
    [ "innerStatusCode", "structUA__DiagnosticInfo.html#af47a0c7b7f900928b9bc10b830b775dc", null ],
    [ "locale", "structUA__DiagnosticInfo.html#a28c683f0767da550eb843a8f89640ede", null ],
    [ "localizedText", "structUA__DiagnosticInfo.html#a3f7584006d0554f6bf381f538f431a24", null ],
    [ "namespaceUri", "structUA__DiagnosticInfo.html#a298201c813b173480fd92b5b1e25d0da", null ],
    [ "symbolicId", "structUA__DiagnosticInfo.html#aa7bba7f8e9135a2c2c0f82f8a74187fe", null ]
];