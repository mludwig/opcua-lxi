var structUA__SetMonitoringModeResponse =
[
    [ "diagnosticInfos", "structUA__SetMonitoringModeResponse.html#af77984729c54362371462958b456fe98", null ],
    [ "diagnosticInfosSize", "structUA__SetMonitoringModeResponse.html#a3ec847da841e81604cc7566d998087f6", null ],
    [ "responseHeader", "structUA__SetMonitoringModeResponse.html#ad0cfb55159cd804f12d1314b61fa9600", null ],
    [ "results", "structUA__SetMonitoringModeResponse.html#ab2eb8a68c115c4bbfafd338a45f68040", null ],
    [ "resultsSize", "structUA__SetMonitoringModeResponse.html#acf1e8a62f54f59830b7e7fe3c478e99a", null ]
];