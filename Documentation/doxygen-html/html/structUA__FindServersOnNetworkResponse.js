var structUA__FindServersOnNetworkResponse =
[
    [ "lastCounterResetTime", "structUA__FindServersOnNetworkResponse.html#ab447266819b6b718a828f8205c3458ea", null ],
    [ "responseHeader", "structUA__FindServersOnNetworkResponse.html#a73adbfc9d9d431783cc9efa2a826d404", null ],
    [ "servers", "structUA__FindServersOnNetworkResponse.html#a190caf8e0b85ddea0843cfcba74d03e8", null ],
    [ "serversSize", "structUA__FindServersOnNetworkResponse.html#a4970bb50acff75caea1ec26bcf4189c7", null ]
];