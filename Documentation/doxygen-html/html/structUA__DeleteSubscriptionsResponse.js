var structUA__DeleteSubscriptionsResponse =
[
    [ "diagnosticInfos", "structUA__DeleteSubscriptionsResponse.html#ab1d5a8d227f99478a03619c0a46a58ba", null ],
    [ "diagnosticInfosSize", "structUA__DeleteSubscriptionsResponse.html#a2ad0037157c628709e1bfbc65a43e139", null ],
    [ "responseHeader", "structUA__DeleteSubscriptionsResponse.html#a9cdebf1289b0e74219d184928cd31415", null ],
    [ "results", "structUA__DeleteSubscriptionsResponse.html#a843bc66ef0e0aa0fc78a50f339371a89", null ],
    [ "resultsSize", "structUA__DeleteSubscriptionsResponse.html#a3305797c34d7913babc179d7b72068f6", null ]
];