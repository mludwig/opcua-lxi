var namespacequasarExceptions =
[
    [ "DesignFlaw", "classquasarExceptions_1_1DesignFlaw.html", "classquasarExceptions_1_1DesignFlaw" ],
    [ "Mistake", "classquasarExceptions_1_1Mistake.html", "classquasarExceptions_1_1Mistake" ],
    [ "WrongArguments", "classquasarExceptions_1_1WrongArguments.html", "classquasarExceptions_1_1WrongArguments" ],
    [ "WrongReturnValue", "classquasarExceptions_1_1WrongReturnValue.html", "classquasarExceptions_1_1WrongReturnValue" ]
];