var structUA__MonitoringParameters =
[
    [ "clientHandle", "structUA__MonitoringParameters.html#a882262bf24dabfd1d4a38e83f71814e0", null ],
    [ "discardOldest", "structUA__MonitoringParameters.html#afd93c17288d11933e4113e596b406ab9", null ],
    [ "filter", "structUA__MonitoringParameters.html#a84e6ad86f41e82bf958ffcde09235138", null ],
    [ "queueSize", "structUA__MonitoringParameters.html#a3d2faace93a35e76c106f7af7be23b9d", null ],
    [ "samplingInterval", "structUA__MonitoringParameters.html#a3dea24f597b5347a3ec0e5ed90927b94", null ]
];