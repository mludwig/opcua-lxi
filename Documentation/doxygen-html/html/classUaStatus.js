var classUaStatus =
[
    [ "UaStatus", "classUaStatus.html#a96dfdbf28638196e3e101261f9d63018", null ],
    [ "UaStatus", "classUaStatus.html#a0edf61f3ee9d9c15597e486ab012f5cb", null ],
    [ "isBad", "classUaStatus.html#a657b0c5a3cb687ab9121686f8dc391a1", null ],
    [ "isGood", "classUaStatus.html#ac371e8952fc3e77837778cb3cb0695aa", null ],
    [ "isNotGood", "classUaStatus.html#a00835bbe5b98fd4bc2125b8fafdb34c3", null ],
    [ "isUncertain", "classUaStatus.html#a12388d8e15e9dd5349fbf6069b854a4a", null ],
    [ "operator UA_StatusCode", "classUaStatus.html#a09e54a5735de5f83830898af8de25930", null ],
    [ "operator=", "classUaStatus.html#a9c7e8e564fb15218bc9729774107a502", null ],
    [ "statusCode", "classUaStatus.html#a4f37fba5e95a5dc43e3b8225856815e9", null ],
    [ "toString", "classUaStatus.html#a77f2d164dce7d56550e95424fd73a051", null ],
    [ "m_status", "classUaStatus.html#aa848c268cfa1706bf6997a6dfd0e36d9", null ]
];