var structUA__RequestHeader =
[
    [ "additionalHeader", "structUA__RequestHeader.html#aed53334f83e7101368e9f1e7813239d4", null ],
    [ "auditEntryId", "structUA__RequestHeader.html#a069a27a4037a10c397d38e40db7845b2", null ],
    [ "authenticationToken", "structUA__RequestHeader.html#a4f5b95e55a6f4204eaf42c44a0315da4", null ],
    [ "requestHandle", "structUA__RequestHeader.html#a979d6070ac6c1e043f63a2228f519ba4", null ],
    [ "returnDiagnostics", "structUA__RequestHeader.html#a69d8bf2d3f6b35bba3ad033997383e85", null ],
    [ "timeoutHint", "structUA__RequestHeader.html#a6a4327ec9b8c7a92eff150cdd225c87b", null ],
    [ "timestamp", "structUA__RequestHeader.html#abb949d70499413df3e0383c45d10b49a", null ]
];