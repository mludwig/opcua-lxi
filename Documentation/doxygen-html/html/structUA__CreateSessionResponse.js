var structUA__CreateSessionResponse =
[
    [ "authenticationToken", "structUA__CreateSessionResponse.html#a35b51472f768bdbf4a3af0ce11df9642", null ],
    [ "maxRequestMessageSize", "structUA__CreateSessionResponse.html#a06d7879c970f7a83bd713e9ae2f62f96", null ],
    [ "responseHeader", "structUA__CreateSessionResponse.html#a106f36cdf7460219d00c535becfc93ed", null ],
    [ "revisedSessionTimeout", "structUA__CreateSessionResponse.html#a409e82281a55432c783332a176165789", null ],
    [ "serverCertificate", "structUA__CreateSessionResponse.html#a63db8e6f8987317caa4165fde30c77c0", null ],
    [ "serverEndpoints", "structUA__CreateSessionResponse.html#a98a1c15c139bb8d9e2bcbd7d1d50a40a", null ],
    [ "serverEndpointsSize", "structUA__CreateSessionResponse.html#a7b1e969efb60cebb78e59a0f087c2ef2", null ],
    [ "serverNonce", "structUA__CreateSessionResponse.html#a3816bf5b7a58be5d88aa43de348aea9a", null ],
    [ "serverSignature", "structUA__CreateSessionResponse.html#ab11050a55b520367117ec9ac75d392c7", null ],
    [ "serverSoftwareCertificates", "structUA__CreateSessionResponse.html#ac32c01cc4ac928068d8883803abf86ed", null ],
    [ "serverSoftwareCertificatesSize", "structUA__CreateSessionResponse.html#aab5c5b72b3573693532676add46f4d29", null ],
    [ "sessionId", "structUA__CreateSessionResponse.html#a9726af2d9ec4b58cc061470a45df684d", null ]
];