var structUA__ReferenceTypeAttributes =
[
    [ "description", "structUA__ReferenceTypeAttributes.html#a60b10e119653f8e7817ebd97a947c579", null ],
    [ "displayName", "structUA__ReferenceTypeAttributes.html#adb6bc2d8611bf4c1c5f44bc6876c60a8", null ],
    [ "inverseName", "structUA__ReferenceTypeAttributes.html#a7de2f36f5a83688b1d9280b59bd80888", null ],
    [ "isAbstract", "structUA__ReferenceTypeAttributes.html#a5ec894c8bda41c0a3c83467ca544328f", null ],
    [ "specifiedAttributes", "structUA__ReferenceTypeAttributes.html#abdf917c87014a72a9367bfc7951c82e5", null ],
    [ "symmetric", "structUA__ReferenceTypeAttributes.html#a3d4967dd5edd1e160fab9931504c3b73", null ],
    [ "userWriteMask", "structUA__ReferenceTypeAttributes.html#a8808f7ba299a6c7eec0a2ad4e27ab530", null ],
    [ "writeMask", "structUA__ReferenceTypeAttributes.html#ae54d14d8ede5b9220763c9c3ea617112", null ]
];