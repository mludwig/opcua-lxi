var dir_3ff2792e5d32d58aef067bb1e1f3a344 =
[
    [ "EnabledModules", "dir_5ab3d765010be52c690242bfbfb5e7d1.html", "dir_5ab3d765010be52c690242bfbfb5e7d1" ],
    [ "addressSpaceGenerators.py", "addressSpaceGenerators_8py.html", null ],
    [ "astyleSubstitute.py", "astyleSubstitute_8py.html", "astyleSubstitute_8py" ],
    [ "automated_build.py", "automated__build_8py.html", "automated__build_8py" ],
    [ "commandMap.py", "commandMap_8py.html", "commandMap_8py" ],
    [ "configurationGenerators.py", "configurationGenerators_8py.html", "configurationGenerators_8py" ],
    [ "DesignInspector.py", "DesignInspector_8py.html", "DesignInspector_8py" ],
    [ "designTools.py", "designTools_8py.html", "designTools_8py" ],
    [ "DesignValidator.py", "DesignValidator_8py.html", "DesignValidator_8py" ],
    [ "deviceGenerators.py", "deviceGenerators_8py.html", "deviceGenerators_8py" ],
    [ "distclean.py", "distclean_8py.html", "distclean_8py" ],
    [ "externalToolCheck.py", "externalToolCheck_8py.html", "externalToolCheck_8py" ],
    [ "generateCmake.py", "generateCmake_8py.html", "generateCmake_8py" ],
    [ "generateHonkyTonk.py", "generateHonkyTonk_8py.html", null ],
    [ "install_framework.py", "install__framework_8py.html", "install__framework_8py" ],
    [ "manage_files.py", "manage__files_8py.html", "manage__files_8py" ],
    [ "meta_build_info.py", "meta__build__info_8py.html", "meta__build__info_8py" ],
    [ "optionalModules.py", "optionalModules_8py.html", "optionalModules_8py" ],
    [ "Oracle.py", "Oracle_8py.html", [
      [ "Oracle", "classOracle_1_1Oracle.html", "classOracle_1_1Oracle" ]
    ] ],
    [ "quasar_basic_utils.py", "quasar__basic__utils_8py.html", "quasar__basic__utils_8py" ],
    [ "quasar_utils.py", "quasar__utils_8py.html", "quasar__utils_8py" ],
    [ "quasarCommands.py", "quasarCommands_8py.html", "quasarCommands_8py" ],
    [ "quasarExceptions.py", "quasarExceptions_8py.html", [
      [ "WrongArguments", "classquasarExceptions_1_1WrongArguments.html", "classquasarExceptions_1_1WrongArguments" ],
      [ "WrongReturnValue", "classquasarExceptions_1_1WrongReturnValue.html", "classquasarExceptions_1_1WrongReturnValue" ],
      [ "Mistake", "classquasarExceptions_1_1Mistake.html", "classquasarExceptions_1_1Mistake" ],
      [ "DesignFlaw", "classquasarExceptions_1_1DesignFlaw.html", "classquasarExceptions_1_1DesignFlaw" ]
    ] ],
    [ "runDoxygen.py", "runDoxygen_8py.html", "runDoxygen_8py" ],
    [ "transform_filters.py", "transform__filters_8py.html", "transform__filters_8py" ],
    [ "transformDesign.py", "transformDesign_8py.html", "transformDesign_8py" ],
    [ "version_control_interface.py", "version__control__interface_8py.html", "version__control__interface_8py" ]
];