var structUA__Connection =
[
    [ "channel", "structUA__Connection.html#a7c9d223c5522642e133fc0e5ce7deb68", null ],
    [ "close", "structUA__Connection.html#a058de89799bcfb3a93b56a5c88955f10", null ],
    [ "config", "structUA__Connection.html#a305c6e5d808850716975af8a04985e5d", null ],
    [ "connectCallbackID", "structUA__Connection.html#af67034ca1c351c82f57a5e791277990a", null ],
    [ "free", "structUA__Connection.html#a98b92ccfda81ebc8b5a135e3c6023493", null ],
    [ "getSendBuffer", "structUA__Connection.html#ad111f5c7f255936ad886f1d4e02b8ae5", null ],
    [ "handle", "structUA__Connection.html#a35da6b71deac37be9cb585360b9761ef", null ],
    [ "incompleteChunk", "structUA__Connection.html#a35bb96cb511b6840daf972b5255bce64", null ],
    [ "openingDate", "structUA__Connection.html#a64c185126ea39e89172f9f9dbc09302f", null ],
    [ "recv", "structUA__Connection.html#a703ad6c424dba3f8d4b3b90542a678ad", null ],
    [ "releaseRecvBuffer", "structUA__Connection.html#ab2344723bdd22cffb6ca7c3bce336d06", null ],
    [ "releaseSendBuffer", "structUA__Connection.html#a3712775bd92c395b5c807392acae5f74", null ],
    [ "send", "structUA__Connection.html#a1083f0bf79d07f30db706b356a87d766", null ],
    [ "sockfd", "structUA__Connection.html#a5dd9ac2eeadb8a60a8d2eefaacb08f2b", null ],
    [ "state", "structUA__Connection.html#a6979a4bc89f65e887c19d3ebbda19061", null ]
];