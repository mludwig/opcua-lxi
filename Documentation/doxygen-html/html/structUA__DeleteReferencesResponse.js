var structUA__DeleteReferencesResponse =
[
    [ "diagnosticInfos", "structUA__DeleteReferencesResponse.html#aaa074f5707054cffda8cd6d5a671f7eb", null ],
    [ "diagnosticInfosSize", "structUA__DeleteReferencesResponse.html#a865d63668fcb38ad8856d6e4e19708c0", null ],
    [ "responseHeader", "structUA__DeleteReferencesResponse.html#add56e8178c4618703aaa656a476f8099", null ],
    [ "results", "structUA__DeleteReferencesResponse.html#ae3b7f221334a0c0073176dcdea145205", null ],
    [ "resultsSize", "structUA__DeleteReferencesResponse.html#adc0ed863e7dddc18d71e1fe956dd7b23", null ]
];