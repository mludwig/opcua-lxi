var structUA__Server =
[
    [ "adminSession", "structUA__Server.html#a2e196ff35df470b3a3619600f707d0a6", null ],
    [ "bootstrapNS0", "structUA__Server.html#aeec88e0a089ef49d0e05155d65cc76d0", null ],
    [ "config", "structUA__Server.html#a1c577cc05399bc67bbea74640b1caf1d", null ],
    [ "endTime", "structUA__Server.html#a6f29078d43e755476d40ebabb489368b", null ],
    [ "namespaces", "structUA__Server.html#a0c5afbd1a160c26cc162a41c2ea26c50", null ],
    [ "namespacesSize", "structUA__Server.html#a2f1beeb821608a0680ec2a087d1d3027", null ],
    [ "nsCtx", "structUA__Server.html#ad644e441d9209b754ea2265810b30694", null ],
    [ "secureChannelManager", "structUA__Server.html#a0c2b3459cecb0697d224c2af3a0c6d13", null ],
    [ "sessionManager", "structUA__Server.html#acf5f53eb34eec848abdea5b09a97a9ea", null ],
    [ "startTime", "structUA__Server.html#a0557308f8c579b363b2c0ab78d99a89b", null ],
    [ "state", "structUA__Server.html#ad0903550b52414abde361411f30d9029", null ],
    [ "timer", "structUA__Server.html#a9f6d6c654442fd2161cb712edd3040e5", null ],
    [ "workQueue", "structUA__Server.html#a598668af282c77a964b50c7d08b5891c", null ]
];