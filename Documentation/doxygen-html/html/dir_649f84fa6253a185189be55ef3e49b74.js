var dir_649f84fa6253a185189be55ef3e49b74 =
[
    [ "AddressSpaceClasses.cpp", "AddressSpaceClasses_8cpp.html", null ],
    [ "array_templates.h", "array__templates_8h.html", [
      [ "UaCompatArray", "classUaCompatArray.html", "classUaCompatArray" ]
    ] ],
    [ "arrays.h", "arrays_8h.html", "arrays_8h" ],
    [ "arrays_test.cpp", "arrays__test_8cpp.html", "arrays__test_8cpp" ],
    [ "ASChannel.h", "ASChannel_8h.html", [
      [ "ASChannel", "classAddressSpace_1_1ASChannel.html", "classAddressSpace_1_1ASChannel" ]
    ] ],
    [ "ASInformationModel.cpp", "ASInformationModel_8cpp.html", null ],
    [ "ASInformationModel.h", "ASInformationModel_8h.html", [
      [ "ASInformationModel", "classAddressSpace_1_1ASInformationModel.html", "classAddressSpace_1_1ASInformationModel" ]
    ] ],
    [ "ASLxiAbstraction.h", "ASLxiAbstraction_8h.html", [
      [ "ASLxiAbstraction", "classAddressSpace_1_1ASLxiAbstraction.html", "classAddressSpace_1_1ASLxiAbstraction" ]
    ] ],
    [ "ASLxiCommand.h", "ASLxiCommand_8h.html", [
      [ "ASLxiCommand", "classAddressSpace_1_1ASLxiCommand.html", "classAddressSpace_1_1ASLxiCommand" ]
    ] ],
    [ "ASPowerSupply.h", "ASPowerSupply_8h.html", [
      [ "ASPowerSupply", "classAddressSpace_1_1ASPowerSupply.html", "classAddressSpace_1_1ASPowerSupply" ]
    ] ],
    [ "ASStatusCh.h", "ASStatusCh_8h.html", [
      [ "ASStatusCh", "classAddressSpace_1_1ASStatusCh.html", "classAddressSpace_1_1ASStatusCh" ]
    ] ],
    [ "ASStatusPs.h", "ASStatusPs_8h.html", [
      [ "ASStatusPs", "classAddressSpace_1_1ASStatusPs.html", "classAddressSpace_1_1ASStatusPs" ]
    ] ],
    [ "avahi.h", "avahi_8h.html", "avahi_8h" ],
    [ "Base_All.cpp", "Base__All_8cpp.html", null ],
    [ "Base_DChannel.h", "Base__DChannel_8h.html", "Base__DChannel_8h" ],
    [ "Base_DLxiAbstraction.h", "Base__DLxiAbstraction_8h.html", "Base__DLxiAbstraction_8h" ],
    [ "Base_DLxiCommand.h", "Base__DLxiCommand_8h.html", "Base__DLxiCommand_8h" ],
    [ "Base_DPowerSupply.h", "Base__DPowerSupply_8h.html", "Base__DPowerSupply_8h" ],
    [ "Base_DStatusCh.h", "Base__DStatusCh_8h.html", "Base__DStatusCh_8h" ],
    [ "Base_DStatusPs.h", "Base__DStatusPs_8h.html", "Base__DStatusPs_8h" ],
    [ "CMakeCXXCompilerId.cpp", "CMakeCXXCompilerId_8cpp.html", "CMakeCXXCompilerId_8cpp" ],
    [ "config.h", "config_8h.html", "config_8h" ],
    [ "Configuration.cxx", "Configuration_8cxx.html", "Configuration_8cxx" ],
    [ "Configuration.hxx", "Configuration_8hxx.html", "Configuration_8hxx" ],
    [ "Configurator.cpp", "Configurator_8cpp.html", "Configurator_8cpp" ],
    [ "ConfigValidator.cpp", "ConfigValidator_8cpp.html", "ConfigValidator_8cpp" ],
    [ "DRoot.cpp", "DRoot_8cpp.html", null ],
    [ "DRoot.h", "DRoot_8h.html", [
      [ "DRoot", "classDevice_1_1DRoot.html", "classDevice_1_1DRoot" ]
    ] ],
    [ "error.h", "error_8h.html", "error_8h" ],
    [ "feature_tests.cxx", "feature__tests_8cxx.html", "feature__tests_8cxx" ],
    [ "lxi.h", "lxi_8h.html", "lxi_8h" ],
    [ "main.cpp", "generatedSources_2main_8cpp.html", "generatedSources_2main_8cpp" ],
    [ "managed_uaarray.h", "managed__uaarray_8h.html", [
      [ "ManagedUaArray", "classManagedUaArray.html", "classManagedUaArray" ]
    ] ],
    [ "mdns.h", "mdns_8h.html", "mdns_8h" ],
    [ "MetaAmalgamate.cpp", "MetaAmalgamate_8cpp.html", "MetaAmalgamate_8cpp" ],
    [ "MetaBuildInfoGenerated.h", "MetaBuildInfoGenerated_8h.html", "MetaBuildInfoGenerated_8h" ],
    [ "methodhandleuanode.h", "methodhandleuanode_8h.html", [
      [ "MethodHandle", "classMethodHandle.html", "classMethodHandle" ],
      [ "MethodHandleUaNode", "classMethodHandleUaNode.html", "classMethodHandleUaNode" ]
    ] ],
    [ "methodmanager.h", "methodmanager_8h.html", [
      [ "ServiceContext", "classServiceContext.html", null ],
      [ "MethodManagerCallback", "classMethodManagerCallback.html", "classMethodManagerCallback" ],
      [ "MethodManager", "classMethodManager.html", "classMethodManager" ]
    ] ],
    [ "nodemanagerbase.cpp", "nodemanagerbase_8cpp.html", "nodemanagerbase_8cpp" ],
    [ "nodemanagerbase.h", "nodemanagerbase_8h.html", [
      [ "NodeManagerConfig", "classNodeManagerConfig.html", null ],
      [ "NodeManagerBase", "classNodeManagerBase.html", "classNodeManagerBase" ],
      [ "ServerRootNode", "classNodeManagerBase_1_1ServerRootNode.html", "classNodeManagerBase_1_1ServerRootNode" ]
    ] ],
    [ "opcua_attributes.h", "opcua__attributes_8h.html", "opcua__attributes_8h" ],
    [ "opcua_basedatavariabletype.cpp", "opcua__basedatavariabletype_8cpp.html", null ],
    [ "opcua_basedatavariabletype.h", "opcua__basedatavariabletype_8h.html", [
      [ "BaseDataVariableType", "classOpcUa_1_1BaseDataVariableType.html", "classOpcUa_1_1BaseDataVariableType" ]
    ] ],
    [ "opcua_baseobjecttype.h", "opcua__baseobjecttype_8h.html", [
      [ "BaseObjectType", "classOpcUa_1_1BaseObjectType.html", "classOpcUa_1_1BaseObjectType" ],
      [ "BaseMethod", "classOpcUa_1_1BaseMethod.html", "classOpcUa_1_1BaseMethod" ]
    ] ],
    [ "opcua_platformdefs.h", "opcua__platformdefs_8h.html", "opcua__platformdefs_8h" ],
    [ "opcua_types.h", "opcua__types_8h.html", "opcua__types_8h" ],
    [ "open62541.h", "generatedSources_2open62541_8h.html", "generatedSources_2open62541_8h" ],
    [ "open62541_compat.cpp", "open62541__compat_8cpp.html", null ],
    [ "open62541_compat.h", "open62541__compat_8h.html", [
      [ "IOManager", "classIOManager.html", null ]
    ] ],
    [ "open62541_compat_common.h", "open62541__compat__common_8h.html", [
      [ "alloc_error", "classalloc__error.html", "classalloc__error" ]
    ] ],
    [ "other.h", "other_8h.html", "other_8h" ],
    [ "session.h", "session_8h.html", [
      [ "Session", "classSession.html", null ]
    ] ],
    [ "simple_arrays.h", "simple__arrays_8h.html", "simple__arrays_8h" ],
    [ "SourceVariables.cpp", "SourceVariables_8cpp.html", "SourceVariables_8cpp" ],
    [ "SourceVariables.h", "SourceVariables_8h.html", "SourceVariables_8h" ],
    [ "stack_fake_structures.h", "stack__fake__structures_8h.html", [
      [ "ReadValueId", "structReadValueId.html", "structReadValueId" ],
      [ "DataValue", "structDataValue.html", "structDataValue" ],
      [ "WriteValue", "structWriteValue.html", "structWriteValue" ]
    ] ],
    [ "statuscode.cpp", "statuscode_8cpp.html", null ],
    [ "statuscode.h", "statuscode_8h.html", "statuscode_8h" ],
    [ "tcp.h", "tcp_8h.html", "tcp_8h" ],
    [ "uabasenodes.h", "uabasenodes_8h.html", [
      [ "UaObject", "classUaObject.html", null ],
      [ "UaMethod", "classUaMethod.html", "classUaMethod" ]
    ] ],
    [ "uabytearray.cpp", "uabytearray_8cpp.html", null ],
    [ "uabytestring.cpp", "uabytestring_8cpp.html", null ],
    [ "uabytestring.h", "uabytestring_8h.html", [
      [ "UaByteString", "classUaByteString.html", "classUaByteString" ]
    ] ],
    [ "uabytestring_test.cpp", "uabytestring__test_8cpp.html", "uabytestring__test_8cpp" ],
    [ "uaclientsdk.h", "uaclientsdk_8h.html", null ],
    [ "uadatavalue.cpp", "uadatavalue_8cpp.html", null ],
    [ "uadatavalue.h", "uadatavalue_8h.html", [
      [ "UaDataValue", "classUaDataValue.html", "classUaDataValue" ]
    ] ],
    [ "uadatavariablecache.cpp", "uadatavariablecache_8cpp.html", null ],
    [ "uadatavariablecache.h", "uadatavariablecache_8h.html", [
      [ "UaPropertyMethodArgument", "classUaPropertyMethodArgument.html", "classUaPropertyMethodArgument" ]
    ] ],
    [ "uadatetime.cpp", "uadatetime_8cpp.html", null ],
    [ "uadatetime.h", "uadatetime_8h.html", [
      [ "UaDateTime", "classUaDateTime.html", "classUaDateTime" ]
    ] ],
    [ "uanode.h", "uanode_8h.html", "uanode_8h" ],
    [ "uanodeid.cpp", "uanodeid_8cpp.html", null ],
    [ "uanodeid.h", "uanodeid_8h.html", "uanodeid_8h" ],
    [ "uaplatformlayer.h", "uaplatformlayer_8h.html", "uaplatformlayer_8h" ],
    [ "uaserver.cpp", "uaserver_8cpp.html", "uaserver_8cpp" ],
    [ "uaserver.h", "uaserver_8h.html", [
      [ "UaServer", "classUaServer.html", "classUaServer" ]
    ] ],
    [ "uasession.cpp", "uasession_8cpp.html", null ],
    [ "uasession.h", "uasession_8h.html", "uasession_8h" ],
    [ "uastring.cpp", "uastring_8cpp.html", null ],
    [ "uastring.h", "uastring_8h.html", [
      [ "UaString", "classUaString.html", "classUaString" ]
    ] ],
    [ "uavariant.cpp", "uavariant_8cpp.html", null ],
    [ "uavariant.h", "uavariant_8h.html", "uavariant_8h" ],
    [ "uavariant_test.cpp", "uavariant__test_8cpp.html", "uavariant__test_8cpp" ],
    [ "uavariant_test.h", "uavariant__test_8h.html", [
      [ "UaVariantTest", "classUaVariantTest.html", "classUaVariantTest" ]
    ] ],
    [ "vxi11.h", "vxi11_8h.html", "vxi11_8h" ],
    [ "vxi11core.h", "vxi11core_8h.html", "vxi11core_8h" ]
];