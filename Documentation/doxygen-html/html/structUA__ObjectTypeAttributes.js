var structUA__ObjectTypeAttributes =
[
    [ "description", "structUA__ObjectTypeAttributes.html#af8b94ab95da31f9ab032c2926099b4d2", null ],
    [ "displayName", "structUA__ObjectTypeAttributes.html#a7313ca296cee6c2d027dcb371811864f", null ],
    [ "isAbstract", "structUA__ObjectTypeAttributes.html#a3a0b1fc9d74cfb1ba4d5e1f8e5c01847", null ],
    [ "specifiedAttributes", "structUA__ObjectTypeAttributes.html#ac23decfec922f9f56f961654c22a7c14", null ],
    [ "userWriteMask", "structUA__ObjectTypeAttributes.html#a53045c672c87031832d9c2fe3b672434", null ],
    [ "writeMask", "structUA__ObjectTypeAttributes.html#a15e60b22ece69b2da0284d2cb759245a", null ]
];