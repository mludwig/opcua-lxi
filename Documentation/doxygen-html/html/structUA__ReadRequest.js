var structUA__ReadRequest =
[
    [ "maxAge", "structUA__ReadRequest.html#a9526a6c1b108a7d64e75b9cbe5b67d00", null ],
    [ "nodesToRead", "structUA__ReadRequest.html#a953ccc72b559799240aa762e1d049eb1", null ],
    [ "nodesToReadSize", "structUA__ReadRequest.html#a358afac8c854f024268b0107f9123535", null ],
    [ "requestHeader", "structUA__ReadRequest.html#a068678cac4d05e6d5c17057a01855065", null ],
    [ "timestampsToReturn", "structUA__ReadRequest.html#a3e9525cc70e450284f6653ced6ad356e", null ]
];