var classAddressSpace_1_1ASQuasar =
[
    [ "ASQuasar", "classAddressSpace_1_1ASQuasar.html#ad8ff6c707d7baf9354b8eb8ec8b56464", null ],
    [ "~ASQuasar", "classAddressSpace_1_1ASQuasar.html#a17812d7752ea5e7f60717e417023cbcd", null ],
    [ "getDeviceLink", "classAddressSpace_1_1ASQuasar.html#ab3eaa505b3ebb0f52bc98637aa0aac59", null ],
    [ "getVersion", "classAddressSpace_1_1ASQuasar.html#a4dbf92471210e4056a6e1eea6bd3f1d5", null ],
    [ "getVersion", "classAddressSpace_1_1ASQuasar.html#ad64550baeb2938bcef825bf49e27bedd", null ],
    [ "linkDevice", "classAddressSpace_1_1ASQuasar.html#a11d22ffe124105846f07715d7c33a928", null ],
    [ "setVersion", "classAddressSpace_1_1ASQuasar.html#a5216ffc2e2c3edd93e608bd01cbd5b93", null ],
    [ "typeDefinitionId", "classAddressSpace_1_1ASQuasar.html#a59b756fdf514f7d4454c6271e749bd1c", null ],
    [ "UA_DISABLE_COPY", "classAddressSpace_1_1ASQuasar.html#a8722a7a49f3ddb7f2741eae9aeb4e09a", null ],
    [ "unlinkDevice", "classAddressSpace_1_1ASQuasar.html#a7e5af402b78d9d2ac18b59272343e3f8", null ],
    [ "m_deviceLink", "classAddressSpace_1_1ASQuasar.html#a09c739dd6ab2ec2493d43d5c1fc9113f", null ],
    [ "m_typeNodeId", "classAddressSpace_1_1ASQuasar.html#a309b816bb276a1d432fac83636bd234b", null ],
    [ "m_version", "classAddressSpace_1_1ASQuasar.html#ab57451035e2a2e30dd82d7b7804d753a", null ]
];