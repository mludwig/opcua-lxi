/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "opcua-lxi quasar server", "index.html", [
    [ "opcua-lxi", "md__home_mludwig_cernLocal_projects_opcua-lxi-master_README.html", [
      [ "dependencies", "md__home_mludwig_cernLocal_projects_opcua-lxi-master_README.html#autotoc_md150", null ],
      [ "todo", "md__home_mludwig_cernLocal_projects_opcua-lxi-master_README.html#autotoc_md151", null ],
      [ "not implemented", "md__home_mludwig_cernLocal_projects_opcua-lxi-master_README.html#autotoc_md152", null ],
      [ "notes", "md__home_mludwig_cernLocal_projects_opcua-lxi-master_README.html#autotoc_md153", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", "functions_type" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", "globals_type" ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ASBuildInformation_8cpp.html",
"ChangeNotifyingVariable_8h.html",
"DGeneralLogLevel_8cpp_source.html",
"addressSpaceGenerators_8py.html",
"classAddressSpace_1_1ASNodeManager.html#ac212f246b21fbcb480892561fdebbf82",
"classAddressSpace_1_1ASStatusPs.html#a8fc269672ae1b9e5b67c91d355d518fd",
"classConfiguration_1_1Channel.html#a05141fa2d15cd9a9f453cf73ee483bf0",
"classConfiguration_1_1GeneralLogLevel.html#ada538a4ae0f01e8244ee38d20a9bc825",
"classConfiguration_1_1Server.html#a81317b7790bdb5ee5b57a1132216194c",
"classDesignInspector_1_1DesignInspector.html#a4beedf45810eedb1a3d5c5ba5065d37a",
"classDevice_1_1DPowerSupply.html",
"classOpcUa_1_1BaseDataVariableType.html#a3a87808e837dd2fc77044fba502f1389",
"classUaStatus.html#a4f37fba5e95a5dc43e3b8225856815e9",
"classmu_1_1ParserBase_1_1change__dec__sep.html",
"classmu_1_1Test_1_1ParserTester.html#a8af55174c5824ad457f07c2f5caa3f3d",
"functions_rela.html",
"generatedSources_2open62541_8h.html#a03efb0bffd0e65630e76b378d877b0b6",
"generatedSources_2open62541_8h.html#a099af6451d41120f035e8c2ef3fef7b4",
"generatedSources_2open62541_8h.html#a0f7a3a6bfb3195db3ca777de62adda20",
"generatedSources_2open62541_8h.html#a14fc665c539cac5890c33f4fb59463b2",
"generatedSources_2open62541_8h.html#a1ab6c63073d8a9fbe755b3f8f925f14f",
"generatedSources_2open62541_8h.html#a1f8dcf1d04ca364ed3a48113a0d7cda0",
"generatedSources_2open62541_8h.html#a24a9be3c5c84c5a92457d6c18add9fda",
"generatedSources_2open62541_8h.html#a2ae1cf3115b8b8a066efb6c993b3f2f1",
"generatedSources_2open62541_8h.html#a2fbd1f89bee1451193ed3b81acd049f0",
"generatedSources_2open62541_8h.html#a35044e403b2347ebd966c68c25bd7f43",
"generatedSources_2open62541_8h.html#a3a1326badedc91a202d3dadd6141c88e",
"generatedSources_2open62541_8h.html#a3fbc7cd91de5fcef7a6d362afff95129",
"generatedSources_2open62541_8h.html#a4510005e2fd8c7e2434d95031f4e117a",
"generatedSources_2open62541_8h.html#a4a23748fe06f77b110a57d8364a0b96c",
"generatedSources_2open62541_8h.html#a4e33492722b777ac6b9d69e071a62b0b",
"generatedSources_2open62541_8h.html#a537315353fdb055ba244014f2303857c",
"generatedSources_2open62541_8h.html#a58a290387e37d78b356fbb8775383f17",
"generatedSources_2open62541_8h.html#a5e392fbc10912c390e230cbb0b1def54",
"generatedSources_2open62541_8h.html#a642f3c85d79aa135fca2baab45d59064",
"generatedSources_2open62541_8h.html#a69288431aac7db7646d75aa035e38f2d",
"generatedSources_2open62541_8h.html#a6edd369e2c571728d5006af5e36bddf7",
"generatedSources_2open62541_8h.html#a74455cefd5802b60230281b8c1588ac6",
"generatedSources_2open62541_8h.html#a7831b33a3696e8916ef25d6bcadbaaa0",
"generatedSources_2open62541_8h.html#a7daaeb136986d93f544955348ba6968e",
"generatedSources_2open62541_8h.html#a8397ea00c5e99e87a3ef18efba1fc6b7",
"generatedSources_2open62541_8h.html#a893fbdb9dc125e9f65fe1ba11926afbf",
"generatedSources_2open62541_8h.html#a8e787b0a596cc63324149563ce951901",
"generatedSources_2open62541_8h.html#a93f2e3f71137556378ae2f8dc2dd13ee",
"generatedSources_2open62541_8h.html#a986c07a11f24a6816912c5f32db9ae87",
"generatedSources_2open62541_8h.html#a9dc327a38153d1380c1133a057d86ba2",
"generatedSources_2open62541_8h.html#aa2c7065d537b3db7e5f2b3e016679de7af48e339a5b7a0187f1e2c71a3b26f37a",
"generatedSources_2open62541_8h.html#aa8fac53ab8139855b1bc87d081fc2e8f",
"generatedSources_2open62541_8h.html#aaef1ec25e9e00456d3e6b26ca5645363",
"generatedSources_2open62541_8h.html#ab3f569f14f5d98b824ca900404a1ef98",
"generatedSources_2open62541_8h.html#ab987104db18a0d5e9cc2f076459c4233",
"generatedSources_2open62541_8h.html#abdb7b83c0763a68ee942280553f89a8a",
"generatedSources_2open62541_8h.html#ac314c6c598bd84d1818f09108f7cacb4",
"generatedSources_2open62541_8h.html#ac814cdb627cc8cf390c06256e0b61d7e",
"generatedSources_2open62541_8h.html#acd8243a6264580225f687107b5f2e8b6",
"generatedSources_2open62541_8h.html#ad2b1079209c5bfd3c1757be4af3d02af",
"generatedSources_2open62541_8h.html#ad8c574dbe1d8a661a43261c220f53dec",
"generatedSources_2open62541_8h.html#ade8de8a8232dcf31c4d5b7dbd4017812",
"generatedSources_2open62541_8h.html#ae38fdad0546215365c10cc36c2a119f6",
"generatedSources_2open62541_8h.html#ae7e79f0c940af0c5e70241f3fcecdce7a2716ab4b09815a227bc073ef6b1837de",
"generatedSources_2open62541_8h.html#aebbf392ca40aaba7c379b8edc46fb40e",
"generatedSources_2open62541_8h.html#af12d4f78b5dec3e8c119b72ab78f4f5c",
"generatedSources_2open62541_8h.html#af651bbfc97eb0917c9a0d880a70c0aa1",
"generatedSources_2open62541_8h.html#afc215fd29ada647c122690a6a317b7da",
"globals_r.html",
"include_2open62541_8h.html#a0450bdbbb17dc8f6611adf77a9a28632",
"include_2open62541_8h.html#a09c814b50311f23caa71345b650e23ed",
"include_2open62541_8h.html#a0f9954839cf7036c9830d775d8a9c5e1",
"include_2open62541_8h.html#a1537a3a7c251ba5a362c5476092beeb1",
"include_2open62541_8h.html#a1ae564475c8fd262d731da7174abd251",
"include_2open62541_8h.html#a1fd2ee78d0bb110da734cffe773c570e",
"include_2open62541_8h.html#a250bb520072ddd627d6d8acbef130ea3",
"include_2open62541_8h.html#a2b188eaba075f46cd20ce95af258ec95",
"include_2open62541_8h.html#a300aefbf7dd7bd108ed4a251b3997d3b",
"include_2open62541_8h.html#a354ee988bf7d8e0975c6d2c4973363fa",
"include_2open62541_8h.html#a3a520d752fce521351eba728dca929ab",
"include_2open62541_8h.html#a3fe5bfb1ccd813eee999dfb94003f4fd",
"include_2open62541_8h.html#a455dcd9046c035e54007905f1634d671",
"include_2open62541_8h.html#a4a4fe0e39679ef398d9f491ad3012f9a",
"include_2open62541_8h.html#a4e651f969989896eb7b6414144ee16a0",
"include_2open62541_8h.html#a53cb78b1b3a804b73ed3944369b2348c",
"include_2open62541_8h.html#a58ecd784f0f71fa835edc6974ee67e88",
"include_2open62541_8h.html#a5eab69d8631d99ea3b74da623bf5bf94",
"include_2open62541_8h.html#a6465c48b670f56fbd14fef78f2520cc8",
"include_2open62541_8h.html#a695be194086a85a2b35cd85b37e9af96",
"include_2open62541_8h.html#a6f3268d3ecdb37adae562eaee308e896",
"include_2open62541_8h.html#a747ee6b4dc6f279715ae48546cbb032d",
"include_2open62541_8h.html#a7865ef84e5ec40e99399a4dd17d57336",
"include_2open62541_8h.html#a7e16377031a8606906eaa001d022d465",
"include_2open62541_8h.html#a83e06747c2cd7e77ea1b5a9d8ee34951",
"include_2open62541_8h.html#a899228311700e99d006f1ba5bcfcbd02",
"include_2open62541_8h.html#a8eef4d58f8570d358c141bb22dc5aa9d",
"include_2open62541_8h.html#a943d1d8ff920c0c2507320fd2dd76492",
"include_2open62541_8h.html#a989fbc5bcd4d4ceb470df4e220309ace",
"include_2open62541_8h.html#a9def4ef499f9414a716944c8f135b974",
"include_2open62541_8h.html#aa30c001ef9772f4268d351e29636113a",
"include_2open62541_8h.html#aa93f72d6c07f50d74faf5215da0d0dfe",
"include_2open62541_8h.html#aaf14d86ceb8c0d89758e7b3dd251faa5",
"include_2open62541_8h.html#ab45e8743b291ef6794b850803e48baa0",
"include_2open62541_8h.html#ab9d939fd1fb6626746a4d0474e53c887",
"include_2open62541_8h.html#abe0a766d37fda23f5675dd3499891070",
"include_2open62541_8h.html#ac33ce422982b37d61d02e7910bcd6260",
"include_2open62541_8h.html#ac83fb67c221964dad5ea34c120b98399",
"include_2open62541_8h.html#acdc4468864f9722e9c24f958dbdcd32a",
"include_2open62541_8h.html#ad2ffc6e121e279450a71d1a61e668ba0",
"include_2open62541_8h.html#ad8fc165da6592512ae999bb2db5480ab",
"include_2open62541_8h.html#adea740438e0eef63090d5d66226cd456",
"include_2open62541_8h.html#ae3af3c02fdb05fd1bd53060113aac471a058c28d3e3a8a7f0851b6a2075dfa85e",
"include_2open62541_8h.html#ae7e79f0c940af0c5e70241f3fcecdce7a3e66794c47c6c83aeb33229cdc6ec180",
"include_2open62541_8h.html#aec11789b976c61632ff0e89d0b636fd0",
"include_2open62541_8h.html#af170170859b0072b7e3b2793f3d720e4",
"include_2open62541_8h.html#af6b8ead0ec814691fd1b10e9327a9cc5",
"include_2open62541_8h.html#afc5e0367b1ebf57787518471fea6b6a7",
"meta_8cpp_source.html",
"muParser_8h.html#aff435b0c277f66041374f93a3803e1f1a91ec8ef41d1b2308e7fcd096bf9a7162",
"open62541_8c.html#a1c5b55fd1dd616c3f83f93cd7efa815fa192a6b707f80291cff48313708dd5979",
"open62541_8c.html#a6d59acef08258b30eda6eec7aae751c6",
"open62541_8c.html#ac20f1d202af306cad27c36ffa034a45e",
"serverconfigxml__quasar_8h.html#ac48789ad0240c35e1a183693db468a8d",
"structUA__ActivateSessionRequest.html",
"structUA__ContentFilter.html#a74faddb6ed27d65883953aacdf2d0bb9",
"structUA__EnumValueType.html#a116c6395b8c59e2361d1d92e5b106324",
"structUA__NotificationMessage.html#a443b6b96e455e1183373519adefe53df",
"structUA__ServerConfig.html",
"structUA__VariableNode.html#ae99e10f61147ea0b82a0e2a15c76317c",
"uastring_8h_source.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';