var structUA__FindServersOnNetworkRequest =
[
    [ "maxRecordsToReturn", "structUA__FindServersOnNetworkRequest.html#a8315ebf015c2b607b50fa5ccd8e3d0df", null ],
    [ "requestHeader", "structUA__FindServersOnNetworkRequest.html#acf87cfb2dc6d1ef4b542ed01373a7780", null ],
    [ "serverCapabilityFilter", "structUA__FindServersOnNetworkRequest.html#a10ebfdf01f1614ee75519ffe234f3911", null ],
    [ "serverCapabilityFilterSize", "structUA__FindServersOnNetworkRequest.html#add07991616b5a2187ec2d2ff54d2db3e", null ],
    [ "startingRecordId", "structUA__FindServersOnNetworkRequest.html#aa31f0f5ae659f29e458558285ebf9754", null ]
];