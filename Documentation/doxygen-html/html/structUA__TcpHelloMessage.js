var structUA__TcpHelloMessage =
[
    [ "endpointUrl", "structUA__TcpHelloMessage.html#a8fead84c7af9ada3082a12ad10bb168e", null ],
    [ "maxChunkCount", "structUA__TcpHelloMessage.html#aa10b5e2370f1dbdd60d964f78e29a67c", null ],
    [ "maxMessageSize", "structUA__TcpHelloMessage.html#a9c30d66357ea7ca47b0b47c9b977292f", null ],
    [ "protocolVersion", "structUA__TcpHelloMessage.html#a01562c42b1b28c2549b237d63cfe3769", null ],
    [ "receiveBufferSize", "structUA__TcpHelloMessage.html#a7bcd40d8ea675a903162cb2c4cad97fa", null ],
    [ "sendBufferSize", "structUA__TcpHelloMessage.html#a9798574f19bc07eafc72d17425ccf5e6", null ]
];