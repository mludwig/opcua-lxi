var structUA__BrowseResponse =
[
    [ "diagnosticInfos", "structUA__BrowseResponse.html#af7450093ff0fa4aba51bc02b958ac312", null ],
    [ "diagnosticInfosSize", "structUA__BrowseResponse.html#ae763831ba3aee53b96f7567623eb26e8", null ],
    [ "responseHeader", "structUA__BrowseResponse.html#a86e6f47a5e322dd0073b35bce93873bd", null ],
    [ "results", "structUA__BrowseResponse.html#a2ed9d7af114d13a2d111681a1f64a245", null ],
    [ "resultsSize", "structUA__BrowseResponse.html#a4aa9c69b80d26ac1d1198e19a8aae132", null ]
];