var structUA__DataTypeAttributes =
[
    [ "description", "structUA__DataTypeAttributes.html#a5d7e749ed29220c1972377f91d242db2", null ],
    [ "displayName", "structUA__DataTypeAttributes.html#ae667c52e31830a6bb5c4c0b13d76a31f", null ],
    [ "isAbstract", "structUA__DataTypeAttributes.html#a626a39074e8acc6450c8e42132920b75", null ],
    [ "specifiedAttributes", "structUA__DataTypeAttributes.html#ad632808c12a34d50797fb81609b7c18e", null ],
    [ "userWriteMask", "structUA__DataTypeAttributes.html#a2eb9b3c85f210208a3f93ce2ab074637", null ],
    [ "writeMask", "structUA__DataTypeAttributes.html#a818f5940dbbc86039732228429acab7e", null ]
];