var vxi11_8h =
[
    [ "vxi11_data_t", "structvxi11__data__t.html", "structvxi11__data__t" ],
    [ "vxi11_connect", "vxi11_8h.html#a3aa4d5542d6750ef84c2f8a60703e66c", null ],
    [ "vxi11_disconnect", "vxi11_8h.html#a5801a5cc89f81bb81c4395ebbca96a30", null ],
    [ "vxi11_discover", "vxi11_8h.html#ad1283051774b977337e16c63eeee64be", null ],
    [ "vxi11_receive", "vxi11_8h.html#a563c7cc6a59f8fa6baca70d0eb5c0996", null ],
    [ "vxi11_send", "vxi11_8h.html#a9285776ef4d9df1bbc1e662db3da5613", null ]
];