var classversion__control__interface_1_1VersionControlInterface =
[
    [ "__init__", "classversion__control__interface_1_1VersionControlInterface.html#ac2059ee2084e2e50f69ed2a0e47f6fbc", null ],
    [ "add_to_vc", "classversion__control__interface_1_1VersionControlInterface.html#a00af34c32fb9d861f83a0950375f4319", null ],
    [ "get_latest_repo_commit", "classversion__control__interface_1_1VersionControlInterface.html#a9c9bbfa3d869a10a425e9b65f3702a86", null ],
    [ "is_versioned", "classversion__control__interface_1_1VersionControlInterface.html#a481025950780a166759be3e231a67505", null ],
    [ "remove_from_vc", "classversion__control__interface_1_1VersionControlInterface.html#a9863e5a3f3677735adb31828900974a5", null ],
    [ "project_path", "classversion__control__interface_1_1VersionControlInterface.html#a03c72ac3bbfe7e14166eae50dcfcde4d", null ],
    [ "repo", "classversion__control__interface_1_1VersionControlInterface.html#a1a45c9b884c1b84d65148981d348dc84", null ],
    [ "svnClient", "classversion__control__interface_1_1VersionControlInterface.html#a2625b3db2761ccb03ca6c8fe8a261c1c", null ],
    [ "vcs_type", "classversion__control__interface_1_1VersionControlInterface.html#a78ed550de8eb649f917f9ff10de3a61e", null ]
];