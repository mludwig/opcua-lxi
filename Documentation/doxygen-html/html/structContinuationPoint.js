var structContinuationPoint =
[
    [ "browseDescription", "structContinuationPoint.html#a95b363f91aaab8bd245ad65f543813fd", null ],
    [ "identifier", "structContinuationPoint.html#aea279fbb56d95274b31d88b8cee14399", null ],
    [ "maxReferences", "structContinuationPoint.html#a0dccf100b30058a1bd12be2dcdb1a3d7", null ],
    [ "next", "structContinuationPoint.html#a5104405cdbd052ba2e2def9342c7e6e0", null ],
    [ "referenceKindIndex", "structContinuationPoint.html#af35586832214c05c9e65bbb9902810d0", null ],
    [ "relevantReferences", "structContinuationPoint.html#a1ad72b0c1d59545f3176c55cc0a5d295", null ],
    [ "relevantReferencesSize", "structContinuationPoint.html#adb30de8e4de97ffd1f2ccec52856197f", null ],
    [ "targetIndex", "structContinuationPoint.html#ae76c53d682c2f5e6b1a9519e3069d822", null ]
];