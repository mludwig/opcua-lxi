var tcp_8h =
[
    [ "tcp_data_t", "structtcp__data__t.html", "structtcp__data__t" ],
    [ "tcp_connect", "tcp_8h.html#aafdf08e2c791f9703bdfdc2a9d37642e", null ],
    [ "tcp_disconnect", "tcp_8h.html#ae58ba2515913da8dfd4d4671b305d5e6", null ],
    [ "tcp_receive", "tcp_8h.html#a504de4296892e313dce9be8f427bd15e", null ],
    [ "tcp_receive_wait", "tcp_8h.html#aa4fa632c09bb05da9ada77536bb9cb77", null ],
    [ "tcp_send", "tcp_8h.html#a2da1186255ac01f7313c276939427274", null ]
];