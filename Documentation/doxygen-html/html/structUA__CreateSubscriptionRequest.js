var structUA__CreateSubscriptionRequest =
[
    [ "maxNotificationsPerPublish", "structUA__CreateSubscriptionRequest.html#ab19dfe8add881df83f1c72a6ed8be9e3", null ],
    [ "priority", "structUA__CreateSubscriptionRequest.html#a023b0d507ee88c159e722d0500805c97", null ],
    [ "publishingEnabled", "structUA__CreateSubscriptionRequest.html#a7f69cd917905c2d7991a1dd8bb1839ee", null ],
    [ "requestedLifetimeCount", "structUA__CreateSubscriptionRequest.html#acd8a1ed467228f89fda77605ce822e53", null ],
    [ "requestedMaxKeepAliveCount", "structUA__CreateSubscriptionRequest.html#a3c3927f3163f2e691548549dc0891d55", null ],
    [ "requestedPublishingInterval", "structUA__CreateSubscriptionRequest.html#ae0fe4cf6bf24ffa8bc051e797a7f2d96", null ],
    [ "requestHeader", "structUA__CreateSubscriptionRequest.html#ad508f4a7aa0d733116156d18c8afcdec", null ]
];