var structUA__MonitoredItemModifyResult =
[
    [ "filterResult", "structUA__MonitoredItemModifyResult.html#a431ff43d4ef25cddc778b335a4515e90", null ],
    [ "revisedQueueSize", "structUA__MonitoredItemModifyResult.html#a8f655652f5ef09f5356bf0f45afe6ac5", null ],
    [ "revisedSamplingInterval", "structUA__MonitoredItemModifyResult.html#aef12c4e08c82287bdbe18cf3e719dbcc", null ],
    [ "statusCode", "structUA__MonitoredItemModifyResult.html#ac980e6c580e11ce922b0282c1a23b5a9", null ]
];