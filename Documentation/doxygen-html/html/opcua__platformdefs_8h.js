var opcua__platformdefs_8h =
[
    [ "OpcUa_Boolean", "opcua__platformdefs_8h.html#a97afdba0e7a4bc768d971513278f4787", null ],
    [ "OpcUa_Byte", "opcua__platformdefs_8h.html#a7c69bf1e8c936381ab023af515c4769c", null ],
    [ "OpcUa_Double", "opcua__platformdefs_8h.html#a517e4943169426f38e725191cc7f90f4", null ],
    [ "OpcUa_Float", "opcua__platformdefs_8h.html#a0e13de8f03f5fdd53fa144a6f8d5e957", null ],
    [ "OpcUa_Int16", "opcua__platformdefs_8h.html#a6e776a6a160c20f51012968574df1bbb", null ],
    [ "OpcUa_Int32", "opcua__platformdefs_8h.html#add2c5de8f11e2d19acdeb0aea6327d4c", null ],
    [ "OpcUa_Int64", "opcua__platformdefs_8h.html#a64679efaeadb7d00d091691021c1fbb8", null ],
    [ "OpcUa_SByte", "opcua__platformdefs_8h.html#a053fd144f63bde8b920e55836e0aa5d5", null ],
    [ "OpcUa_UInt16", "opcua__platformdefs_8h.html#adb49933a72993d96341181b8dd7fcf6b", null ],
    [ "OpcUa_UInt32", "opcua__platformdefs_8h.html#a644d602ca2552e12ee90c2eb3d0d89b2", null ],
    [ "OpcUa_UInt64", "opcua__platformdefs_8h.html#a85088645cb430bbd2635d551cba45c2f", null ]
];