var structUA__NetworkMessageGroupHeader =
[
    [ "groupVersion", "structUA__NetworkMessageGroupHeader.html#a7df240e22d9f6f137b2a590a3d39b176", null ],
    [ "groupVersionEnabled", "structUA__NetworkMessageGroupHeader.html#adee7e93336da796b80e142d9737fb1a6", null ],
    [ "networkMessageNumber", "structUA__NetworkMessageGroupHeader.html#a09e1fbb55498bc7f2a1e419619852232", null ],
    [ "networkMessageNumberEnabled", "structUA__NetworkMessageGroupHeader.html#a3a9d2a431352542ac7205d42f2e918bf", null ],
    [ "sequenceNumber", "structUA__NetworkMessageGroupHeader.html#a82380f1847c7719f33e2e1cce0a28a7c", null ],
    [ "sequenceNumberEnabled", "structUA__NetworkMessageGroupHeader.html#abe30505e3769e490df9a7a46ff12348f", null ],
    [ "writerGroupId", "structUA__NetworkMessageGroupHeader.html#a2bca3be95e6e95bc1b780c7917e2292a", null ],
    [ "writerGroupIdEnabled", "structUA__NetworkMessageGroupHeader.html#af07cec2947e93c97cb69dfab0e4a05ce", null ]
];