var other_8h =
[
    [ "UaLocalizedText", "classUaLocalizedText.html", "classUaLocalizedText" ],
    [ "UaQualifiedName", "classUaQualifiedName.html", "classUaQualifiedName" ],
    [ "UaMutexRefCounted", "classUaMutexRefCounted.html", null ],
    [ "ServiceSettings", "classServiceSettings.html", null ],
    [ "DiagnosticInfo", "classDiagnosticInfo.html", null ],
    [ "OpcUa_False", "other_8h.html#a3ef47f3c6186c017c89629f2caa0dbfa", null ],
    [ "OpcUa_True", "other_8h.html#a9cd23e9f713e600ebd21efda404a46dc", null ],
    [ "OpcUaId_HasComponent", "other_8h.html#a73f9d5a230de6411cc7c1f4dbd175453", null ],
    [ "OpcUaId_HasProperty", "other_8h.html#a729f52506c88d587c786e5068eb4431e", null ],
    [ "OpcUaId_ObjectsFolder", "other_8h.html#aba8413d2491fd123b082398cfa4eba1a", null ],
    [ "OpcUaId_Organizes", "other_8h.html#ae74f48584c09f405de05766f33c1382a", null ],
    [ "UA_DISABLE_COPY", "other_8h.html#a14af2bf9231f83974e87bc42b06a41f3", null ],
    [ "OpcUa_AccessLevels", "other_8h.html#a22c41b218089dadacb2294dbf84c56a9", [
      [ "OpcUa_AccessLevels_CurrentRead", "other_8h.html#a22c41b218089dadacb2294dbf84c56a9ade38bf1b79d06ae2dcdefd9b93b6db8d", null ],
      [ "OpcUa_AccessLevels_CurrentReadOrWrite", "other_8h.html#a22c41b218089dadacb2294dbf84c56a9a4d1e3cab2038014aef89c3dc6eab9296", null ]
    ] ]
];