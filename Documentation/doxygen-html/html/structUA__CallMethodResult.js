var structUA__CallMethodResult =
[
    [ "inputArgumentDiagnosticInfos", "structUA__CallMethodResult.html#a59c902e604e4190216f17a0e8319e060", null ],
    [ "inputArgumentDiagnosticInfosSize", "structUA__CallMethodResult.html#af0cd4051a5bc87052d3826e534db8cf2", null ],
    [ "inputArgumentResults", "structUA__CallMethodResult.html#aea5aadab7a6294b3d45a5382c032313b", null ],
    [ "inputArgumentResultsSize", "structUA__CallMethodResult.html#ad5684934355d94bca5776dbb143a3029", null ],
    [ "outputArguments", "structUA__CallMethodResult.html#a26e4618fcb8289be3db736250dce9da2", null ],
    [ "outputArgumentsSize", "structUA__CallMethodResult.html#a606fd630756f9799c1f01b4a9d7f0fc9", null ],
    [ "statusCode", "structUA__CallMethodResult.html#aea681fca2720698d328cd80816e452af", null ]
];