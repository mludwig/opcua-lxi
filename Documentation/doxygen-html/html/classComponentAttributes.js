var classComponentAttributes =
[
    [ "ComponentAttributes", "classComponentAttributes.html#ae58e27e345a1f2159dcfad014bb6e415", null ],
    [ "getHandle", "classComponentAttributes.html#abd7e09edf3ef6784643ee4913d018189", null ],
    [ "getLevel", "classComponentAttributes.html#aec3fa579c5721fdf97badeac8ed1daa0", null ],
    [ "getName", "classComponentAttributes.html#a15addbe7a3cc56c88516d2bf8fd0ce91", null ],
    [ "setLevel", "classComponentAttributes.html#a819cf4c6a79ae82d23fc3417b43e4f85", null ],
    [ "m_handle", "classComponentAttributes.html#ae91d077e59b874768a01ce1f4056f98e", null ],
    [ "m_level", "classComponentAttributes.html#a8f25eab3798aa93c886720e1093f4a04", null ],
    [ "m_name", "classComponentAttributes.html#ade4381cb2d8998c07a24bf50d1161755", null ]
];