var serverconfigxml__quasar_8h =
[
    [ "ServerConfigXml", "classServerConfigXml.html", "classServerConfigXml" ],
    [ "UaEndpointXml", "classUaEndpointXml.html", "classUaEndpointXml" ],
    [ "CONFIG_DEFAULT_SECURE", "serverconfigxml__quasar_8h.html#a93ece1bae46cb15eb31af1485dfc9543", null ],
    [ "CONFIGXML_APPLICATIONNAME", "serverconfigxml__quasar_8h.html#a53ea986042981aff10e5656eb4bd81b7", null ],
    [ "CONFIGXML_APPLICATIONURI", "serverconfigxml__quasar_8h.html#af56dbcd2c4f06c6a818c582f43c870d4", null ],
    [ "CONFIGXML_BUILDNUMBER", "serverconfigxml__quasar_8h.html#a2ebb204715c543e86b94c16fa476f4a5", null ],
    [ "CONFIGXML_MANUFACTURERNAME", "serverconfigxml__quasar_8h.html#a3e3bb44388dc49fe584f243ec2452ed7", null ],
    [ "CONFIGXML_SERVERNAME", "serverconfigxml__quasar_8h.html#ac8cd8c4c49bc816d613c5d1585ef2db8", null ],
    [ "CONFIGXML_SERVERTRACEFILE", "serverconfigxml__quasar_8h.html#aad433e9c0e618ad30285b61a3ed13a76", null ],
    [ "CONFIGXML_SERVERURI", "serverconfigxml__quasar_8h.html#ad370ab8fdb75372b5f7d1e566e2352f3", null ],
    [ "CONFIGXML_SOFTWAREVERSION", "serverconfigxml__quasar_8h.html#ac48789ad0240c35e1a183693db468a8d", null ]
];