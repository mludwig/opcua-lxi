var structUA__EndpointDescription =
[
    [ "endpointUrl", "structUA__EndpointDescription.html#ac62e80827dce1de3d59ad3ad5e8b1159", null ],
    [ "securityLevel", "structUA__EndpointDescription.html#a9243fc1471d77fabec53892edda6aca6", null ],
    [ "securityMode", "structUA__EndpointDescription.html#a0555a1c24b924ccb58ddbdeff5c21525", null ],
    [ "securityPolicyUri", "structUA__EndpointDescription.html#ae6140d276e6498ba60de5a5a4aa038b3", null ],
    [ "server", "structUA__EndpointDescription.html#ac6e9c0d8154656db3b31f09bb723eec1", null ],
    [ "serverCertificate", "structUA__EndpointDescription.html#a836758191f4bd2d12b96c9df307dc225", null ],
    [ "transportProfileUri", "structUA__EndpointDescription.html#a1277d0e74bead01aa5ba73e8db5e59da", null ],
    [ "userIdentityTokens", "structUA__EndpointDescription.html#a339a396e9554c45d2dcf049292abbe9a", null ],
    [ "userIdentityTokensSize", "structUA__EndpointDescription.html#a3c15f3b83a042791399432610c1a1d72", null ]
];