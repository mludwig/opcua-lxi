var structUA__DataSetMessageHeader =
[
    [ "configVersionMajorVersion", "structUA__DataSetMessageHeader.html#a0f386086b394ef46e765caf756c3c932", null ],
    [ "configVersionMajorVersionEnabled", "structUA__DataSetMessageHeader.html#a8313ae465f3585b8c671342ab9cb6040", null ],
    [ "configVersionMinorVersion", "structUA__DataSetMessageHeader.html#a38a48bb5194180d1ea3a7cfbc9bdaf80", null ],
    [ "configVersionMinorVersionEnabled", "structUA__DataSetMessageHeader.html#ac4e7d905a3b9b39e0ce2e3d07ba279f9", null ],
    [ "dataSetMessageSequenceNr", "structUA__DataSetMessageHeader.html#a28b1587dcb94bc6f29f2ac5a721ddc5e", null ],
    [ "dataSetMessageSequenceNrEnabled", "structUA__DataSetMessageHeader.html#a794ccf2eca2f68ad67efa5aaf46b013c", null ],
    [ "dataSetMessageType", "structUA__DataSetMessageHeader.html#a40684a156396323ad26c561ce4cb7e6a", null ],
    [ "dataSetMessageValid", "structUA__DataSetMessageHeader.html#a7b36a57a4b164c0e2a88652b05de591e", null ],
    [ "fieldEncoding", "structUA__DataSetMessageHeader.html#a2d483f0dec7a81bb05cc92c73890ad60", null ],
    [ "picoSeconds", "structUA__DataSetMessageHeader.html#abb493462ad6fd4089ca599819319924c", null ],
    [ "picoSecondsIncluded", "structUA__DataSetMessageHeader.html#a1408641aa73b263091dad2328a26130f", null ],
    [ "status", "structUA__DataSetMessageHeader.html#aa87419e7a4e849ac6c9e38a05ed2edc5", null ],
    [ "statusEnabled", "structUA__DataSetMessageHeader.html#afca743924a6ce6cf7750d9c554f8f751", null ],
    [ "timestamp", "structUA__DataSetMessageHeader.html#afa08cff9f8f8bc84a59a2c09881fd17d", null ],
    [ "timestampEnabled", "structUA__DataSetMessageHeader.html#a9dc2ab7f13a01339f6441acaefcc60da", null ]
];