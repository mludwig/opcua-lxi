var structUA__ConnectionConfig =
[
    [ "maxChunkCount", "structUA__ConnectionConfig.html#a81ee2d188d2f91abf01656de3fe8baa3", null ],
    [ "maxMessageSize", "structUA__ConnectionConfig.html#a893ad4c0546d3172f65004a129fffa3d", null ],
    [ "protocolVersion", "structUA__ConnectionConfig.html#a87ba3a7575aeb17cdbe9395b562babf5", null ],
    [ "recvBufferSize", "structUA__ConnectionConfig.html#ab70c6c71dd05052b87cb23cf50c70ff9", null ],
    [ "sendBufferSize", "structUA__ConnectionConfig.html#a2195feb8b7dca0e8cf1554862921e0f9", null ]
];