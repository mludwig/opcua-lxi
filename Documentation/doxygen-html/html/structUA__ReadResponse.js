var structUA__ReadResponse =
[
    [ "diagnosticInfos", "structUA__ReadResponse.html#a6c82474bd514d83a204b6d44ef010736", null ],
    [ "diagnosticInfosSize", "structUA__ReadResponse.html#a78da5d61eb220bb11bf77ad592e95014", null ],
    [ "responseHeader", "structUA__ReadResponse.html#ab5e7d87e350af64748f98705b14b2987", null ],
    [ "results", "structUA__ReadResponse.html#aa9f3986d3e0a48e111f8c058a6e522cc", null ],
    [ "resultsSize", "structUA__ReadResponse.html#a6b64459ccb6c3050a4a0cfe0985673aa", null ]
];