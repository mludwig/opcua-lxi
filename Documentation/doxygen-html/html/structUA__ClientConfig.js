var structUA__ClientConfig =
[
    [ "certificateVerification", "structUA__ClientConfig.html#a4da8f6c7af3253dee3804576cfa4060a", null ],
    [ "clientContext", "structUA__ClientConfig.html#a1cae41573c09c4dafeb202cc28c27b21", null ],
    [ "clientDescription", "structUA__ClientConfig.html#a6fade19b283192b28b97a0fb690117a9", null ],
    [ "connectionFunc", "structUA__ClientConfig.html#a4cec19e7b55df75ee08268abb2d79f24", null ],
    [ "connectivityCheckInterval", "structUA__ClientConfig.html#abb2dd125ec3f721cb8bd499ae1c1080d", null ],
    [ "customDataTypes", "structUA__ClientConfig.html#a6bdd5c53c36ca3003a0d69fa553b81a4", null ],
    [ "endpoint", "structUA__ClientConfig.html#a22c1c28e832378de36e08fafc272bbcf", null ],
    [ "inactivityCallback", "structUA__ClientConfig.html#ab7a660ff1450287ada84546fd8127780", null ],
    [ "initConnectionFunc", "structUA__ClientConfig.html#a9ff28930720ca0ed67866885826a5d3f", null ],
    [ "localConnectionConfig", "structUA__ClientConfig.html#a7f8b40f7d423523fa1db69b0981aee74", null ],
    [ "logger", "structUA__ClientConfig.html#a257fba365bd636a2e5dfcd8f2cc6021e", null ],
    [ "outStandingPublishRequests", "structUA__ClientConfig.html#a2a315ec738bdc118fe0fd2601b83c459", null ],
    [ "pollConnectionFunc", "structUA__ClientConfig.html#a311f6d6ab841546c93ea2b2072a818f9", null ],
    [ "requestedSessionTimeout", "structUA__ClientConfig.html#a489d409411f7457b3c1982766b935c05", null ],
    [ "secureChannelLifeTime", "structUA__ClientConfig.html#afb39b9bc212f93c4030796c7da5302e7", null ],
    [ "securityMode", "structUA__ClientConfig.html#a9450311be41b1d43826e9e79cc87a27e", null ],
    [ "securityPolicies", "structUA__ClientConfig.html#ae24e5906d9fc88625ee3764d475eb04f", null ],
    [ "securityPoliciesSize", "structUA__ClientConfig.html#a222f3e9111ce37947d632d0e4a065c04", null ],
    [ "securityPolicyUri", "structUA__ClientConfig.html#a86b21b06787c365f0b6ae3e295754106", null ],
    [ "stateCallback", "structUA__ClientConfig.html#ac3137ba50b71077995fc375a7790a3b6", null ],
    [ "subscriptionInactivityCallback", "structUA__ClientConfig.html#a29b738d380ae055b5203cde4c3f20a1d", null ],
    [ "timeout", "structUA__ClientConfig.html#a0c2200cdb2da2cc360d7f4203e1bbd7d", null ],
    [ "userIdentityToken", "structUA__ClientConfig.html#a8b5f3385f794c67139476d5956394963", null ],
    [ "userTokenPolicy", "structUA__ClientConfig.html#aa350e739b9d1bae03b21ead040369435", null ]
];