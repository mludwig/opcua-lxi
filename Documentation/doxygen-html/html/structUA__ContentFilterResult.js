var structUA__ContentFilterResult =
[
    [ "elementDiagnosticInfos", "structUA__ContentFilterResult.html#a7bf65adec660cf5de3f389dca1024b3c", null ],
    [ "elementDiagnosticInfosSize", "structUA__ContentFilterResult.html#a36fd24627cef1c57cf69b6674d362a24", null ],
    [ "elementResults", "structUA__ContentFilterResult.html#aeca6ff9493cbd2a5618f23c1776912b7", null ],
    [ "elementResultsSize", "structUA__ContentFilterResult.html#aaac1556a3a06449f4ff42a21adaba26f", null ]
];