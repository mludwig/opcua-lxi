var structUA__Argument =
[
    [ "arrayDimensions", "structUA__Argument.html#a9e015767b779e0f7effda2b606caebd4", null ],
    [ "arrayDimensionsSize", "structUA__Argument.html#a684122c98b18cd674aa56ab8ee82ea4b", null ],
    [ "dataType", "structUA__Argument.html#ad91d13bee3d224bdcb22bf03703e49d4", null ],
    [ "description", "structUA__Argument.html#a8008c6f2917ca7410f4a79af40a92304", null ],
    [ "name", "structUA__Argument.html#ad747f62355ff229dab7c5044e4ecb9d2", null ],
    [ "valueRank", "structUA__Argument.html#a2eb8eed140afacb4e8fb08c575d2c2b8", null ]
];