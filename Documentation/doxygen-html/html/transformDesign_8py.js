var transformDesign_8py =
[
    [ "TransformKeys", "classtransformDesign_1_1TransformKeys.html", null ],
    [ "FieldIds", "classtransformDesign_1_1FieldIds.html", null ],
    [ "get_transform_path", "transformDesign_8py.html#ae65df44437fa3a8b24261d4d0224808e", null ],
    [ "getTransformOutput", "transformDesign_8py.html#a464ea5d11c95772ea5baaa1040d529f4", null ],
    [ "getTransformSpecByKey", "transformDesign_8py.html#a120753b491d71b521753996f67edce09", null ],
    [ "transformByKey", "transformDesign_8py.html#a46c321b7cc0b6cf26a326565a11e77ec", null ],
    [ "transformDesign", "transformDesign_8py.html#a8639dd89d273e226a33f7f5177421b44", null ],
    [ "transformDesignByJinja", "transformDesign_8py.html#a7be72962c4e36bd0f14b715687c27299", null ],
    [ "transformDesignByXslt", "transformDesign_8py.html#a4db0f7be99ee420926d2d870835fbc88", null ],
    [ "transformDesignVerbose", "transformDesign_8py.html#a29e5996552621c7ce6646ea9641800cd", null ],
    [ "LEGACY_CODE_GENERATION", "transformDesign_8py.html#ad490e8fe93ece7797f4f5260834fbfaf", null ],
    [ "QuasarTransforms", "transformDesign_8py.html#af77b401eca16ac282c505c460025dc88", null ]
];