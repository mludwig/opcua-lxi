var structUA__ViewAttributes =
[
    [ "containsNoLoops", "structUA__ViewAttributes.html#a5daaaa4a8397211a819d1e0a6a703b01", null ],
    [ "description", "structUA__ViewAttributes.html#a7125a35d58de343eaeb4d81711d731ba", null ],
    [ "displayName", "structUA__ViewAttributes.html#ad39c96458a0aa6bd76e1374a59c8775c", null ],
    [ "eventNotifier", "structUA__ViewAttributes.html#af59a7abb262650118d3f4c5ae66a29c1", null ],
    [ "specifiedAttributes", "structUA__ViewAttributes.html#a824773b1b5705070ac0bd4711a4f8621", null ],
    [ "userWriteMask", "structUA__ViewAttributes.html#aca91d435d175af9e4dc761561522a6e9", null ],
    [ "writeMask", "structUA__ViewAttributes.html#ae18e44ecf06a65733278b41ed867cf1e", null ]
];