var structUA__ObjectAttributes =
[
    [ "description", "structUA__ObjectAttributes.html#ad3dee77b7f1dc419cd3c7b049f868a3d", null ],
    [ "displayName", "structUA__ObjectAttributes.html#a235beaaf4a3db08d9d40fbef543521c0", null ],
    [ "eventNotifier", "structUA__ObjectAttributes.html#ac79f871bad74573cf3fc9e0f3dafd807", null ],
    [ "specifiedAttributes", "structUA__ObjectAttributes.html#a51ffd6879a0c34942269473c818bcd9b", null ],
    [ "userWriteMask", "structUA__ObjectAttributes.html#a7ba30d6d57fe387f9a6f05c29afb5309", null ],
    [ "writeMask", "structUA__ObjectAttributes.html#a07f7908f6a2128ab92817482c23b2280", null ]
];