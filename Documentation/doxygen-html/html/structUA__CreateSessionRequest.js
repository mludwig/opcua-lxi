var structUA__CreateSessionRequest =
[
    [ "clientCertificate", "structUA__CreateSessionRequest.html#af4b98d1378ce7c15877fe9d9c571e784", null ],
    [ "clientDescription", "structUA__CreateSessionRequest.html#aff414dcc3cff4ade1ffd21b71903de19", null ],
    [ "clientNonce", "structUA__CreateSessionRequest.html#a5789b06dff9826bd5e97af1b22795862", null ],
    [ "endpointUrl", "structUA__CreateSessionRequest.html#a1d8ac00723d6d8b92df9382efd6bbccd", null ],
    [ "maxResponseMessageSize", "structUA__CreateSessionRequest.html#af312a0256284fc903d3ccd73b8061e6e", null ],
    [ "requestedSessionTimeout", "structUA__CreateSessionRequest.html#a056284ce40bc841d1b62b7735789bd75", null ],
    [ "requestHeader", "structUA__CreateSessionRequest.html#ac68694e5b48f7125c5f4bc9455ff016e", null ],
    [ "serverUri", "structUA__CreateSessionRequest.html#a7755771824b4de1e928077532be9f2d3", null ],
    [ "sessionName", "structUA__CreateSessionRequest.html#ae6f14af7b260396a9bb6c058f4300b81", null ]
];