var structDevice__DocmdParms =
[
    [ "cmd", "structDevice__DocmdParms.html#a1983900e59768fbc11798df94f1774f4", null ],
    [ "data_in", "structDevice__DocmdParms.html#ab1a8acc36756750613518fa438219e8f", null ],
    [ "data_in_len", "structDevice__DocmdParms.html#a788248b04006944ee1f24c468554c5f5", null ],
    [ "data_in_val", "structDevice__DocmdParms.html#ad8c0368ef05c75942ff0dd89c8b1abfc", null ],
    [ "datasize", "structDevice__DocmdParms.html#ad3de434b777249503d10353ebf3aa678", null ],
    [ "flags", "structDevice__DocmdParms.html#af1afeaadfce5805a2281e36139fce015", null ],
    [ "io_timeout", "structDevice__DocmdParms.html#a8c69115735ce5d7ceaa7b9f8766624ff", null ],
    [ "lid", "structDevice__DocmdParms.html#aa10e21507b42568e5f5cc2a5b7f6b665", null ],
    [ "lock_timeout", "structDevice__DocmdParms.html#a6ea330c7a44efb275786c0fa01063675", null ],
    [ "network_order", "structDevice__DocmdParms.html#a319a06c63c3722d656a6e77333c4e432", null ]
];