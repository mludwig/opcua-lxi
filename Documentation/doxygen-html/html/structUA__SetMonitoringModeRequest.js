var structUA__SetMonitoringModeRequest =
[
    [ "monitoredItemIds", "structUA__SetMonitoringModeRequest.html#aa6cb9fee24c492b36675ce49cc1f29df", null ],
    [ "monitoredItemIdsSize", "structUA__SetMonitoringModeRequest.html#a8b69820818559a58e874f53c634c59f6", null ],
    [ "monitoringMode", "structUA__SetMonitoringModeRequest.html#ad3c2794b57af7e3980d205a8f060d0c7", null ],
    [ "requestHeader", "structUA__SetMonitoringModeRequest.html#a2e8ec4b6eec98da5ecdc9e46c24be2cb", null ],
    [ "subscriptionId", "structUA__SetMonitoringModeRequest.html#aadc280a889130ca5c3fe203476a4bb77", null ]
];