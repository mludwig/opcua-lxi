var structUA__ActivateSessionRequest =
[
    [ "clientSignature", "structUA__ActivateSessionRequest.html#a2d091134c17da1ecbce6a7c6b0440a49", null ],
    [ "clientSoftwareCertificates", "structUA__ActivateSessionRequest.html#a96e6a5f21fac9fe9d81d99f10448dc2f", null ],
    [ "clientSoftwareCertificatesSize", "structUA__ActivateSessionRequest.html#aa4aa5c268a84c1ce53fcbd0ebea4e489", null ],
    [ "localeIds", "structUA__ActivateSessionRequest.html#ab056a44cb4240f42ed9d0de5cd70339d", null ],
    [ "localeIdsSize", "structUA__ActivateSessionRequest.html#af187527280ff34ecfaf2eece224d3fcf", null ],
    [ "requestHeader", "structUA__ActivateSessionRequest.html#a7109bce99d5ecd6c37ac455269ae45a7", null ],
    [ "userIdentityToken", "structUA__ActivateSessionRequest.html#a95af5281fe8d6a62c03b8fd9171f8b73", null ],
    [ "userTokenSignature", "structUA__ActivateSessionRequest.html#a70f55c5fb2ed589d332dd0179a9be692", null ]
];