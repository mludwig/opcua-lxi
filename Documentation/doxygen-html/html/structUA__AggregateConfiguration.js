var structUA__AggregateConfiguration =
[
    [ "percentDataBad", "structUA__AggregateConfiguration.html#a3d4c620cfe783aff87f0ff9018168428", null ],
    [ "percentDataGood", "structUA__AggregateConfiguration.html#a08c8c9d80a4485441d5782ca2fde2397", null ],
    [ "treatUncertainAsBad", "structUA__AggregateConfiguration.html#a6bc2a0c4e34478f35f54e34f1e70283a", null ],
    [ "useServerCapabilitiesDefaults", "structUA__AggregateConfiguration.html#a5fd401920c5bceac919d328036931aea", null ],
    [ "useSlopedExtrapolation", "structUA__AggregateConfiguration.html#a2a02c8fb947c0f3feb149e9a36175f6a", null ]
];