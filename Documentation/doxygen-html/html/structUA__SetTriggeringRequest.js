var structUA__SetTriggeringRequest =
[
    [ "linksToAdd", "structUA__SetTriggeringRequest.html#a1122c315ad16f18d48012fb81c0539ea", null ],
    [ "linksToAddSize", "structUA__SetTriggeringRequest.html#a2b2527dd98dce6d4ed9e624d43a6c76b", null ],
    [ "linksToRemove", "structUA__SetTriggeringRequest.html#a62238b2b246ea1ee2f798c600e955eaf", null ],
    [ "linksToRemoveSize", "structUA__SetTriggeringRequest.html#aecc68722503733edf848e7e0f861c65c", null ],
    [ "requestHeader", "structUA__SetTriggeringRequest.html#aea57a7499c466d166ab1a36d803bcb90", null ],
    [ "subscriptionId", "structUA__SetTriggeringRequest.html#a485b43c68a7a357c4435a341e34dfb62", null ],
    [ "triggeringItemId", "structUA__SetTriggeringRequest.html#a1695e171963bb1e4794c9e0b88d7cae6", null ]
];