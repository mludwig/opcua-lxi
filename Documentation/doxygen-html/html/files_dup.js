var files_dup =
[
    [ "AddressSpace", "dir_9d0300c4d768b47749ab6b2e19d309b2.html", "dir_9d0300c4d768b47749ab6b2e19d309b2" ],
    [ "CalculatedVariables", "dir_6be399d288dbf1ef5f6aec9ca32914e7.html", "dir_6be399d288dbf1ef5f6aec9ca32914e7" ],
    [ "Common", "dir_4ab6b4cc6a7edbff49100e9123df213f.html", "dir_4ab6b4cc6a7edbff49100e9123df213f" ],
    [ "Configuration", "dir_64387ea9bbe6fc952e4d6bb8dd3c13de.html", "dir_64387ea9bbe6fc952e4d6bb8dd3c13de" ],
    [ "Design", "dir_e933556c2d91ca26d574a9556ecaee86.html", null ],
    [ "Device", "dir_4c76ddfc10a27668d6c42d3412320ee0.html", "dir_4c76ddfc10a27668d6c42d3412320ee0" ],
    [ "docker", "dir_ce48d24d5e2389ddf74c12f4fb3017f6.html", null ],
    [ "FrameworkInternals", "dir_3ff2792e5d32d58aef067bb1e1f3a344.html", "dir_3ff2792e5d32d58aef067bb1e1f3a344" ],
    [ "generatedSources", "dir_649f84fa6253a185189be55ef3e49b74.html", "dir_649f84fa6253a185189be55ef3e49b74" ],
    [ "html", "dir_1592ba7515d40cd5ff0647c0ff197723.html", "dir_1592ba7515d40cd5ff0647c0ff197723" ],
    [ "include", "dir_d44c64559bbebec7f509842c48db8b23.html", "dir_d44c64559bbebec7f509842c48db8b23" ],
    [ "LogIt", "dir_02eeca945d0f2da9c46e2b78b50b4375.html", "dir_02eeca945d0f2da9c46e2b78b50b4375" ],
    [ "LxiCommunication", "dir_0490e8c55b4fc72f88dba28d70c201e2.html", "dir_0490e8c55b4fc72f88dba28d70c201e2" ],
    [ "Meta", "dir_7795a02a4813dcd036f04e5f7854f185.html", "dir_7795a02a4813dcd036f04e5f7854f185" ],
    [ "other", "dir_8392597409477febc4fadea8376c2418.html", null ],
    [ "run", "dir_a306b7cc5167c29e131a00bfce4c6187.html", null ],
    [ "Server", "dir_1a445cb237d74f7a5f8d11ec3b0585cc.html", "dir_1a445cb237d74f7a5f8d11ec3b0585cc" ],
    [ "sphinx-source", "dir_7d47e719639384a89c449145b2f2b58b.html", "dir_7d47e719639384a89c449145b2f2b58b" ],
    [ "src", "dir_68267d1309a1af8e8297ef4c3efbcdba.html", "dir_68267d1309a1af8e8297ef4c3efbcdba" ],
    [ "tools", "dir_4eeb864c4eec08c7d6b9d3b0352cfdde.html", "dir_4eeb864c4eec08c7d6b9d3b0352cfdde" ],
    [ "quasar.py", "quasar_8py.html", "quasar_8py" ]
];