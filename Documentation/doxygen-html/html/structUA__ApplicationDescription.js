var structUA__ApplicationDescription =
[
    [ "applicationName", "structUA__ApplicationDescription.html#a3293da75ac498ae2ac9193a74d58a8a4", null ],
    [ "applicationType", "structUA__ApplicationDescription.html#a5a64c88e4b8f589c24978ae0ad92984a", null ],
    [ "applicationUri", "structUA__ApplicationDescription.html#a6dc1d2dac981f0e98ff710fa872af536", null ],
    [ "discoveryProfileUri", "structUA__ApplicationDescription.html#ac685f75d8947bebfa7e16adc19ccd33f", null ],
    [ "discoveryUrls", "structUA__ApplicationDescription.html#ab73777176e489fda6607f409265517ff", null ],
    [ "discoveryUrlsSize", "structUA__ApplicationDescription.html#a1e866cc65601ea7bca5a45cd641d0299", null ],
    [ "gatewayServerUri", "structUA__ApplicationDescription.html#abfe87af61cffc29acc02250bc5dd8a9f", null ],
    [ "productUri", "structUA__ApplicationDescription.html#a03aae332aa9caab8a0f94c934e7cfe1d", null ]
];