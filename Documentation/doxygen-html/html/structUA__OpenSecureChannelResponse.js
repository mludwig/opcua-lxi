var structUA__OpenSecureChannelResponse =
[
    [ "responseHeader", "structUA__OpenSecureChannelResponse.html#acf4815c89debd02e26634749e233c639", null ],
    [ "securityToken", "structUA__OpenSecureChannelResponse.html#aff0ffd16bbc4a7de41eb5ac18163e1d5", null ],
    [ "serverNonce", "structUA__OpenSecureChannelResponse.html#acee3cb0848492acfdb21667cdda62801", null ],
    [ "serverProtocolVersion", "structUA__OpenSecureChannelResponse.html#ae315f1afaff3d7860b94b53b396a8283", null ]
];