var structUA__FindServersRequest =
[
    [ "endpointUrl", "structUA__FindServersRequest.html#adb911f9f408ef4fe497b79b6fd39e4c9", null ],
    [ "localeIds", "structUA__FindServersRequest.html#af13642d5d22c72459ce8c60fb490431c", null ],
    [ "localeIdsSize", "structUA__FindServersRequest.html#a75f9b48ddffe7f22498912921fec7458", null ],
    [ "requestHeader", "structUA__FindServersRequest.html#a5ff072279f5878c5188e6b56ee8c5d9e", null ],
    [ "serverUris", "structUA__FindServersRequest.html#afd94cb50958994608c697cee7e568122", null ],
    [ "serverUrisSize", "structUA__FindServersRequest.html#ab54af5fc91c7d18f1eb0b259fea953e0", null ]
];