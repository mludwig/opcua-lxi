var structUA__AddReferencesResponse =
[
    [ "diagnosticInfos", "structUA__AddReferencesResponse.html#a15a7e09a4c5588a1a134ef30edf46151", null ],
    [ "diagnosticInfosSize", "structUA__AddReferencesResponse.html#a34228fd4e8f7fd80034fef8820fb589f", null ],
    [ "responseHeader", "structUA__AddReferencesResponse.html#aaaf1e3cc252818c59f3c8d0523775c38", null ],
    [ "results", "structUA__AddReferencesResponse.html#a98d16c4aa0be8465ac4f57fb73c80e7a", null ],
    [ "resultsSize", "structUA__AddReferencesResponse.html#ab480d360404ddd1e47e8f54fd18c6bf3", null ]
];