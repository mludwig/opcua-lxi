var namespaceUaClientSdk =
[
    [ "CallIn", "structUaClientSdk_1_1CallIn.html", "structUaClientSdk_1_1CallIn" ],
    [ "CallOut", "structUaClientSdk_1_1CallOut.html", "structUaClientSdk_1_1CallOut" ],
    [ "SessionConnectInfo", "structUaClientSdk_1_1SessionConnectInfo.html", "structUaClientSdk_1_1SessionConnectInfo" ],
    [ "SessionSecurityInfo", "structUaClientSdk_1_1SessionSecurityInfo.html", null ],
    [ "UaSession", "classUaClientSdk_1_1UaSession.html", "classUaClientSdk_1_1UaSession" ],
    [ "UaSessionCallback", "classUaClientSdk_1_1UaSessionCallback.html", null ]
];