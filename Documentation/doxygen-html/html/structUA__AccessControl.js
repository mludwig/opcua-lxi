var structUA__AccessControl =
[
    [ "activateSession", "structUA__AccessControl.html#abffac13b49350ca196c4277e5ca7b871", null ],
    [ "allowAddNode", "structUA__AccessControl.html#ab00a7989ae5ba6f6b31189fa2dbc6ca7", null ],
    [ "allowAddReference", "structUA__AccessControl.html#ac26f55857ee39b199c27d5f11bf94c10", null ],
    [ "allowDeleteNode", "structUA__AccessControl.html#a2070714b1e6a5c975723bb18b3552f12", null ],
    [ "allowDeleteReference", "structUA__AccessControl.html#a256ea08ff05cebe8f1ae7833ddfc88ab", null ],
    [ "closeSession", "structUA__AccessControl.html#a5aa769da849beb78d1538a201f9f7ecd", null ],
    [ "context", "structUA__AccessControl.html#a21d5b573914f86200e8d22168f611d5a", null ],
    [ "deleteMembers", "structUA__AccessControl.html#a9e106281eea9ec86c4a9e3837add9eb1", null ],
    [ "getUserAccessLevel", "structUA__AccessControl.html#afbbfe13d3e66d49536acecee9c8fa4c5", null ],
    [ "getUserExecutable", "structUA__AccessControl.html#a66fc15b4b0e4142ac1f226092a6e2db5", null ],
    [ "getUserExecutableOnObject", "structUA__AccessControl.html#a1b4feada7c498a8edc75cc287142f347", null ],
    [ "getUserRightsMask", "structUA__AccessControl.html#a44646aaf788169b6e5cba6064bec4310", null ],
    [ "userTokenPolicies", "structUA__AccessControl.html#ae167f8b8a10d85cf8cdcf63522ce42a6", null ],
    [ "userTokenPoliciesSize", "structUA__AccessControl.html#a46bcd813ab2fc5a4cd166036d876d710", null ]
];