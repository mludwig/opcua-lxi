var dir_30ff900869959b17197a70705703ad2d =
[
    [ "BaseQuasarServer.h", "BaseQuasarServer_8h.html", [
      [ "BaseQuasarServer", "classBaseQuasarServer.html", "classBaseQuasarServer" ]
    ] ],
    [ "opcserver.h", "opcserver_8h.html", null ],
    [ "opcserver_open62541.h", "opcserver__open62541_8h.html", null ],
    [ "QuasarServer.h", "QuasarServer_8h.html", [
      [ "QuasarServer", "classQuasarServer.html", "classQuasarServer" ]
    ] ],
    [ "QuasarServerCallback.h", "QuasarServerCallback_8h.html", [
      [ "OpcServerCallback", "classOpcServerCallback.html", "classOpcServerCallback" ],
      [ "QuasarServerCallback", "classQuasarServerCallback.html", "classQuasarServerCallback" ]
    ] ],
    [ "QuasarVersion.h", "QuasarVersion_8h.html", "QuasarVersion_8h" ],
    [ "serverconfigxml_quasar.h", "serverconfigxml__quasar_8h.html", "serverconfigxml__quasar_8h" ],
    [ "shutdown.h", "shutdown_8h.html", "shutdown_8h" ],
    [ "version.h", "version_8h.html", "version_8h" ]
];