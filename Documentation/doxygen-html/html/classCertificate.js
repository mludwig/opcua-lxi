var classCertificate =
[
    [ "behaviour_t", "classCertificate.html#ac123fc7521ed9eb2fce17795df8e2be8", [
      [ "BEHAVIOR_NONE", "classCertificate.html#ac123fc7521ed9eb2fce17795df8e2be8a10ff74c92e11136d10ff4e43d1fd8974", null ],
      [ "BEHAVIOR_TRY", "classCertificate.html#ac123fc7521ed9eb2fce17795df8e2be8a7545d74e4a77fc0995e88d80fc12c645", null ],
      [ "BEHAVIOR_TRYCA", "classCertificate.html#ac123fc7521ed9eb2fce17795df8e2be8acd426fb9e8738547a0ee68e0e40e04e1", null ],
      [ "BEHAVIOR_CERT", "classCertificate.html#ac123fc7521ed9eb2fce17795df8e2be8a5cc6f0d1ab0d03b2e3f7ff9000f42a0f", null ],
      [ "BEHAVIOR_CERTCA", "classCertificate.html#ac123fc7521ed9eb2fce17795df8e2be8ade5d4ace543a710afabba2b78bdee20a", null ]
    ] ],
    [ "status_t", "classCertificate.html#a74671bbf1815f477463dcd4594d7a032", [
      [ "STATUS_OK", "classCertificate.html#a74671bbf1815f477463dcd4594d7a032aad99de43eb3ff464e8729e1bb58d618f", null ],
      [ "STATUS_FAILED", "classCertificate.html#a74671bbf1815f477463dcd4594d7a032a0f594bb8f3ee858324c045f4bbcb6b8b", null ],
      [ "STATUS_UNKNOWN", "classCertificate.html#a74671bbf1815f477463dcd4594d7a032a1c23899ba0397b42542d60500a8a85ae", null ]
    ] ],
    [ "~Certificate", "classCertificate.html#ace8ed84b76d5d1a838a1f8c3f0b8fc47", null ],
    [ "Certificate", "classCertificate.html#a8952bc706a7acef214c141176003553f", null ],
    [ "Certificate", "classCertificate.html#a11835ad5a5a1172db6a49ebe9e0b2bac", null ],
    [ "_timeASN1toTIME_T", "classCertificate.html#afb46b42d8dd1c6dd3ffb8bd7f0801fe1", null ],
    [ "init", "classCertificate.html#aa65e4ef49e613915543a2c1cc57d5ed4", null ],
    [ "loadCertificateFromFile", "classCertificate.html#a2788b3d6c4edba04973539059aa5e336", null ],
    [ "loadPrivateKeyFromFile", "classCertificate.html#a70e28d987113d32ba38adf923ce8dd7f", null ],
    [ "operator=", "classCertificate.html#ac29f3fb26a5660aa0fa2a457f7201ba9", null ],
    [ "remainingDays", "classCertificate.html#a446d8da7993de43e11d25d3bda4bb086", null ],
    [ "remainingHours", "classCertificate.html#a63224507186e4047f44533bb1e4557ab", null ],
    [ "remainingMins", "classCertificate.html#adff66c1f09faece7671ec1e113525a54", null ],
    [ "remainingSecs", "classCertificate.html#a3faf74bc9a05fc31263c551565531478", null ],
    [ "remainingTime", "classCertificate.html#a75366f3524dc3c148773d540091fd38f", null ],
    [ "remainingValidityTime", "classCertificate.html#a470b3f785c0e51d7be57a6a0d1f3b1d8", null ],
    [ "setTypeDER", "classCertificate.html#aeefd52daa952ffb0410bc0e38e5ee8f8", null ],
    [ "setTypePEM", "classCertificate.html#a2f1a50f53aa5f957e27419681f271e72", null ],
    [ "validateCertificateFilename", "classCertificate.html#a83b8e93e0e7da936e4de6eb5a8030e63", null ],
    [ "m_behaviour", "classCertificate.html#af7229daab0dff9cd1956a08402728101", null ],
    [ "m_certfn", "classCertificate.html#acfd86527f7a803dd8404267c5c9325ae", null ],
    [ "m_privkeyfn", "classCertificate.html#a9b13c92fb9a675fac058b83ac23a65a7", null ],
    [ "m_remaining_validity_in_seconds", "classCertificate.html#aa4ee490b3c3f313f7c9476c71323dd89", null ],
    [ "m_ssl", "classCertificate.html#a058aab60daa1583134361bc7df530af2", null ],
    [ "m_status", "classCertificate.html#accf49add672c188780368c05b5c29820", null ],
    [ "m_time_end", "classCertificate.html#a843ebf226909b25f7d44f4d9de6b57ad", null ],
    [ "m_type", "classCertificate.html#aa561d493a363d8e818adced1b3025f94", null ],
    [ "t", "classCertificate.html#a06c1f3970fd505d67fb04a5712e22512", null ]
];