var structUA__AddNodesResponse =
[
    [ "diagnosticInfos", "structUA__AddNodesResponse.html#a041bd1e3c56a97033c3f4744cffa4baf", null ],
    [ "diagnosticInfosSize", "structUA__AddNodesResponse.html#a593b340bb8f1e4194d221c2bf4ab8f87", null ],
    [ "responseHeader", "structUA__AddNodesResponse.html#ab49c205e2687742e560c4b686323ddd5", null ],
    [ "results", "structUA__AddNodesResponse.html#accfe550f72a49776c38d25ed0dd7195f", null ],
    [ "resultsSize", "structUA__AddNodesResponse.html#ab4be6756730748f84bbe379c7300018b", null ]
];