var structUA__VariableAttributes =
[
    [ "accessLevel", "structUA__VariableAttributes.html#a1f9e60c6c258ab800ed6d32a3a337f12", null ],
    [ "arrayDimensions", "structUA__VariableAttributes.html#a4ced1fae54e1f3de40015280c968be6b", null ],
    [ "arrayDimensionsSize", "structUA__VariableAttributes.html#ab2b9ee9ffb3babafaa23e4e84b3e56f0", null ],
    [ "dataType", "structUA__VariableAttributes.html#a92e751581f1b42a5d60eaf9eacb8f309", null ],
    [ "description", "structUA__VariableAttributes.html#a35f48e9160500239c8fc257514ed856c", null ],
    [ "displayName", "structUA__VariableAttributes.html#a39426291dbfd62598dfb61821f012950", null ],
    [ "historizing", "structUA__VariableAttributes.html#a6b20dce841c378d5cfddf0239728507e", null ],
    [ "minimumSamplingInterval", "structUA__VariableAttributes.html#aa2e87f43d61abc101fb84e75d9a8ef79", null ],
    [ "specifiedAttributes", "structUA__VariableAttributes.html#a2cfe358df3c835b198d1c055778e01cb", null ],
    [ "userAccessLevel", "structUA__VariableAttributes.html#a37238486dc7cc95583d3f0a15d3afc06", null ],
    [ "userWriteMask", "structUA__VariableAttributes.html#accf141f2f40eec9c105fbe3b0d6ce591", null ],
    [ "value", "structUA__VariableAttributes.html#a142ca9b0dd01ec5a9bfad0af649f7cd7", null ],
    [ "valueRank", "structUA__VariableAttributes.html#a788791afdbc5b580bd083fba0b711950", null ],
    [ "writeMask", "structUA__VariableAttributes.html#a5390512cadf57eab9a53c72b6de6d3ff", null ]
];