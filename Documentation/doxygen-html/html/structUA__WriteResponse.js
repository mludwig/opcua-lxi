var structUA__WriteResponse =
[
    [ "diagnosticInfos", "structUA__WriteResponse.html#abdc97cb818dfa28e7f712b8bc209b288", null ],
    [ "diagnosticInfosSize", "structUA__WriteResponse.html#a7f6f2dd167bc7c32c342c1a5316bf149", null ],
    [ "responseHeader", "structUA__WriteResponse.html#a13279c8eeebfda49c0433a60bdee44ab", null ],
    [ "results", "structUA__WriteResponse.html#a5de0d7ad7f260114e08df1f25d2c825e", null ],
    [ "resultsSize", "structUA__WriteResponse.html#af62c34703375a1da96196d772bb9023b", null ]
];