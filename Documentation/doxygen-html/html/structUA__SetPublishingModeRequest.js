var structUA__SetPublishingModeRequest =
[
    [ "publishingEnabled", "structUA__SetPublishingModeRequest.html#a0ae26ed669894c3cbde5bd8155036379", null ],
    [ "requestHeader", "structUA__SetPublishingModeRequest.html#ac7cc063d9737d03360ac5775b402a7ec", null ],
    [ "subscriptionIds", "structUA__SetPublishingModeRequest.html#aea3a80c823e221ed5d9ff25aad3f697b", null ],
    [ "subscriptionIdsSize", "structUA__SetPublishingModeRequest.html#a69845e25ddf38c60a9441051a556ae54", null ]
];