var classDevice_1_1DChannel =
[
    [ "DChannel", "classDevice_1_1DChannel.html#ad2b4379cb6fb5255653c114f5be099ab", null ],
    [ "~DChannel", "classDevice_1_1DChannel.html#a621ebd2f3ccaccb2fa357fba4c8807aa", null ],
    [ "DChannel", "classDevice_1_1DChannel.html#a5b5a4035c027532ff65c8ad02224a687", null ],
    [ "init", "classDevice_1_1DChannel.html#a9188fb298e797b27a9e7c59c020e9d74", null ],
    [ "operator=", "classDevice_1_1DChannel.html#a2986d307d9a91348bfb7d7f92cdca843", null ],
    [ "swMaxCurrent", "classDevice_1_1DChannel.html#ab5a0715b31017e68bc38ee29558fe58d", null ],
    [ "swMaxVoltage", "classDevice_1_1DChannel.html#a2c5c6b81969ecb3712b3004159b60799", null ],
    [ "m_channel", "classDevice_1_1DChannel.html#a77fb37325c190c565fd9fdcc30d65ebf", null ],
    [ "m_swMaxCurrent", "classDevice_1_1DChannel.html#a26cefcdff03ccb543d1700998bbad90d", null ],
    [ "m_swMaxVoltage", "classDevice_1_1DChannel.html#ab6aa54f01d7eb009bdaa3e0248c06317", null ]
];