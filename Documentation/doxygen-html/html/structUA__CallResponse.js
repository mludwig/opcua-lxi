var structUA__CallResponse =
[
    [ "diagnosticInfos", "structUA__CallResponse.html#a2681039e141c5e802ab784c7c0e584f5", null ],
    [ "diagnosticInfosSize", "structUA__CallResponse.html#a143c867a668418ea19bc1e445cabbd53", null ],
    [ "responseHeader", "structUA__CallResponse.html#a9a4a7857d0ac8f23dffacce8fd0f04b2", null ],
    [ "results", "structUA__CallResponse.html#afc13ffa96aacd79771bbb287c6bc990e", null ],
    [ "resultsSize", "structUA__CallResponse.html#ab66429e75812f212e197a8638bae396c", null ]
];