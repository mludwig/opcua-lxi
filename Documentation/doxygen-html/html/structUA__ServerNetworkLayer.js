var structUA__ServerNetworkLayer =
[
    [ "deleteMembers", "structUA__ServerNetworkLayer.html#a97498bc9c4975fb7500519dab1d49209", null ],
    [ "discoveryUrl", "structUA__ServerNetworkLayer.html#a8b43ba58167b1497d4dbf862984919d3", null ],
    [ "handle", "structUA__ServerNetworkLayer.html#aa7cdb59be4da98193f5d88d0863af4ff", null ],
    [ "listen", "structUA__ServerNetworkLayer.html#acad65a47d1cc2f51807e96ffa1d0e96c", null ],
    [ "localConnectionConfig", "structUA__ServerNetworkLayer.html#aa108b0eeb4aad2995fc37d648b341442", null ],
    [ "start", "structUA__ServerNetworkLayer.html#abdd041077bb3cafbdab3e5c31f2ae12a", null ],
    [ "stop", "structUA__ServerNetworkLayer.html#aca851f5d195ecbaf037184246ac440c2", null ]
];