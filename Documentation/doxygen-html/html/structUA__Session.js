var structUA__Session =
[
    [ "activated", "structUA__Session.html#aec80693033a2eb0bfba005ba2f063e0a", null ],
    [ "availableContinuationPoints", "structUA__Session.html#ae7182bc51b05dce7b184e7e00ac15a5a", null ],
    [ "clientDescription", "structUA__Session.html#ac22828ad4ae842cc558bbb17627e4bb7", null ],
    [ "continuationPoints", "structUA__Session.html#a85a74d674701eedc7726d690741ffbe6", null ],
    [ "header", "structUA__Session.html#a6702280f3e56ac834391635735dbc974", null ],
    [ "maxRequestMessageSize", "structUA__Session.html#acd3ed55d092611a47a9deb3a72bd09ef", null ],
    [ "maxResponseMessageSize", "structUA__Session.html#a24a9a5f890a2b70db0ad526da1b886f8", null ],
    [ "serverNonce", "structUA__Session.html#a71c39780dee9020134b1bdae8d3fc491", null ],
    [ "sessionHandle", "structUA__Session.html#ad3f6d604d06d312ad1bb96d10609e225", null ],
    [ "sessionId", "structUA__Session.html#a04b611f1c5c2e991378c5c21d36a144e", null ],
    [ "sessionName", "structUA__Session.html#a83ee3b647386ad4a300601bc8f85f8d7", null ],
    [ "timeout", "structUA__Session.html#a3802af52d7eeb6635cbe2f3641d8426a", null ],
    [ "validTill", "structUA__Session.html#a36ae64e0a95bff52f4a23167c08462b3", null ]
];