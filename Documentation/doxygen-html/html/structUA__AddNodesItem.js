var structUA__AddNodesItem =
[
    [ "browseName", "structUA__AddNodesItem.html#a180f6562b17ec344cb15e000b307599c", null ],
    [ "nodeAttributes", "structUA__AddNodesItem.html#ac968de7300cd071fb16a295b25b4e9ac", null ],
    [ "nodeClass", "structUA__AddNodesItem.html#ad312278fa31dad766c55c5d64038de00", null ],
    [ "parentNodeId", "structUA__AddNodesItem.html#a7c89d6f509cc2c0bccc99571b4a9f963", null ],
    [ "referenceTypeId", "structUA__AddNodesItem.html#a2def2334ded4f3e9fd4585f4a14473b6", null ],
    [ "requestedNewNodeId", "structUA__AddNodesItem.html#a6713cc0ae31035f4059e2f02d5268f07", null ],
    [ "typeDefinition", "structUA__AddNodesItem.html#a81ee173f6b6f84fb7957ba7aec9c3eb2", null ]
];