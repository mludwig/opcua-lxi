var structUA__PublishResponse =
[
    [ "availableSequenceNumbers", "structUA__PublishResponse.html#a936598465754615abcb83bf12cb545c0", null ],
    [ "availableSequenceNumbersSize", "structUA__PublishResponse.html#af6cae88790055cf48967a0a050f11ee1", null ],
    [ "diagnosticInfos", "structUA__PublishResponse.html#af1e43a40f6a25322e8ecc07e168cf2e9", null ],
    [ "diagnosticInfosSize", "structUA__PublishResponse.html#aaf97152c870401e0e78e74d694460b4d", null ],
    [ "moreNotifications", "structUA__PublishResponse.html#a66fcd2311b4b97b150eab4f7740d6622", null ],
    [ "notificationMessage", "structUA__PublishResponse.html#a0642c2f23d2fddfbf43311eb35a02122", null ],
    [ "responseHeader", "structUA__PublishResponse.html#aac4546ee0d5d691c391ae5172b1909b3", null ],
    [ "results", "structUA__PublishResponse.html#a25fc5f2f7a15bbb2b51d038945fd7204", null ],
    [ "resultsSize", "structUA__PublishResponse.html#ad1de2707701092f91daf74aeb7861c0f", null ],
    [ "subscriptionId", "structUA__PublishResponse.html#a273e929829e5ac0a036d8fc009d006ab", null ]
];