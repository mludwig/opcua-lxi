var structUA__DataType =
[
    [ "binaryEncodingId", "structUA__DataType.html#af8046715f1121e61ad572139380b1707", null ],
    [ "members", "structUA__DataType.html#af055c2929d8a98f2a9213908bc3389d6", null ],
    [ "membersSize", "structUA__DataType.html#aa2cfdaca812599703cf17420e2935b43", null ],
    [ "memSize", "structUA__DataType.html#abf561fbb8c63f88a061b0e328575f418", null ],
    [ "overlayable", "structUA__DataType.html#a099407b1e8942c75fbb244d0516383c6", null ],
    [ "pointerFree", "structUA__DataType.html#a269ad7755ac20eb057b1f52a920c7c06", null ],
    [ "typeId", "structUA__DataType.html#a600c88a57a9e371cfe1da19f273f441b", null ],
    [ "typeIndex", "structUA__DataType.html#aa8c55e9c2f92f0130e8fb98a61ce437e", null ],
    [ "typeKind", "structUA__DataType.html#a11d003b2dcb235d7b3815c5e755bc2c1", null ],
    [ "typeName", "structUA__DataType.html#acc24cf6fbef7b4f771daeded120e5340", null ]
];