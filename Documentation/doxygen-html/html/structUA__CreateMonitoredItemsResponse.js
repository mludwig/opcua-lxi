var structUA__CreateMonitoredItemsResponse =
[
    [ "diagnosticInfos", "structUA__CreateMonitoredItemsResponse.html#ad546b889b1ff0fe2725b0e81b5947af4", null ],
    [ "diagnosticInfosSize", "structUA__CreateMonitoredItemsResponse.html#ae3c6b146c7fa1de1cb70f453e4d6efa2", null ],
    [ "responseHeader", "structUA__CreateMonitoredItemsResponse.html#a10797cda761b285ea69685b77f259971", null ],
    [ "results", "structUA__CreateMonitoredItemsResponse.html#a9a2f23575ab24b8116e1f23b6c8d21d8", null ],
    [ "resultsSize", "structUA__CreateMonitoredItemsResponse.html#aab6d5443cd27c6ce590a2fce5c085015", null ]
];