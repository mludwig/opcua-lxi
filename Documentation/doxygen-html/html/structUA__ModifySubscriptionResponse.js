var structUA__ModifySubscriptionResponse =
[
    [ "responseHeader", "structUA__ModifySubscriptionResponse.html#ade395fa474aa8987eb5e7f7d57530ad2", null ],
    [ "revisedLifetimeCount", "structUA__ModifySubscriptionResponse.html#a7a072872c1f38167f2ad2b4ad1fbe888", null ],
    [ "revisedMaxKeepAliveCount", "structUA__ModifySubscriptionResponse.html#a5a0b4819050bf14edfde18f03cd01493", null ],
    [ "revisedPublishingInterval", "structUA__ModifySubscriptionResponse.html#a7fd665f604dda8cb622706eecdfdf285", null ]
];