var structUA__ReferenceDescription =
[
    [ "browseName", "structUA__ReferenceDescription.html#ad1e31d6c57d0e1e88da14a6e118fda53", null ],
    [ "displayName", "structUA__ReferenceDescription.html#ab365a526e779a1eba84790f35ddb8861", null ],
    [ "isForward", "structUA__ReferenceDescription.html#a2c438703cc8e3c6c8903b30394b30efc", null ],
    [ "nodeClass", "structUA__ReferenceDescription.html#a772b050602c7b956ff1fac4e8ffae092", null ],
    [ "nodeId", "structUA__ReferenceDescription.html#ad010ab41c0f441963dfa37cdfed63864", null ],
    [ "referenceTypeId", "structUA__ReferenceDescription.html#a3ad0dd5b6e6565bd0d4d0376c78352e6", null ],
    [ "typeDefinition", "structUA__ReferenceDescription.html#aa61db0f2275e30a477ff6dd05827c4b3", null ]
];