var structUA__VariableTypeAttributes =
[
    [ "arrayDimensions", "structUA__VariableTypeAttributes.html#a4e7f5c2c4891489913b5c6ebc6a18325", null ],
    [ "arrayDimensionsSize", "structUA__VariableTypeAttributes.html#aecfcfae067ae1157276521c5476b405a", null ],
    [ "dataType", "structUA__VariableTypeAttributes.html#a4e855833d612cf128fdd2195634b5054", null ],
    [ "description", "structUA__VariableTypeAttributes.html#ae24cb598180e0dab09a39cb09655543e", null ],
    [ "displayName", "structUA__VariableTypeAttributes.html#ad1ae4d4a68fac2b43a2958701e2576ef", null ],
    [ "isAbstract", "structUA__VariableTypeAttributes.html#aae195991d11201ed3705b326f905869a", null ],
    [ "specifiedAttributes", "structUA__VariableTypeAttributes.html#ae5b30a3a32ef3a94dd063db0c639a227", null ],
    [ "userWriteMask", "structUA__VariableTypeAttributes.html#a374850c6b82d1600064b5b0b3cf6cd9a", null ],
    [ "value", "structUA__VariableTypeAttributes.html#a31f639db6a92882f032b33f3db093176", null ],
    [ "valueRank", "structUA__VariableTypeAttributes.html#a3e74a51c0db592309f394eeedb6f0f13", null ],
    [ "writeMask", "structUA__VariableTypeAttributes.html#afd62833c708eacebd26d814a4166d883", null ]
];