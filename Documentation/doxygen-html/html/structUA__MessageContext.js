var structUA__MessageContext =
[
    [ "buf_end", "structUA__MessageContext.html#a7a958209a66d6b896bb96076ead7b0fa", null ],
    [ "buf_pos", "structUA__MessageContext.html#a0d7d8ff8a712beb02af147dc3a04b608", null ],
    [ "channel", "structUA__MessageContext.html#a275998a10e4c26132afea98cb8f1bb8a", null ],
    [ "chunksSoFar", "structUA__MessageContext.html#ac348212ac1ef75d83824ea275e7cefdf", null ],
    [ "final", "structUA__MessageContext.html#affb1b53059ca4868b6c801c501145d92", null ],
    [ "messageBuffer", "structUA__MessageContext.html#a2d826987171f00b9b6e603431f19cb27", null ],
    [ "messageSizeSoFar", "structUA__MessageContext.html#ae858f9e18da7d91f2a463a3f65a1610c", null ],
    [ "messageType", "structUA__MessageContext.html#adb7ddcbf9836a38315c101f59b752459", null ],
    [ "requestId", "structUA__MessageContext.html#aacc78d99c4a0eb49662bc9270aa899c7", null ]
];