var structSyncResponseDescription =
[
    [ "client", "structSyncResponseDescription.html#ad8ff615c7d4209ce7199a1d60f7770f3", null ],
    [ "received", "structSyncResponseDescription.html#af5b8930c60cb488898f65fcecf6f204a", null ],
    [ "requestId", "structSyncResponseDescription.html#a5c1fff4a8042ec7fbfada4ead6e280e4", null ],
    [ "response", "structSyncResponseDescription.html#a6bc88921f259fcf133ad646536bb1358", null ],
    [ "responseType", "structSyncResponseDescription.html#a66ea66fbffc5f052d3a46afe7b32d865", null ]
];