# #!/bin/csh
# cleanup documentation result to that we can recreate them from scratch in th CI/CD pipelines
rm -rf ./sphinx-source/xml/*
rm -rf ./sphinx-result/*
rm -rf ./doxygen-result/*
rm -rf ./generatedSources
rm -rf ./html
rm -rf ./latex
rm -rf ./pdf
rm -rf ./qthelp

