#!/bin/csh
# prune documentation to keep just the html result
rm -rfv ./sphinx-result/latex/
rm -rfv ./sphinx-result/.doctrees/*
rm -rfv ./sphinx-source/doxygen-result/
