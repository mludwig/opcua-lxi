
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by quasar (https://github.com/quasar-team/quasar/)

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.


 */


#include <Configuration.hxx> // TODO; should go away, is already in Base class for ages

#include <DStatusCh.h>
#include <ASStatusCh.h>

namespace Device
{
// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DStatusCh::DStatusCh (
    const Configuration::StatusCh& config,
    Parent_DStatusCh* parent
):
    Base_DStatusCh( config, parent)

    /* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DStatusCh::~DStatusCh ()
{
}

/* delegates for cachevariables */



/* delegators for methods */

// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DStatusCh::update( unsigned int status ){
	AddressSpace::ASStatusCh *as = getAddressSpaceLink();
	UaDateTime serverTime = UaDateTime::now();
	OpcUa_StatusCode opcua_status = OpcUa_Good;

	bool bit0 = false;
	bool bit1 = false;
	bool bit2 = false;
	bool bit3 = false;
	bool bit4 = false;
	bool bit5 = false;
	bool bit6 = false;
	bool bit7 = false;
	bool bit8 = false;

	if ( status & 0x1 << STATUS_CH_ON )		bit0 = true;
	if ( status & 0x1 << STATUS_CH_TRIPV )	bit1 = true;
	if ( status & 0x1 << STATUS_CH_TRIPC )	bit2 = true;
	if ( status & 0x1 << STATUS_CH_CC )		bit3 = true;
	if ( status & 0x1 << STATUS_CH_OVVS )	bit4 = true;
	if ( status & 0x1 << STATUS_CH_OVVH )	bit5 = true;
	if ( status & 0x1 << STATUS_CH_UNV )	bit6 = true;
	if ( status & 0x1 << STATUS_CH_OVCS )	bit7 = true;
	if ( status & 0x1 << STATUS_CH_OVCH )	bit8 = true;

	as->setSTATUS_CH_ON(     bit0, opcua_status, serverTime );
	as->setSTATUS_CH_TRIPV(  bit1, opcua_status, serverTime );
	as->setSTATUS_CH_TRIPC(  bit2, opcua_status, serverTime );
	as->setSTATUS_CH_CC(     bit3, opcua_status, serverTime );
	as->setSTATUS_CH_OVVS(   bit4, opcua_status, serverTime );
	as->setSTATUS_CH_OVVH(   bit5, opcua_status, serverTime );
	as->setSTATUS_CH_UNV(    bit6, opcua_status, serverTime );
	as->setSTATUS_CH_OVCS(   bit7, opcua_status, serverTime );
	as->setSTATUS_CH_OVCH(   bit8, opcua_status, serverTime );
}

}
