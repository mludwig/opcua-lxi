# @author mludwig (c) cern
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cal9.open6" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
# alma9 runs with 1.75.0
SET( IGNORE_DEFAULT_BOOST_SETUP OFF )
#SET( BOOST_HOME "/opt/3rdPartySoftware/boost/boost_1_81_0" )
#SET( BOOST_PATH_HEADERS "/opt/3rdPartySoftware/boost/boost_1_81_0" )
#SET( BOOST_PATH_LIBS "/opt/3rdPartySoftware/boost/boost_1_81_0/stage/lib" )



#--------------
# open6 toolkit
#-------------- 
add_definitions( -DBACKEND_OPEN62541 )
SET( OPCUA_TOOLKIT_PATH "" )
SET( OPCUA_TOOLKIT_LIBS_RELEASE -lrt -lpthread )
SET( OPCUA_TOOLKIT_LIBS_DEBUG -lrt -lpthread )
SET (LOGIT_HAS_UATRACE FALSE)
SET (SERVERCONFIG_LOADER ON CACHE BOOL "Since quasar 1.5.1 the open62541-compat will also load ServerConfig.xml same way UA-SDK does")
include_directories( ${PROJECT_BINARY_DIR}/open62541-compat/extern/open62541/include )

# liblxi done in toolchain

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

