#!/bin/csh
# open PDU default port 23600 for lxi
firewall-cmd --get-active-zones
sudo firewall-cmd --zone=public --permanent --add-port=23600/tcp
sudo firewall-cmd --reload
