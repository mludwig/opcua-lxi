#!/bin/bash
# quasar 1.5.1 10 may 2021 for open6
python3 ./quasar.py set_build_config ./cc7_open6.cmake
python3 ./quasar.py enable_module open62541-compat
# somewhat undocumented quasar command, call that explicitly to generate the file as well. Otherwise it is missing arbitrarily 3/5 cases
python3 ./quasar.py generate base_h LxiCommand
python3 ./quasar.py build

#
# we would like to have all the generated files available for QA as well,
# but when we do QA we don't care about the actual building: it is generating docs
# and static code analysis. In order to have all generated files in scope we just
# search through a part of the build tree and copy the files into Documentation/generatedSources
#
# but that does not work: sphinx does not find the classes in the doxygen xml output
#rm -rf ./generatedSources
#mkdir -p ./generatedSources
#find ./build -name "*.cpp" -exec cp -v {}  ./generatedSources \;
#find ./build -name "*.h"   -exec cp -v {}  ./generatedSources \;
#find ./build -name "*.hxx" -exec cp -v {}  ./generatedSources \;
#find ./build -name "*.cxx" -exec cp -v {}  ./generatedSources \;
#
# copy to run
echo "===copy bins to run==="
cp -v ./build/bin/OpcUaLxiServer.open6 ./run
cp -v ./build/Configuration/Configuration.xsd ./run/Configuration
cp -v ./build/open62541-compat/xsd/ServerConfig.xsd ./run/Configuration
#
# adapt ServerConfig.xml to toolkit
cp ./run/ServerConfig.open6.xml ./run/ServerConfig.xml



