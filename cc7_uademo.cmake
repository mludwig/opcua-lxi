# @author mludwig
# Tue Jun  6 17:01:58 CEST 2017
# configure build chain for ua and boost on local machine

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cc7.uademo" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
SET( BOOST_HOME "/home/mludwig/3rdPartySoftware/boost/boost_1_81_0" )

#----------------
# UA toolkit demo
#----------------
SET ( OPCUA_TOOLKIT_PATH "/home/mludwig/3rdPartySoftware/UnifiedAutomation/1.5.5/sdk" )
message( INFO " UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" ) 
add_definitions( -DBACKEND_UATOOLKIT ) 
SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamodule -lcoremodule -luapki -luabase -luastack -lxmlparser" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS_DEBUG} ) 
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uabase )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uastack )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uaserver )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uapki )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/xmlparser )

# tookit: add empty target as flag for ua
add_custom_target ( quasar_opcua_backend_is_ready )

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

