/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */



#include <boost/foreach.hpp>
#include <iostream>
#include <algorithm>
#include <boost/asio.hpp>

#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>

#include <ASPowerSupply.h>
#include <ASLxiCommand.h>
#include <ASNodeQueries.h>

#include <DPowerSupply.h>
#include <Configurator.h>

#include <VERSION.h>
#include <unistd.h>

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{
 
}

/**
 * one thread per power supply. They are on different IPs
 * Each thread does polling and control. All channels of a power supply are polled in the same thread.
 *
 */
void QuasarServer::mainLoop()
{
	printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

	LOG(Log::INF) << "running VERSION " << OPCUA_LXI_VERSION
			<< " built at " << __DATE__ << " " << __TIME__;

	try {
		//std::string hn = boost::asio::ip::host_name();
		//std::string msg = " endpoint= " + m_endpointPortNumber;
		//printServerMsg( msg );

		LOG(Log::INF) << "mainLoop starting threads, reading configuration for PowerSupply";
		std::vector<std::thread> threadVector;

		BOOST_FOREACH(Device::DPowerSupply* dev, Device::DRoot::getInstance()->powersupplys()) {
			threadVector.push_back( std::thread ( &Device::DPowerSupply::pollUpdate, dev ));
		}
		BOOST_FOREACH(Device::DPowerSupply* dev, Device::DRoot::getInstance()->powersupplys()) {
			threadVector.push_back( std::thread ( &Device::DPowerSupply::checkAcquisitionTimestamps, dev ));
		}

		// Wait for user command to terminate the server thread.
		while(ShutDownFlag() == 0) {
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}

		for ( size_t i = 0; i < threadVector.size(); i++) {
			threadVector[i].join();
		}
	} 	catch (const std::exception& e) {
		LOG(Log::ERR) << __FUNCTION__ << "got an exception: " << e.what();
	}
	printServerMsg(" Shutting down server");

}

/**
 * read the configuration:
 *
 * - the power supplies. Each ps is identified by an ipv4 number as a primary key. Using the ip
 *   the ID string is discovered, and from this we get the serial number, specific type, firmware version.
 *   The specific type is a member of a hw family, therefore we know the full abstraction method set
 *   including the needed Lxi commands for each method.
 *   Each power supply is one instance, which runs in one thread for polling and controls, ruled by a bi-state
 *   state machine (poll-control).
 *
 * - the LxiCommand mapping. This is the mapping between the hw abstraction "methods"
 *   and the actual LxiCommands which communicate with the device. We have several families:
 *   must be one of {"all" | "aimTTi_LD" | "aimTTi_PL" | "aimTTi_TSX"} to indicate a family of
 *   devices. One abstraction "method" can have several mappings for several families, but of course
 *   it must be unique per family. Some "methods" do not have a mapping for a family: then the
 *   family does not implement that functionality and therefore ist can not have an abstraction.
 *
 *   The ps class inherits from the LxiComm.
 */
void QuasarServer::initialize()
{
	LOG(Log::INF) << "Initializing Quasar server: Devices PowerSupply";
	BOOST_FOREACH(Device::DPowerSupply* dev, Device::DRoot::getInstance()->powersupplys())
	{
		dev->init();
	}

	LOG(Log::INF) << "Initializing Quasar server: AS for objects LxiCommands RO cachevariables init";
	std::vector< AddressSpace::ASLxiCommand * > objASLxiCommand;
	std::string pattern (".*");
	AddressSpace::findAllByPattern<AddressSpace::ASLxiCommand> (m_nodeManager,
			m_nodeManager->getNode(UaNodeId(OpcUaId_ObjectsFolder, 0)),
			OpcUa_NodeClass_Object,
			pattern, objASLxiCommand);

	BOOST_FOREACH(AddressSpace::ASLxiCommand *aps, objASLxiCommand)
	{
		Device::DLxiCommand *dev = aps->getDeviceLink();
		dev->init();
	}
}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server, stopping PS threads";
	BOOST_FOREACH(Device::DPowerSupply* dev, Device::DRoot::getInstance()->powersupplys())
	{
		dev->shutdown();
	}
}

void QuasarServer::initializeLogIt()
{
	BaseQuasarServer::initializeLogIt();
    LOG(Log::INF) << "Logging initialized.";
}
