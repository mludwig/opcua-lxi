#!/bin/bash

./quasar.py set_build_config ./gitlab_cal9_uademo.cmake
./quasar.py disable_module open62541-compat
# somewhat undocumented quasar command, call that explicitly to generate the file as well. Otherwise it is missing arbitrarily 3/5 cases
./quasar.py generate base_h LxiCommand
./quasar.py build
#
# QA: scope for generated code as well
rm -rf ./generatedSources
mkdir -p ./generatedSources
find ./build -name "*.cpp" -exec cp -v {}  ./generatedSources \;
find ./build -name "*.h"   -exec cp -v {}  ./generatedSources \;
find ./build -name "*.hxx" -exec cp -v {}  ./generatedSources \;
find ./build -name "*.cxx" -exec cp -v {}  ./generatedSources \;
#
# copy to run 
echo "===copy bins to run==="
find ./build -name "OpcUaLxiServer.cal9.uademo" -exec cp -v {} ./run \;
find ./build -name "Configuration.xsd" -exec cp -v {} ./run/Configuration \;
find ./build -name "ServerConfig.xsd" -exec cp -v {} ./run/Configuration \;

# adapt the ServerConfig.xml for the toolkit
cp -v ./run/ServerConfig.ua.xml ./run/ServerConfig.xml 
cp -v ./run/ServerConfig.ua.xml ./run/ServerConfig_rpm.xml 
