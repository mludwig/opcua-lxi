# @author mludwig (c) cern
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cc7.open6" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
SET( IGNORE_DEFAULT_BOOST_SETUP "ON" )
SET( BOOST_HOME /opt/3rdPartySoftware/boost/boost_1_81_0 )
SET( BOOST_ROOT ${BOOST_HOME} )
SET( BOOST_PATH_LIBS ${BOOST_HOME}/stage/lib )
SET( BOOST_PATH_HEADERS  ${BOOST_HOME} )

#--------------
# open6 toolkit
#-------------- 
add_definitions( -DBACKEND_OPEN62541 )
SET( OPCUA_TOOLKIT_PATH "" )
SET( OPCUA_TOOLKIT_LIBS_RELEASE -lrt -lpthread )
SET( OPCUA_TOOLKIT_LIBS_DEBUG -lrt -lpthread )
SET (LOGIT_HAS_UATRACE FALSE)
SET (SERVERCONFIG_LOADER ON CACHE BOOL "Since quasar 1.5.1 the open62541-compat will also load ServerConfig.xml same way UA-SDK does")
include_directories( ${PROJECT_BINARY_DIR}/open62541-compat/extern/open62541/include )

# liblxi done in toolchain

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

