#!/bin/bash
# may 2021
# building opcua-lxi with ua licensed toolkit, quasar 1.5.1
#
python3 ./quasar.py set_build_config ./gitlab_cc7_ua.cmake
python3 ./quasar.py disable_module open62541-compat
# somewhat undocumented quasar command, call that explicitly to generate the file as well. Otherwise it is missing arbitrarily 3/5 cases
python3 ./quasar.py generate base_h LxiCommand
python3 ./quasar.py build 
#
# copy to run 
echo "===copy bins to run==="
find ./build -name "OpcUaLxiServer.cc7.ua" -exec cp -v {} ./run \;
find ./build -name "Configuration.xsd" -exec cp -v {} ./run/Configuration \;
find ./build -name "ServerConfig.xsd" -exec cp -v {} ./run/Configuration \;
#cp -v ./build/bin/OpcUaLxiServer.cc7.ua ./run/OpcUaLxiServer.cc7.ua
#cp -v ./build/Configuration/Configuration.xsd ./run/Configuration
# adapt the ServerConfig.xml for the toolkit
cp -v ./run/ServerConfig.ua.xml ./run/ServerConfig.xml 
cp -v ./run/ServerConfig.ua.xml ./run/ServerConfig_rpm.xml 

