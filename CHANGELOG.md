# opcua-lxi -  changelog

OPCUA to LXi bridge for controlling LXi compliant devices, mostly power supplies from AIM-TTi for a start for NA62 .

### v1.0.19 [16.feb.2024]
- fix ENS-34363 reported by NA62: documentation is broken, server does not run under alma9.2 with rpm from JCOP page, configuration has little mismatches
- updated build image gitlab-registry.cern.ch/mludwig/docker:opcua-lxi.cal9, it is 9.3 while NA62 runs on 9.2, redhat. 
- using the latest ubuntu2204 image for documentation.
- fixed Doxyfile for doxygen 1.9.1 (was 1.6.1) under ubuntu2204 image
- testing alma9.3 built RPM on runtime alma9.2 system (this can be done in a VM)
- use boost-1.75.0 from system as default for alma9


### v.1.0.18
- builds for [ cal9, cc7 ] X [ open6, ua, uademo ] available, images updated
- renamed the rpms in a more explicit way to distinguish the versions
- tested with cal9.2
- JCOP production


### v.1.0.17
- quasar 1.5.18, boost 1.75.0 from system cal9, cc7 images updated
- boost 1.81.0 optional in images
- improved PSconnected field, flips to fals if no acquisition for >50secs OR lxi comm down
- HWconnected shows only lxi comm status, unrelieable
- renamed all artifacts for more clarity: OpcUaLxi*<os>.<toolset>
- fixed quasar bug in Server/CMakeLists.txt, added src/QuasarUaTraceHook.cpp # code is empty for open6, ifdefs

## v1.0.16 [24.nov.2022 ]
- after 10 failed reconnections, do a RESET on the PS, and try again (endless loop).
a-the network is lost, but power stays ON, network comes back

the network socket (tcp/ip) with it's lxi session is NOT reconnected on the PLH side, even when a lxi_disconnect, lxi_connect is made. The new session blocks on a mutex on the opcua-lxi server side tcp/ip kernel layer), which means basically that the PLH does not reply. After a long (unknown) time the PLH might drop the session and wait for a new request. Is there anything in the manual about such a session-expiry delay? If the opcua-lxi makes a new session request when the PLH is not ready then the server thread for that PLH concerned is stuck forever on the (tcp/ip-) session mutex.

If the opcua-lxi server ignores transmissison errors and just KEEPS the session it has, the communication with the PLH comes back after about 30secs when network is resotored

b-power is lost, and comes back

after power-up the PLH expects a new session request. Any previous session is invalid on the stack and can not be used on the opcua-lxi server. This is what the server does ONCE at init in all cases.

The opcua-lxi server can not distinguish between a and b. The difference in the code is not a big deal. If the PLH looses connection with (b) and opcua-lxi is configured (a) this will not work, and vice-versa. The behavior has to match on both sides. Implement A

## v1.0.15 [23.nov.2022]
- ENS3221: reconnection issues improved: the ID reply does not always come up correctly after power up
  Protect against that with a loop trying 10 times

## v1.0.14 [26.sept.2022]
- increase certificate validity in ServerConfig.xml from 1 year to 10 years
- some mini-code fixes
- increased reconnection delay from 1000ms to 3000ms to increase chance ( https://its.cern.ch/jira/browse/ENS-32211 ). Also improved logging and a more strict reconnected flag in all cases.


## v1.0.13 [9.may.2022]
- adding CPX400DP contributed from Oliver

## v1.0.12 [8.dec.2021]
- solving ENS-30720: add CPX400SP for NA62
- updated doc accordingly

## v1.0.11 [14.june2021]
- *** release version recommended for production, tested ***
- the Lxi default server port is now 23500, and it is configurable for all tollkits if needed (ServerConfig.xml)
- ServerConfig.xml for toolkits delivered (just the header line is different)
- quasar version 1.5.1
- no other changes. 

## v1.0.10 [~June 2021]
- solving OPCUA-2272: .open6 should load ServerConfig.xml to avoid port clashes and make it configurable. Also cleanup remaining issues with .open6 build
- solving OPCUA-2124 alongside: quasar 1.5.1
- lxi default port is 23600 as per ServerConfig.xml
- C++17
- no other functional changes, just CI, quasar update and port config


## v1.0.9 [29.march.2021]
- cranked up lxi tcp timeout from 1000ms to 5000ms

## v1.0.8 [19.march.2021]
communication is still lost after 30 minutes (ENS-29075). No reply from aimTTi yet on the
question relating to their firmware. So, lets slow down the server even more: NA62 thinks
1 Hz is still fast enough.
- define M_DELAY_MS_SUPER 0
- define M_DELAY_MS_FAST 10
- define M_DELAY_MS_MEDIUM 150  // too fast for aimTTi_PL
- define M_DELAY_MS_SLOW 1000   // aimTTi_PL
- define M_DELAY_MS_SNAIL 5000 // default if family is unknown

## v1.0.7 [1.march.2021 ]
- improved reporting of connect/disconnect: added field PSConnected
- increased comm delay from 100ms to 150ms (SLOW) since occasionally a few timeouts still pop up with the qmd404
- documentation appended accordingly

## v1.0.6 [ 5.jan.2021 ]
- further take down polling frequency: wait delay*3 before new cycle: asymmetric, 
  mainLoopDelay
- show alive counter also when trying to reconnect

## v1.0.5 [ 30.nov.2020]
- static linkage for ua, uademo and open6
- cpack setup cleaned for RPM production
- comm delay for type corrected

## v1.0.4 [ 26.nov.2020 ]
- take down command frequency to 1Hz as default 
- automatic comm delay for each family, selected families might be faster. Hardcoded in DPowerSupply and DLxiCommand.
- aimTTi_PL at 100ms (10Hz)
- liblxi shared linkage for ua and uademo
- liblxi static linkage fopr open6
- updated all docker build images for CI 


## v1.0.3 [ 20.nov.2020 ]
- added a delay 30ms to each lxi command, in order to slow down comms with the aim-tti PS. They seems to saturate internal buffers and loose connection otherwise. 

## todo
- CI/CD integration: deliver doc to eos: pdf, doxygen html, html as searcheable trees (ics-fd-qa)
- delivery as rpm using cpack
- re-check documentation using sphinx: parse through quasar AS classes for the OPCUA AS layout, profit from the comments in the code. Use ./build tree
  
