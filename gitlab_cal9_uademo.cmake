# @author mludwig (c) cern
MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] start" )
SET( CMAKE_VERBOSE_MAKEFILE "ON" )
SET( OPCUA_LXI_SERVER_SUFFIX "cal9.uademo" )
SET( OPCUA_TOOLKIT ${OPCUA_LXI_SERVER_SUFFIX} )

#------ 
# Boost
#------
# alma9 runs with 1.75.0
SET( IGNORE_DEFAULT_BOOST_SETUP OFF )
#SET( BOOST_HOME "/opt/3rdPartySoftware/boost/boost_1_81_0" )
#SET( BOOST_PATH_HEADERS "/opt/3rdPartySoftware/boost/boost_1_81_0" )
#SET( BOOST_PATH_LIBS "/opt/3rdPartySoftware/boost/boost_1_81_0/stage/lib" )


#-----------
# UA toolkit
#-----------
SET( OPCUA_TOOLKIT_PATH "/opt/3rdPartySoftware/UnifiedAutomation-1.7.9/sdk" )
message(STATUS "UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" ) 
add_definitions( -DBACKEND_UATOOLKIT )
SET( OPCUA_TOOLKIT_LIBS_DEBUG   "-luamodule -lcoremodule -luapkicpp -luabasecpp -luastack -lxmlparsercpp /opt/3rdPartySoftware/openssl-1.1.1u/libssl.so /opt/3rdPartySoftware/openssl-1.1.1u/libcrypto.so -lxml2 -lpthread" ) 
SET( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS_DEBUG} ) 
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uabasecpp )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uastack )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uaservercpp )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/uapkicpp )
include_directories( ${OPCUA_TOOLKIT_PATH}/include/xmlparsercpp )


#-----------
# UA toolkit
#-----------
#SET( OPCUA_TOOLKIT_PATH "/opt/3rdPartySoftware/UnifiedAutomation-1.5.5/UnifiedAutomation/1.5.5/sdk" )
#message(STATUS "UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" ) 
#add_definitions( -DBACKEND_UATOOLKIT )
#SET( OPCUA_TOOLKIT_LIBS_DEBUG   "-luamodule -lcoremodule -luapki -luabase -luastack -lxmlparser -lxml2 -lssl -lcrypto -lpthread" ) 
#SET( OPCUA_TOOLKIT_LIBS_RELEASE ${OPCUA_TOOLKIT_LIBS_DEBUG} ) 
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uabase )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uastack )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uaserver )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/uapki )
#include_directories( ${OPCUA_TOOLKIT_PATH}/include/xmlparser )

# tookit: add empty target as flag for ua
add_custom_target ( quasar_opcua_backend_is_ready )

# strange problem: does not find a base class include
#include_directories( ./build/Device/generated )

# liblxi done in toolchain

MESSAGE( INFO "[${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}] end" )

