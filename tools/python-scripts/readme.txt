readme - python tests
=====================

we are using opcua-asyncio as a python client for OPCUA 
we are trying to do some stress tests and stability
one client instance writes, reads and subscribes to the server's OPCUA AS
each client instance ramps up and down each channel from a list of 3 hysically existing power supplies
the python implementation is contained in a docker image

to run:

# 1. docker run --net=host --expose=23600 gitlab-registry.cern.ch/mludwig/opcua_python:opcua-asyncio1.0.0
1. docker run --net=host --expose=23600 gitlab-registry.cern.ch/mludwig/docker:opcua-asyncio.cc7

2. then get the docker conytainer ID ( docker ps)

3. then inject the client script:
   docker cp myscript.py containerID:/client.py

if you want to try out thread safety and parallel execution with the server, like i.e. many clients connecting to the server
and trying to do (conflicting!) things to the powersupplies, just run several images in parallel and inject the same script
with some time-delay. Each docker container will pick up the script, execute it (in the same way!) and when finished, wait
for a new injection. If you run i.e. three containres in parallel you can have 3 channels ramping concurrently, always working with the same single server instance and endpoint.


