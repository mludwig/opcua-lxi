# test script for opcua-lxi server:
# exercise a physical power supply
# demo client to read/write to an OPCUA server for .open6 toolkit


# ---start asycio---
import asyncio
import sys

sys.path.insert(0, "..")
import logging
from asyncua import Client, Node, ua
from datetime import datetime


# defined levels are: INFO, DEBUG, WARNING, ERROR. they are independent channels without hierarchy
# problem is that there are lots of INFO messages in asyncio which should rather be DEBUG, so you
# get too many INFO to be useful. Therefore let's abuse WARNING to log our results actually
# logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.WARNING)

_logger = logging.getLogger('asyncua')

# ---end asyncio

# ---common---
endpoint0 = "opc.tcp://localhost:4841"
globalName = "opcuaserver" 
#globalUrn = "urn:JCOP:OpcUaCaenServer"
globalUrn = "OPCUASERVER"
uri = 'OPCUASERVER'
 

# -- start asyncio functions---


# subscription handler as a class
# from https://github.com/FreeOpcUa/opcua-asyncio/blob/master/examples/client-subscription.py
class SubscriptionHandler:
    """
    The SubscriptionHandler is used to handle the data that is received for the subscription.
    """
    def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        #_logger.info('datachange_notification %r %s', node, val)
        _logger.warning("new data arrived: %r= %s", node, val)



# get value from a var name like 'TEST3_CycleTime_AS.PosSt.value'
async def get( cl, varname ):
    var=cl.get_node( varname )
    _logger.debug("get var %s", var )
    value=await var.read_value()
    _logger.debug("get %s= %r", varname, value)
    return value


# read to get the variant type and then set
async def set( cl, parname, valueToSet ):
    _logger.debug("set-get %s", parname)
    par=cl.get_node( parname )

    dvx=await par.get_data_value()    # need the type of the variant
    dv = ua.DataValue(ua.Variant( valueToSet, dvx.Value.VariantType))
    dv.ServerTimestamp = None         # needed, otherwise .ua will not accept
    dv.SourceTimestamp = None
    await par.set_value(dv)
    _logger.debug("set %s to %s", parname, valueToSet)

# ramp up and down a channel 1..10V
async def ramp_a_channel( cl, psname, channelname ):
    nsidx = await cl.get_namespace_index( uri )
    nodePrefix=f"ns={nsidx};s=" 
    limitCurrentNode=nodePrefix+psname+"."+channelname+".LimitCurrent"
    limitCurrent=100
    await set( cl, limitCurrentNode, limitCurrent )

    enableNode=nodePrefix+psname+"."+channelname+".EnableOutput"
    enable=True
    await set( cl, enableNode, enable )

    # ramp up the target voltage from 1 to 10 Volts in 0.1 V steps, makes 100 steps
    targetVoltageNode=nodePrefix+psname+"."+channelname+".TargetVoltage"
    for i in range(1,100):
       targetVoltage=i/10
       await set( cl, targetVoltageNode, targetVoltage )
       _logger.warning("SET %s= %r", targetVoltageNode, targetVoltage )
       await asyncio.sleep( 0.2 )

    for i in range(1,100):
       targetVoltage=10-i/10
       await set( cl, targetVoltageNode, targetVoltage )
       _logger.warning("SET %s= %r", targetVoltageNode, targetVoltage )
       await asyncio.sleep( 0.2 )

    enable=False
    await set( cl, enableNode, enable )


# ---start asyncio main---
async def main():
   _logger.info("===start init===")
   async with Client(url=endpoint0) as client:

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
        root = client.get_root_node()
        _logger.debug("root node= %r", root)
        nsidx = await client.get_namespace_index( uri )
        _logger.debug("namespace index= %d", nsidx)
        nodePrefix=f"ns={nsidx};s="
        _logger.info("nodePrefix= %s", nodePrefix)
        _logger.info("===end init===")

    
        # with a 1kOhm load we will ramp up and down 0..10V one channel once
	# while subscribing to the counter and to the output voltage and current


        # subscription to Counter, and outputs, all on the same handler with different instances
        nn_name0=nodePrefix+"plh250_test1.Counter"
        nn_node0= client.get_node( nn_name0 )
        nn_name1=nodePrefix+"plh250_test1.Chan001.OutputVoltage"
        nn_node1= client.get_node( nn_name1 )
        nn_name2=nodePrefix+"plh250_test1.Chan001.OutputCurrent"
        nn_node2= client.get_node( nn_name2 )

        handler = SubscriptionHandler()
        timeoutInSecs=500
        subscription = await client.create_subscription( timeoutInSecs, handler )

        # we can subscribe to several nodes with the same handler
        #nodes = [ n_value ] # or: from above example, whatever
        nodes = [ nn_node0, nn_node1, nn_node2 ]

        # subscribe
        await subscription.subscribe_data_change( nodes )

        #await ramp_a_channel( client, "plh250_test1", "Chan001" )
        #await ramp_a_channel( client, "plh250-test2", "Chan001" )
        pslist = [ "plh250_test1", "plh250_test2" ]
        for ps in pslist:
           await ramp_a_channel( client, ps, "Chan001" )

        await ramp_a_channel( client, "pl303qmd_test3", "Chan001" )
        await ramp_a_channel( client, "pl303qmd_test3", "Chan002" )

      
        # We delete the subscription (this un-subscribes from the data changes of the two variables).
        # This is optional since closing the connection will also delete all subscriptions.
        await subscription.delete()

        # After one second we exit the Client context manager - this will close the connection.
        await asyncio.sleep(1)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(main())
    loop.close()

# ---end asyncio main---


