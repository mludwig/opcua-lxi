# /** # * © Copyright CERN 2019. All  rights  reserved.  This  software  is  released  under  a 
# * CERN proprietary  software  licence.  Any  permission to  use  it  shall  be  granted 
# * in  writing.  Requests shall be addressed to CERN through mail 
# * - author(s) 
# * - KT@cern.ch 
# * # * author(s): 
# *   Michael Ludwig BE-ICS-FD michael.ludwig@cern.ch 
# * # * This file is part of OPCUA Lxi/PDU project. 
# */ 
cmake_minimum_required(VERSION 3.0)
project( curltest LANGUAGES C CXX  VERSION 0.0.0.1  )
set (CMAKE_CXX_STANDARD 17)

message(STATUS " [${CMAKE_CURRENT_LIST_FILE}:${CMAKE_CURRENT_LIST_LINE}]: curltest version= ${PROJECT_VERSION}" )
file(WRITE include/VERSION.h "// VERSION.h - do not edit\n#define curltest_VERSION \"${PROJECT_VERSION}\"\n" )

set( CMAKE_BUILD_TYPE "Debug" )
#set( CMAKE_BUILD_TYPE "Release" )

SET( CMAKE_VERBOSE_MAKEFILE on )
SET( CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
SET( CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

# or ./usr/local/lib/liblxi.so
link_directories ( /home/mludwig/projects/opcua-lxi/liblxi/src/.libs )
add_subdirectory ( src )
