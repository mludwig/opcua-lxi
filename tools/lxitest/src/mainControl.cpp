/**
 * test talking to a aimTTi PS via curl and the built-in web server
 * test how the curl handler can be used to get a proper reply, and
 * how much this could be compatible for various types of PS, like
 * i.e. ServerTech PDU, aimTTi Lxi...
 *
 */
#include <sys/time.h>

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>

#include <boost/thread.hpp>

#include <VERSION.h>
#include <lxi.h>

int main( int argc, char **argv ){
	std::cout << __FILE__ << " " << __LINE__ << " C++ version= " << __cplusplus << std::endl; // C++11 shows 201103
	const size_t szresponse = 65536;
	char response[szresponse];
	int timeout = 300; //ms
	int device0 = -1;
	int device1 = -1;
	std::string scommand = "*IDN?";
	//std::string addr = "172.26.96.228";
	std::string addr0 = "beicsfdplh250test1";
	std::string addr1 = "beicsfdplh250test2";
	int port = 9221;
	//	std::string name = "t445978.local.";
	std::string name = "";
	// Initialize LXI library
	lxi_init();
	std::cout << __FILE__ << " " << __LINE__ << " lxi init OK" << std::endl;

	// Connect LXI device
	/**
	typedef enum
	{
	    VXI11,
	    RAW,
	    HISLIP
	} lxi_protocol_t;
	 */

	device0 = lxi_connect( addr0.c_str(), port, name.c_str(), timeout, RAW );
	std::cout << __FILE__ << " " << __LINE__ << " lxi_connect OK, device0= " << device0 << std::endl;
	device1 = lxi_connect( addr1.c_str(), port, name.c_str(), timeout, RAW );
	std::cout << __FILE__ << " " << __LINE__ << " lxi_connect OK, device1= " << device1 << std::endl;


	lxi_send( device0, scommand.c_str(), strlen( scommand.c_str()), timeout );
	std::cout << __FILE__ << " " << __LINE__ << " lxi_send OK" << std::endl;
	// Wait for response
	lxi_receive(device0, response, sizeof(response), timeout);
	std::cout << __FILE__ << " " << __LINE__ << " lxi_receive OK" << std::endl;
	std::cout << " response= " <<  response << std::endl;


	lxi_send( device1, scommand.c_str(), strlen( scommand.c_str()), timeout );
	std::cout << __FILE__ << " " << __LINE__ << " lxi_send OK" << std::endl;
	// Wait for response
	lxi_receive(device1, response, sizeof(response), timeout);
	std::cout << __FILE__ << " " << __LINE__ << " lxi_receive OK" << std::endl;
	std::cout << " response= " <<  response << std::endl;




	// do some initial setup. Some commands have a reply, some not.
	std::vector<std::string> commands;
	std::vector< bool > reply;

	/*
	commands.push_back("IFUNLOCK");    reply.push_back( true );
	commands.push_back("IFLOCK");      reply.push_back( true );
	commands.push_back("DELTAI1 0.2"); reply.push_back( false );
	commands.push_back("DELTAV1 0.3"); reply.push_back( false );
	commands.push_back("IRANGE1 1");   reply.push_back( false ); // 0..500mA range

	commands.push_back("V1 12");       reply.push_back( false );
	 */

	commands.push_back("V1 12.2");    reply.push_back( false );
	commands.push_back("V1O?");    reply.push_back( true );
	commands.push_back("I1 0.004");   reply.push_back( false );
	commands.push_back("I1O?");    reply.push_back( true );

	struct timeval tv0, tv1;
	struct timezone tz;
	gettimeofday( &tv0, &tz );
	int counter = 0;
	const int count = 1000;
	for ( int k = 0; k < count; k++ ){

		for ( int i = 0; i < commands.size(); i++ ){
			lxi_send( device0, commands[i].c_str(), strlen( commands[i].c_str()), timeout );
			std::cout << __FILE__ << " " << __LINE__ << " lxi_send0 " << commands[i].c_str() << std::endl;
			// boost::this_thread::sleep(boost::posix_time::seconds(2));

			if ( reply[i] ){
				memset( response, 0, szresponse );
				lxi_receive(device0, response, sizeof(response), timeout);
				std::cout << __FILE__ << " " << __LINE__ << " lxi_receive0 " << response;
				// boost::this_thread::sleep(boost::posix_time::seconds(2));
			}

			lxi_send( device1, commands[i].c_str(), strlen( commands[i].c_str()), timeout );
			std::cout << __FILE__ << " " << __LINE__ << " lxi_send1 " << commands[i].c_str() << std::endl;
			// boost::this_thread::sleep(boost::posix_time::seconds(2));

			if ( reply[i] ){
				memset( response, 0, szresponse );
				lxi_receive(device1, response, sizeof(response), timeout);
				std::cout << __FILE__ << " " << __LINE__ << " lxi_receive1 " << response;
				// boost::this_thread::sleep(boost::posix_time::seconds(2));
			}
		}
		counter++;
	}

	gettimeofday( &tv1, &tz );
	double delta = ((double) tv1.tv_sec  - (double) tv0.tv_sec ) * 1000
			+ ((double) tv1.tv_usec/1000 - (double) tv0.tv_usec/ 1000 );
	std::cout << __FILE__ << " " << __LINE__ << " had " << counter << " set/get in " << delta << " ms" << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " takes " << delta/counter << " ms per request " << std::endl;



	// Disconnect


	lxi_disconnect(device0);
	std::cout << __FILE__ << " " << __LINE__ << " lxi_disconnect OK" << std::endl;

	return 0;
}
