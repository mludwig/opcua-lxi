/**
 * test talking to a aimTTi PS via curl and the built-in web server
 * test how the curl handler can be used to get a proper reply, and
 * how much this could be compatible for various types of PS, like
 * i.e. ServerTech PDU, aimTTi Lxi...
 *
 */
#include <sys/time.h>
#include <boost/thread.hpp>

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>

#include <VERSION.h>
#include <lxi.h>

int main( int argc, char **argv ){
	std::cout << __FILE__ << " " << __LINE__ << " C++ version= " << __cplusplus << std::endl; // C++11 shows 201103
	const size_t szresponse = 65536;
	char response[szresponse];
	int timeout = 300;
	int device = -1;
	std::string scommand = "*IDN?";
	std::string addr = "beicsfdplh250test1";
	//	std::string addr = "172.26.96.228";
	int port = 9221;
	//std::string name = "t445978.local.";
	std::string name = "";
	// Initialize LXI library
	lxi_init();
	std::cout << __FILE__ << " " << __LINE__ << " lxi init OK" << std::endl;

	// Connect LXI device
	/**
	typedef enum
	{
	    VXI11,
	    RAW,
	    HISLIP
	} lxi_protocol_t;
*/

	device = lxi_connect( addr.c_str(), port, name.c_str(), timeout, RAW );
	// device = lxi_connect( addr.c_str(), port, name.c_str(), timeout, VXI11);

	// device = lxi_connect( addr.c_str(), 0, NULL, timeout, HISLIP); /// not supported
	std::cout << __FILE__ << " " << __LINE__ << " lxi_connect OK, device= " << device << std::endl;

	// Send SCPI command
	lxi_send( device, scommand.c_str(), strlen( scommand.c_str()), timeout );
	std::cout << __FILE__ << " " << __LINE__ << " lxi_send OK" << std::endl;

	// Wait for response
	lxi_receive(device, response, sizeof(response), timeout);
	std::cout << __FILE__ << " " << __LINE__ << " lxi_receive OK" << std::endl;

	std::cout << " response= " <<  response << std::endl;


	// go into polling loop to see how fast this works
	std::vector<std::string> commands;
/**
	commands.push_back("*IDN?");
	commands.push_back("EER?");
	commands.push_back("IFLOCK?");
	commands.push_back("QER?");
	commands.push_back("DELTAI1?");
	commands.push_back("DELTAV1?");
	commands.push_back("QER?");
	*/


	commands.push_back("V1?");
	commands.push_back("V1O?");
	commands.push_back("I1?");
	commands.push_back("I1O?");

	// commands.push_back("I1 0.02");
	// commands.push_back("V1 10.2");

	int counter = 0;
	const int count = 1000;
	struct timeval tv0, tv1;
	struct timezone tz;
 	gettimeofday( &tv0, &tz );

	for ( int k = 0; k < count; k++ ){
		for ( int i = 0; i < commands.size(); i++ ){

			//std::cout << __FILE__ << " " << __LINE__ << " loop k= " << k << " i= " << i << std::endl;

			lxi_send( device, commands[i].c_str(), strlen( commands[i].c_str()), timeout );
			//std::cout << __FILE__ << " " << __LINE__ << " lxi_send " << commands[i].c_str() << std::endl;

			memset( response, 0, szresponse );
			lxi_receive(device, response, sizeof(response), timeout);
			std::cout << __FILE__ << " " << __LINE__ << " lxi_receive " << response;

			// boost::this_thread::sleep(boost::posix_time::milliseconds(3000));

			counter++;
		}
		// std::cout << __FILE__ << " " << __LINE__ << " counter= " << counter << std::endl;
	}
	gettimeofday( &tv1, &tz );
	double delta = ( (double) tv1.tv_sec  - (double) tv0.tv_sec ) * 1000
			     + ((double) tv1.tv_usec / 1000 - (double) tv0.tv_usec / 1000 );
	std::cout << __FILE__ << " " << __LINE__ << " had " << counter << " set/get in " << delta << " ms" << std::endl;
	std::cout << __FILE__ << " " << __LINE__ << " takes " << delta/(counter * 2) << " ms per request " << std::endl;



	// Disconnect


	lxi_disconnect(device);
	std::cout << __FILE__ << " " << __LINE__ << " lxi_disconnect OK" << std::endl;

	return 0;
}
