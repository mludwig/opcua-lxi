#!/bin/bash
find ./ -name "CMakeCache.txt" -exec rm -v {} \;
find ./ -name "CMakeFiles" -exec rm -rfv {} \;
find ./ -name "cmake_install.cmake" -exec rm -rfv {} \;
find ./ -name "Makefile" -exec rm -rfv {} \;
#
rm -v ./bin/lxiTest.bin
 